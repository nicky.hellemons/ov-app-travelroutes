import java.time.LocalTime;

//Nicky & Koen
public class TrainData extends Data
{
    public TrainData()
    {
        //////////ROUTES
        Location location = new Location("Leeuwarden", new Coordinates(53.196642860257505, 5.7930114049474986));
        locationMap.put(location.getLocationName(), location);

        location = new Location("Heerenveen", new Coordinates(52.960768755043425, 5.915687296675503));
        locationMap.put(location.getLocationName(), location);

        location = new Location("Steenwijk", new Coordinates(52.79100028379485, 6.116493384862385));
        locationMap.put(location.getLocationName(), location);

        location = new Location("Meppel", new Coordinates(52.69202804306726, 6.197682498354228));
        locationMap.put(location.getLocationName(), location);

        location = new Location("Zwolle", new Coordinates(52.505665767293245, 6.090572469513917));
        locationMap.put(location.getLocationName(), location);

        location = new Location("Rotterdam Centraal", new Coordinates(51.924452191833474, 4.469983382992588));
        locationMap.put(location.getLocationName(), location);

        location = new Location("Amersfoort Centraal", new Coordinates(52.15352355432786, 5.374044569504253));
        locationMap.put(location.getLocationName(), location);

        location = new Location("Utrecht Centraal", new Coordinates(52.08952978231634, 5.109979215521572));
        locationMap.put(location.getLocationName(), location);

        location = new Location("Gouda", new Coordinates(52.01705360226917, 4.7064303406654));
        locationMap.put(location.getLocationName(), location);

        location = new Location("Rotterdam Alexander", new Coordinates(51.95168716668481, 4.552045269498759));
        locationMap.put(location.getLocationName(), location);

        location = new Location("Wijhe", new Coordinates(52.391231034016776, 6.140951298345938));
        locationMap.put(location.getLocationName(), location);

        location = new Location("Olst", new Coordinates(52.335400815796355, 6.1133036541686785));
        locationMap.put(location.getLocationName(), location);

        location = new Location("Deventer", new Coordinates(52.257185956927145, 6.160653111836823));
        locationMap.put(location.getLocationName(), location);

        location = new Location("Zutphen", new Coordinates(52.144831726635765, 6.19425025416344));
        locationMap.put(location.getLocationName(), location);

        location = new Location("Dieren", new Coordinates(52.04491690069474, 6.103234043673443));
        locationMap.put(location.getLocationName(), location);

        location = new Location("Arnhem Centraal", new Coordinates(51.984053787532105, 5.901164942510531));
        locationMap.put(location.getLocationName(), location);

        location = new Location("Nijmegen", new Coordinates(51.84374261746977, 5.853754329012147));
        locationMap.put(location.getLocationName(), location);

        location = new Location("Oss", new Coordinates(51.765282798213946, 5.5303003983288646));
        locationMap.put(location.getLocationName(), location);

        location = new Location("Tilburg", new Coordinates(51.56070564896777, 5.083353754147555));
        locationMap.put(location.getLocationName(), location);

        location = new Location("Breda", new Coordinates(51.59506263277015, 4.779178855994499));
        locationMap.put(location.getLocationName(), location);

        location = new Location("Etten-Leur", new Coordinates(51.57521959718557, 4.636649771334611));
        locationMap.put(location.getLocationName(), location);

        location = new Location("Roosendaal", new Coordinates(51.540180039188016, 4.458664642498483));
        locationMap.put(location.getLocationName(), location);

        location = new Location("Den Haag Centraal", new Coordinates(52.077451784914146, 4.3209000964913065));
        locationMap.put(location.getLocationName(), location);

        location = new Location("Enschede", new Coordinates(52.22253590130521, 6.8904794541655825));
        locationMap.put(location.getLocationName(), location);

        location = new Location("Hengelo", new Coordinates(52.262237511542295, 6.79369958300179));
        locationMap.put(location.getLocationName(), location);

        location = new Location("Almelo", new Coordinates(52.35746846178861, 6.654835467663795));
        locationMap.put(location.getLocationName(), location);

        location = new Location("Apeldoorn", new Coordinates(52.209645620781025, 5.968582398340985));
        locationMap.put(location.getLocationName(), location);

        location = new Location("Hilversum", new Coordinates(52.22675364451757, 5.181070725330533));
        locationMap.put(location.getLocationName(), location);

        location = new Location("Duivendrecht", new Coordinates(52.32342395327297, 4.9372132364093995));
        locationMap.put(location.getLocationName(), location);

        location = new Location("Schiphol Airport", new Coordinates(52.30949817128957, 4.762156167662494));
        locationMap.put(location.getLocationName(), location);

        location = new Location("Amsterdam Zuid", new Coordinates(52.3388617290237, 4.873603813685081));
        locationMap.put(location.getLocationName(), location);

        location = new Location("Amsterdam Bijlmer Arena", new Coordinates(52.31237507122209, 4.946966725332874));
        locationMap.put(location.getLocationName(), location);

        location = new Location("'s-Hertogenbosch", new Coordinates(51.690640692122464, 5.293579884832272));
        locationMap.put(location.getLocationName(), location);

        location = new Location("Eindhoven Centraal", new Coordinates(51.443631073588584, 5.478746082979544));
        locationMap.put(location.getLocationName(), location);

        location = new Location("Helmond", new Coordinates(51.475736598356924, 5.661773184826467));
        locationMap.put(location.getLocationName(), location);

        location = new Location("Deurne", new Coordinates(51.456370155348324, 5.788040729001663));
        locationMap.put(location.getLocationName(), location);

        location = new Location("Horst-Sevenum", new Coordinates(51.427183043506034, 6.041306198319713));
        locationMap.put(location.getLocationName(), location);

        location = new Location("Blerick", new Coordinates(51.37246742796558, 6.155411982977609));
        locationMap.put(location.getLocationName(), location);

        location = new Location("Venlo", new Coordinates(51.36524805153501, 6.1714049289992));
        locationMap.put(location.getLocationName(), location);

        //////////ROUTES


        //////////ROUTE Leeuwarden-Rotterdam Centraal
        for (int hour = 5; hour <= 21; hour++) {
            Route route = new Route(locationMap.get("Leeuwarden"), LocalTime.of(hour, 47));
            route.addStopOver(locationMap.get("Heerenveen"), LocalTime.of(hour + 1, 3), LocalTime.of(hour + 1, 3));
            route.addStopOver(locationMap.get("Steenwijk"), LocalTime.of(hour + 1, 16), LocalTime.of(hour + 1, 16));
            route.addStopOver(locationMap.get("Meppel"), LocalTime.of(hour + 1, 26), LocalTime.of(hour + 1, 26));
            route.addStopOver(locationMap.get("Zwolle"), LocalTime.of(hour + 1, 42), LocalTime.of(hour + 1, 50));
            route.addStopOver(locationMap.get("Amersfoort Centraal"), LocalTime.of(hour + 2, 25), LocalTime.of(hour + 2, 29));
            route.addStopOver(locationMap.get("Utrecht Centraal"), LocalTime.of(hour + 2, 42), LocalTime.of(hour + 2, 48));
            if (hour == 21) {
                hour -= 24;
            }
            route.addStopOver(locationMap.get("Gouda"), LocalTime.of(hour + 3, 6), LocalTime.of(hour + 3, 7));
            route.addStopOver(locationMap.get("Rotterdam Alexander"), LocalTime.of(hour + 3, 16), LocalTime.of(hour + 3, 16));
            route.addEndPoint(locationMap.get("Rotterdam Centraal"), LocalTime.of(hour + 3, 25));
            addRoute(route);
            if (hour < 0) {
                break;
            }
            addRoute(route);
        }

        //////////ROUTE Rotterdam Centraal-Leeuwarden
        for (int hour = 6; hour <= 21; hour++) {
            Route route = new Route(locationMap.get("Rotterdam Centraal"), LocalTime.of(hour, 35));
            route.addStopOver(locationMap.get("Rotterdam Alexander"), LocalTime.of(hour, 43), LocalTime.of(hour, 43));
            route.addStopOver(locationMap.get("Gouda"), LocalTime.of(hour, 53), LocalTime.of(hour, 54));
            route.addStopOver(locationMap.get("Utrecht Centraal"), LocalTime.of(hour + 1, 12), LocalTime.of(hour + 1, 18));
            route.addStopOver(locationMap.get("Amersfoort Centraal"), LocalTime.of(hour + 1, 31), LocalTime.of(hour + 1, 34));
            route.addStopOver(locationMap.get("Zwolle"), LocalTime.of(hour + 2, 9), LocalTime.of(hour + 2, 18));
            route.addStopOver(locationMap.get("Meppel"), LocalTime.of(hour + 2, 33), LocalTime.of(hour + 2, 34));
            route.addStopOver(locationMap.get("Steenwijk"), LocalTime.of(hour + 2, 43), LocalTime.of(hour + 2, 43));
            route.addStopOver(locationMap.get("Heerenveen"), LocalTime.of(hour + 2, 56), LocalTime.of(hour + 2, 56));
            if (hour == 21) {
                hour -= 24;
            }
            route.addEndPoint(locationMap.get("Leeuwarden"), LocalTime.of(hour + 3, 13));
            addRoute(route);
            if (hour < 0) {
                break;
            }
            addRoute(route);
        }


        //////////ROUTE Schiphol-Venlo (eerste half uur)
        for (int hour = 6; hour <= 21; hour++) {
            Route route = new Route(locationMap.get("Schiphol Airport"), LocalTime.of(hour, 19));
            route.addStopOver(locationMap.get("Amsterdam Zuid"), LocalTime.of(hour, 25), LocalTime.of(hour, 26));
            route.addStopOver(locationMap.get("Amsterdam Bijlmer Arena"), LocalTime.of(hour, 31), LocalTime.of(hour, 32));
            route.addStopOver(locationMap.get("Utrecht Centraal"), LocalTime.of(hour, 48), LocalTime.of(hour, 54));
            route.addStopOver(locationMap.get("'s-Hertogenbosch"), LocalTime.of(hour +1, 21), LocalTime.of(hour +1, 25));
            route.addStopOver(locationMap.get("Eindhoven Centraal"), LocalTime.of(hour +1, 43), LocalTime.of(hour +1, 49));
            route.addStopOver(locationMap.get("Helmond"), LocalTime.of(hour +1, 58), LocalTime.of(hour +1, 58));
            route.addStopOver(locationMap.get("Deurne"), LocalTime.of(hour +2, 5), LocalTime.of(hour +2, 5));
            route.addStopOver(locationMap.get("Horst-Sevenum"), LocalTime.of(hour +2, 17), LocalTime.of(hour +2, 17));
            route.addStopOver(locationMap.get("Blerick"), LocalTime.of(hour +2, 24), LocalTime.of(hour +2, 25));
            route.addEndPoint(locationMap.get("Venlo"), LocalTime.of(hour+2, 28));
            addRoute(route);
        }
        //////////ROUTE Schiphol-Venlo (tweede half uur)
        for (int hour = 6; hour <= 21; hour++) {
            Route route = new Route(locationMap.get("Schiphol Airport"), LocalTime.of(hour, 49));
            route.addStopOver(locationMap.get("Amsterdam Zuid"), LocalTime.of(hour, 55), LocalTime.of(hour, 56));
            route.addStopOver(locationMap.get("Amsterdam Bijlmer Arena"), LocalTime.of(hour +1, 1), LocalTime.of(hour +1, 2));
            route.addStopOver(locationMap.get("Utrecht Centraal"), LocalTime.of(hour +1, 18), LocalTime.of(hour +1, 24));
            route.addStopOver(locationMap.get("'s-Hertogenbosch"), LocalTime.of(hour +1, 51), LocalTime.of(hour +1, 55));
            route.addStopOver(locationMap.get("Eindhoven Centraal"), LocalTime.of(hour +2, 13), LocalTime.of(hour +2, 19));
            route.addStopOver(locationMap.get("Helmond"), LocalTime.of(hour +2, 28), LocalTime.of(hour +2, 28));
            route.addStopOver(locationMap.get("Deurne"), LocalTime.of(hour +2, 35), LocalTime.of(hour +2, 35));
            route.addStopOver(locationMap.get("Horst-Sevenum"), LocalTime.of(hour +2, 47), LocalTime.of(hour +2, 47));
            route.addStopOver(locationMap.get("Blerick"), LocalTime.of(hour +2, 54), LocalTime.of(hour +2, 55));
            route.addEndPoint(locationMap.get("Venlo"), LocalTime.of(hour+2, 58));
            addRoute(route);
        }

        //////////ROUTE Venlo-Schiphol (eerste half uur)
        for (int hour = 5; hour <= 21; hour++) {
            Route route = new Route(locationMap.get("Venlo"), LocalTime.of(hour, 33));
            route.addStopOver(locationMap.get("Blerick"), LocalTime.of(hour, 36), LocalTime.of(hour, 36));
            route.addStopOver(locationMap.get("Horst-Sevenum"), LocalTime.of(hour, 43), LocalTime.of(hour, 43));
            route.addStopOver(locationMap.get("Deurne"), LocalTime.of(hour, 54), LocalTime.of(hour, 54));
            route.addStopOver(locationMap.get("Helmond"), LocalTime.of(hour +1, 2), LocalTime.of(hour +1, 2));
            route.addStopOver(locationMap.get("Eindhoven Centraal"), LocalTime.of(hour +1, 11), LocalTime.of(hour +1, 17));
            route.addStopOver(locationMap.get("'s-Hertogenbosch"), LocalTime.of(hour +1, 36), LocalTime.of(hour +1, 38));
            route.addStopOver(locationMap.get("Utrecht Centraal"), LocalTime.of(hour +2, 5), LocalTime.of(hour +2, 11));
            route.addStopOver(locationMap.get("Amsterdam Bijlmer Arena"), LocalTime.of(hour +2, 27), LocalTime.of(hour +2, 28));
            route.addStopOver(locationMap.get("Amsterdam Zuid"), LocalTime.of(hour +2, 33), LocalTime.of(hour +2, 34));
            route.addEndPoint(locationMap.get("Schiphol Airport"), LocalTime.of(hour +2, 40));
            addRoute(route);
        }
        //////////ROUTE Venlo-Schiphol (tweede half uur)
        for (int hour = 6; hour <= 21; hour++) {
            Route route = new Route(locationMap.get("Venlo"), LocalTime.of(hour, 3));
            route.addStopOver(locationMap.get("Blerick"), LocalTime.of(hour, 6), LocalTime.of(hour, 6));
            route.addStopOver(locationMap.get("Horst-Sevenum"), LocalTime.of(hour, 13), LocalTime.of(hour, 13));
            route.addStopOver(locationMap.get("Deurne"), LocalTime.of(hour, 24), LocalTime.of(hour, 24));
            route.addStopOver(locationMap.get("Helmond"), LocalTime.of(hour , 32), LocalTime.of(hour , 32));
            route.addStopOver(locationMap.get("Eindhoven Centraal"), LocalTime.of(hour, 41), LocalTime.of(hour, 47));
            route.addStopOver(locationMap.get("'s-Hertogenbosch"), LocalTime.of(hour +1, 6), LocalTime.of(hour +1, 8));
            route.addStopOver(locationMap.get("Utrecht Centraal"), LocalTime.of(hour +1, 35), LocalTime.of(hour +1, 41));
            route.addStopOver(locationMap.get("Amsterdam Bijlmer Arena"), LocalTime.of(hour +1, 57), LocalTime.of(hour +1, 58));
            route.addStopOver(locationMap.get("Amsterdam Zuid"), LocalTime.of(hour +2, 3), LocalTime.of(hour +2, 4));
            route.addEndPoint(locationMap.get("Schiphol Airport"), LocalTime.of(hour +2, 10));
            addRoute(route);
        }

        //////////ROUTE Zwolle-Roosendaal (eerste half uur)
        for (int hour = 6; hour <= 20; hour++) { //20 = 22
            Route route = new Route(locationMap.get("Zwolle"), LocalTime.of(hour, 20));
            route.addStopOver(locationMap.get("Wijhe"), LocalTime.of(hour, 29), LocalTime.of(hour, 29));
            route.addStopOver(locationMap.get("Olst"), LocalTime.of(hour, 36), LocalTime.of(hour, 36));
            route.addStopOver(locationMap.get("Deventer"), LocalTime.of(hour, 44), LocalTime.of(hour, 45));
            route.addStopOver(locationMap.get("Zutphen"), LocalTime.of(hour, 56), LocalTime.of(hour, 58));
            route.addStopOver(locationMap.get("Dieren"), LocalTime.of(hour +1, 7), LocalTime.of(hour +1, 7));
            route.addStopOver(locationMap.get("Arnhem Centraal"), LocalTime.of(hour +1, 19), LocalTime.of(hour +1, 23));
            route.addStopOver(locationMap.get("Nijmegen"), LocalTime.of(hour +1, 36), LocalTime.of(hour +1, 39));
            route.addStopOver(locationMap.get("Oss"), LocalTime.of(hour +1, 54), LocalTime.of(hour +1, 55));
            route.addStopOver(locationMap.get("'s-Hertogenbosch"), LocalTime.of(hour +2, 7), LocalTime.of(hour +2, 12));
            route.addStopOver(locationMap.get("Tilburg"), LocalTime.of(hour +2, 27), LocalTime.of(hour +2, 28));
            route.addStopOver(locationMap.get("Breda"), LocalTime.of(hour +2, 40), LocalTime.of(hour +2, 45));
            route.addStopOver(locationMap.get("Etten-Leur"), LocalTime.of(hour +2, 52), LocalTime.of(hour +2, 52));
            route.addEndPoint(locationMap.get("Roosendaal"), LocalTime.of(hour +3, 3));
            addRoute(route);
        }

        //////////ROUTE Zwolle-Roosendaal (tweede half uur)
        for (int hour = 6; hour <= 20; hour++) { //20 = 22
            Route route = new Route(locationMap.get("Zwolle"), LocalTime.of(hour, 50));
            route.addStopOver(locationMap.get("Wijhe"), LocalTime.of(hour, 59), LocalTime.of(hour, 59));
            route.addStopOver(locationMap.get("Olst"), LocalTime.of(hour +1, 6), LocalTime.of(hour +1, 6));
            route.addStopOver(locationMap.get("Deventer"), LocalTime.of(hour +1, 14), LocalTime.of(hour +1, 15));
            route.addStopOver(locationMap.get("Zutphen"), LocalTime.of(hour +1, 26), LocalTime.of(hour +1, 28));
            route.addStopOver(locationMap.get("Dieren"), LocalTime.of(hour +1, 37), LocalTime.of(hour +1, 3));
            route.addStopOver(locationMap.get("Arnhem Centraal"), LocalTime.of(hour +1, 49), LocalTime.of(hour +1, 53));
            route.addStopOver(locationMap.get("Nijmegen"), LocalTime.of(hour +2, 6), LocalTime.of(hour +2, 9));
            route.addStopOver(locationMap.get("Oss"), LocalTime.of(hour +2, 24), LocalTime.of(hour +2, 25));
            route.addStopOver(locationMap.get("'s-Hertogenbosch"), LocalTime.of(hour +2, 37), LocalTime.of(hour +2, 42));
            route.addStopOver(locationMap.get("Tilburg"), LocalTime.of(hour +2, 57), LocalTime.of(hour +2, 58));
            route.addStopOver(locationMap.get("Breda"), LocalTime.of(hour +3, 10), LocalTime.of(hour +3, 15));
            route.addStopOver(locationMap.get("Etten-Leur"), LocalTime.of(hour +3, 22), LocalTime.of(hour +3, 22));
            route.addEndPoint(locationMap.get("Roosendaal"), LocalTime.of(hour +3, 33));
            addRoute(route);
        }

        //////////ROUTE Roosendaal-Zwolle (eerste half uur)
        for (int hour = 5; hour <= 20; hour++) { // 20 = 22
            Route route = new Route(locationMap.get("Roosendaal"), LocalTime.of(hour, 27));
            route.addStopOver(locationMap.get("Etten-Leur"), LocalTime.of(hour, 36), LocalTime.of(hour, 36));
            route.addStopOver(locationMap.get("Breda"), LocalTime.of(hour, 45), LocalTime.of(hour, 50));
            route.addStopOver(locationMap.get("Tilburg"), LocalTime.of(hour +1, 2), LocalTime.of( +1, 3));
            route.addStopOver(locationMap.get("'s-Hertogenbosch"), LocalTime.of(hour +1, 19), LocalTime.of(hour +1, 24));
            route.addStopOver(locationMap.get("Oss"), LocalTime.of(hour +1, 35), LocalTime.of(hour +1, 35));
            route.addStopOver(locationMap.get("Nijmegen"), LocalTime.of(hour +1, 52), LocalTime.of(hour +1, 54));
            route.addStopOver(locationMap.get("Arnhem Centraal"), LocalTime.of(hour +2, 7), LocalTime.of(hour +2, 11));
            route.addStopOver(locationMap.get("Dieren"), LocalTime.of(hour +2, 22), LocalTime.of(hour +2, 23));
            route.addStopOver(locationMap.get("Zutphen"), LocalTime.of(hour +2, 32), LocalTime.of(hour +2, 33));
            route.addStopOver(locationMap.get("Deventer"), LocalTime.of(hour +2, 45), LocalTime.of(hour +2, 46));
            route.addStopOver(locationMap.get("Olst"), LocalTime.of(hour +2, 54), LocalTime.of(hour +2, 54));
            route.addStopOver(locationMap.get("Wijhe"), LocalTime.of(hour +3, 0), LocalTime.of(hour +3, 0));
            route.addEndPoint(locationMap.get("Zwolle"), LocalTime.of(hour +3, 11));
            addRoute(route);
        }

        //////////ROUTE Roosendaal-Zwolle (tweede half uur)
        for (int hour = 5; hour <= 20; hour++) { // 20 = 22
            Route route = new Route(locationMap.get("Roosendaal"), LocalTime.of(hour, 57));
            route.addStopOver(locationMap.get("Etten-Leur"), LocalTime.of(hour+1, 6), LocalTime.of(hour+1, 6));
            route.addStopOver(locationMap.get("Breda"), LocalTime.of(hour+1, 15), LocalTime.of(hour+1, 20));
            route.addStopOver(locationMap.get("Tilburg"), LocalTime.of(hour +1, 32), LocalTime.of( +1, 33));
            route.addStopOver(locationMap.get("'s-Hertogenbosch"), LocalTime.of(hour +1, 49), LocalTime.of(hour +1, 54));
            route.addStopOver(locationMap.get("Oss"), LocalTime.of(hour +2, 5), LocalTime.of(hour +2, 5));
            route.addStopOver(locationMap.get("Nijmegen"), LocalTime.of(hour +2, 22), LocalTime.of(hour +2, 24));
            route.addStopOver(locationMap.get("Arnhem Centraal"), LocalTime.of(hour +2, 37), LocalTime.of(hour +2, 41));
            route.addStopOver(locationMap.get("Dieren"), LocalTime.of(hour +2, 52), LocalTime.of(hour +2, 53));
            route.addStopOver(locationMap.get("Zutphen"), LocalTime.of(hour +3, 2), LocalTime.of(hour +3, 3));
            route.addStopOver(locationMap.get("Deventer"), LocalTime.of(hour +3, 15), LocalTime.of(hour +3, 16));
            route.addStopOver(locationMap.get("Olst"), LocalTime.of(hour +3, 24), LocalTime.of(hour +3, 24));
            route.addStopOver(locationMap.get("Wijhe"), LocalTime.of(hour +3, 30), LocalTime.of(hour +3, 30));
            route.addEndPoint(locationMap.get("Zwolle"), LocalTime.of(hour +3, 41));
            addRoute(route);
        }

        //////////ROUTE Amersfoort Centraal-Den Haag (eerste half uur)
        for (int hour = 6; hour <= 21; hour++) {
            Route route = new Route(locationMap.get("Amersfoort Centraal"), LocalTime.of(hour, 10));
            route.addStopOver(locationMap.get("Utrecht Centraal"), LocalTime.of(hour, 23), LocalTime.of(hour, 28));
            route.addStopOver(locationMap.get("Gouda"), LocalTime.of(hour, 46), LocalTime.of(hour, 47));
            route.addEndPoint(locationMap.get("Den Haag Centraal"), LocalTime.of(hour +1, 5));
            addRoute(route);
        }

        //////////ROUTE Amersfoort Centraal-Den Haag (tweede half uur)
        for (int hour = 6; hour <= 21; hour++) {
            Route route = new Route(locationMap.get("Amersfoort Centraal"), LocalTime.of(hour, 40));
            route.addStopOver(locationMap.get("Utrecht Centraal"), LocalTime.of(hour +1, 10), LocalTime.of(hour, 15));
            route.addStopOver(locationMap.get("Gouda"), LocalTime.of(hour +1, 45), LocalTime.of(hour, 46));
            route.addEndPoint(locationMap.get("Den Haag Centraal"), LocalTime.of(hour +2, 16));
            addRoute(route);
        }

        //////////ROUTE Den Haag-Amersfoort Centraal (eerste half uur)
        for (int hour = 5; hour <= 21; hour++) {
            Route route = new Route(locationMap.get("Den Haag Centraal"), LocalTime.of(hour, 55));
            route.addStopOver(locationMap.get("Gouda"), LocalTime.of(hour +1, 13), LocalTime.of(hour +1, 14));
            route.addStopOver(locationMap.get("Utrecht Centraal"), LocalTime.of(hour +1, 32), LocalTime.of(hour +1, 37));
            route.addEndPoint(locationMap.get("Amersfoort Centraal"), LocalTime.of(hour +1, 50));
            addRoute(route);
        }

        //////////ROUTE Den Haag-Amersfoort Centraal (tweede half uur)
        for (int hour = 5; hour <= 21; hour++) {
            Route route = new Route(locationMap.get("Den Haag Centraal"), LocalTime.of(hour +1, 25));
            route.addStopOver(locationMap.get("Gouda"), LocalTime.of(hour +1, 43), LocalTime.of(hour +1, 44));
            route.addStopOver(locationMap.get("Utrecht Centraal"), LocalTime.of(hour +2, 2), LocalTime.of(hour +2, 5));
            route.addEndPoint(locationMap.get("Amersfoort Centraal"), LocalTime.of(hour +2, 18));
            addRoute(route);
        }

        /////////ROUTE Enschede-Schiphol
        for (int hour = 5; hour <= 21; hour++) {
            Route route = new Route(locationMap.get("Enschede"), LocalTime.of(hour, 16));
            route.addStopOver(locationMap.get("Hengelo"), LocalTime.of(hour, 23), LocalTime.of(hour, 24));
            route.addStopOver(locationMap.get("Almelo"), LocalTime.of(hour, 35), LocalTime.of(hour, 36));
            route.addStopOver(locationMap.get("Deventer"), LocalTime.of(hour +1, 0), LocalTime.of(hour +1, 2));
            route.addStopOver(locationMap.get("Apeldoorn"), LocalTime.of(hour +1, 13), LocalTime.of(hour +1, 14));
            route.addStopOver(locationMap.get("Amersfoort Centraal"), LocalTime.of(hour +1, 38), LocalTime.of(hour +1, 41));
            route.addStopOver(locationMap.get("Hilversum"), LocalTime.of(hour +1, 53), LocalTime.of(hour +1, 54));
            route.addStopOver(locationMap.get("Duivendrecht"), LocalTime.of(hour +2, 10), LocalTime.of(hour +2, 10));
            route.addStopOver(locationMap.get("Amsterdam Zuid"), LocalTime.of(hour +2, 15), LocalTime.of(hour +2, 16));
            route.addEndPoint(locationMap.get("Schiphol Airport"), LocalTime.of(hour +2, 22));
            addRoute(route);
        }

        /////////ROUTE Schiphol-Enschede
        for (int hour = 5; hour <= 21; hour++) {
            Route route = new Route(locationMap.get("Schiphol Airport"), LocalTime.of(hour, 38));
            route.addStopOver(locationMap.get("Amsterdam Zuid"), LocalTime.of(hour, 44), LocalTime.of(hour, 45));
            route.addStopOver(locationMap.get("Duivendrecht"), LocalTime.of(hour, 50), LocalTime.of(hour, 50));
            route.addStopOver(locationMap.get("Hilversum"), LocalTime.of(hour +1, 6), LocalTime.of(hour +1, 7));
            route.addStopOver(locationMap.get("Amersfoort Centraal"), LocalTime.of(hour +1, 18), LocalTime.of(hour +1, 22));
            route.addStopOver(locationMap.get("Apeldoorn"), LocalTime.of(hour +1, 46), LocalTime.of(hour +1, 47));
            route.addStopOver(locationMap.get("Deventer"), LocalTime.of(hour +1, 58), LocalTime.of(hour +2, 0));
            route.addStopOver(locationMap.get("Almelo"), LocalTime.of(hour +2, 24), LocalTime.of(hour +2, 26));
            route.addStopOver(locationMap.get("Hengelo"), LocalTime.of(hour +2, 37), LocalTime.of(hour +2, 38));
            route.addEndPoint(locationMap.get("Enschede"), LocalTime.of(hour +2, 45));
            addRoute(route);
        }
    }

    //////////Methods specific to data


}


