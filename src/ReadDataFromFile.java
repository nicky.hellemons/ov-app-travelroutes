import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;

import java.io.FileReader;

public class ReadDataFromFile {

    private JSONArray userArray;
    private int userID;
    private String firstName;
    private String lastName;
    private String password;
    private String email;
    private JSONObject users;
    private Object obj;

    public ReadDataFromFile() {
        try {
            String filePath = "src/JSONFiles/users.json";

            JSONParser jsonparser = new JSONParser();
            FileReader reader = new FileReader(filePath);
            obj = jsonparser.parse(reader);
            JSONObject empJSONObj = (JSONObject) obj;
            userArray = (JSONArray) empJSONObj.get("users");


            for (Object o : userArray) {
                users = (JSONObject) o;
                String userIDString = (String) users.get("userid");
                userID = Integer.parseInt(userIDString);
                firstName = (String) users.get("firstName");
                lastName = (String) users.get("lastName");
                password = (String) users.get("password");
                email = (String) users.get("email");
            }
        } catch (Exception ignored) {}
    }


    public Object getObj() {
        return obj;
    }

    public JSONArray getUserArray() {
        return userArray;
    }

    public int getUserID() {
        return userID;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public String getPassword() {
        return password;
    }

    public String getEmail() {
        return email;
    }
}