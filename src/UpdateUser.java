import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

import java.io.FileWriter;
import java.io.IOException;

public class UpdateUser {


    private static FileWriter file;
    ReadDataFromFile data = new ReadDataFromFile();
    private int userID;
    private JSONArray userArray;

    static public void CrunchifyLog(String str) {
        System.out.println(str);
    }

    public void updateUser(String userID, String updateValue, String newValue) {

        try {

            this.userID = Integer.parseInt(userID);
            JSONObject empJSONObj = (JSONObject) data.getObj();
            userArray = (JSONArray) empJSONObj.get("users");
            JSONObject user = (JSONObject) userArray.get(this.userID - 1);
            user.put(updateValue, newValue);

            file = new FileWriter("src/JSONFiles/users.json");
            file.write(empJSONObj.toJSONString());
            CrunchifyLog("Successfully Updated JSON Object to File...");
            System.out.println("user updated");

        } catch (Exception e) {
            e.printStackTrace();
        } finally {

            try {
                file.flush();
                file.close();
            } catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        }
    }


}
