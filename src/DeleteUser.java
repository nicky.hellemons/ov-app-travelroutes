import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

import java.io.FileWriter;
import java.io.IOException;

public class DeleteUser {

    private static FileWriter file;
    ReadDataFromFile data = new ReadDataFromFile();
    private JSONArray userArray;

    static public void CrunchifyLog(String str) {
        System.out.println(str);
    }

    public void deleteUser(int deleteUserIndex) {

        //deze methode gebruiken in andere class om gebuiker te verwijderen
//        new DeleteUser().DeleteUser(data.getUserArray().size() - 1);
        try {

            JSONObject empJSONObj = (JSONObject) data.getObj();
            userArray = (JSONArray) empJSONObj.get("users");
            userArray.remove(deleteUserIndex);

            file = new FileWriter("src/JSONFiles/users.json");
            file.write(empJSONObj.toJSONString());
            CrunchifyLog("Successfully Copied JSON Object to File...");


        } catch (Exception e) {
            e.printStackTrace();
        } finally {

            try {
                file.flush();
                file.close();
            } catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        }
    }


}
