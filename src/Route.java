import java.time.LocalTime;
import java.util.ArrayList;

//Nicky & Ivo
public class Route {
    private ArrayList<Stopover> stopOvers = new ArrayList<>();

    public Route(Location location, LocalTime departure) {
        var stopOver = new Stopover(location.getLocationName(), null, departure);
        stopOvers.add(stopOver);
    }

    public void addStopOver(Location location, LocalTime arrival, LocalTime departure) {
        var stopOver = new Stopover(location.getLocationName(), arrival, departure);
        stopOvers.add(stopOver);
    }

    public void addEndPoint(Location location, LocalTime arrival) {
        var stopOver = new Stopover(location.getLocationName(), arrival, null);
        stopOvers.add(stopOver);
    }

    public String getKey() {
        String key = stopOvers.get(0).getLocationName();

        for (int i = 1; i < stopOvers.size(); i++) {
            key += "-";
            key += stopOvers.get(i).getLocationName();
        }

        return key;
    }

    public ArrayList<Stopover> getStopOvers() {
        return stopOvers;
    }

    public Integer indexOf(Stopover stopover) {
        return stopOvers.indexOf(stopover) + 1;
    }

    public LocalTime getDeparture(Location location) {
        for (var e : stopOvers) {
            if (e.getLocationName().equals(location.getLocationName())) {
                return e.getDeparture();
            }
        }
        return null;
    }

    public LocalTime getArrival(Location location) {
        for (var e: stopOvers) {
            if (e.getLocationName().equals(location.getLocationName())) {
                return e.getArrival();
            }
        }
        return null;
    }

}

