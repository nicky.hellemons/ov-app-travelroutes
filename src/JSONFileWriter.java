import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

import java.io.FileWriter;
import java.io.IOException;

public class JSONFileWriter {

    private static FileWriter file;
    ReadDataFromFile data = new ReadDataFromFile();
    private JSONArray userArray;
    private String userID;


    private boolean newUserStatus;

    static public void CrunchifyLog(String str) {
        System.out.println(str);
    }

    public boolean isNewUserStatus() {
        return newUserStatus;
    }

    public void jsonfilewriter(String userName, String firstName, String lastName, String password, String email, String avatar) {

        for (int i = 0; i < data.getUserArray().size(); i++) {
            JSONObject users = (JSONObject) data.getUserArray().get(i);

            if ((email.equals(users.get("email")) || userName.equals(users.get("username")))) {
                newUserStatus = false;

                break;
            } else {
                newUserStatus = true;

            }

        }

        if (!newUserStatus) {
            System.out.println(isNewUserStatus());
        }

        if (newUserStatus) {
            System.out.println(isNewUserStatus());
        }

        if (newUserStatus) {
            try {
                System.out.println(isNewUserStatus());
                System.out.println("succesvol aangemaakt");
                userID = String.valueOf(data.getUserArray().size() + 1);
                JSONObject obj = new JSONObject();
                obj.put("userid", userID);
                obj.put("username", userName);
                obj.put("firstName", firstName);
                obj.put("lastName", lastName);
                obj.put("password", password);
                obj.put("email", email);
                obj.put("avatar", avatar);
                JSONObject empJSONObj = (JSONObject) data.getObj();
                userArray = (JSONArray) empJSONObj.get("users");
                userArray.add(obj);
                file = new FileWriter("src/JSONFiles/users.json");
                file.write(empJSONObj.toJSONString());
                CrunchifyLog("Successfully Copied JSON Object to File...");
                CrunchifyLog("\nJSON Object: " + obj);


            } catch (Exception e) {
                e.printStackTrace();
            } finally {

                try {
                    file.flush();
                    file.close();
                } catch (IOException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
            }
        }
    }


}
