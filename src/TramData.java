import java.time.LocalTime;

public class TramData extends Data {
    public TramData() {
        Location location = new Location("Matterhorn", new Coordinates(52.35376092522255, 4.774621987174505));
        locationMap.put("Matterhorn", location);

        location = new Location("Pilatus", new Coordinates(52.35298913938665, 4.776534917305436));
        locationMap.put("Pilatus", location);

        location = new Location("Inaristraat", new Coordinates(52.35043339699038, 4.782893978257211));
        locationMap.put("Inaristraat", location);

        location = new Location("Ecuplein", new Coordinates(52.34974131042801, 4.788732852563252));
        locationMap.put("Ecuplein", location);

        location = new Location("Baden Powellweg", new Coordinates(52.3509853643729, 4.794930422036631));
        locationMap.put("Baden Powellweg", location);

        location = new Location("Hoekenes", new Coordinates(52.35251237303969, 4.802510637361094));
        locationMap.put("Hoekenes", location);

        location = new Location("Louis Davidsstraat", new Coordinates(52.353909305745745, 4.809019100932804));
        locationMap.put("Louis Davidsstraat", location);

        location = new Location("Meer en Vaart", new Coordinates(52.356475112651715, 4.810198756964458));
        locationMap.put("Meer en Vaart", location);

        location = new Location("Johan Huizingalaan", new Coordinates(52.3577299370836, 4.8258308985639475));
        locationMap.put("Johan Huizingalaan", location);

        location = new Location("Station Lelylaan", new Coordinates(52.35795312244207, 4.833938514641535));
        locationMap.put("Station Lelylaan", location);

        location = new Location("Derkinderenstraat", new Coordinates(52.35797976997028, 4.8399109640221365));
        locationMap.put("Derkinderenstraat", location);

        location = new Location("Surinameplein", new Coordinates(52.358090490263095, 4.851032813042791));
        locationMap.put("Surinameplein", location);

        location = new Location("Rhijnvis Feithstraat", new Coordinates(52.35896574000236, 4.860196651684989));
        locationMap.put("Rhijnvis Feithstraat", location);

        location = new Location("J.P. Heijestraat", new Coordinates(52.36047902427128, 4.86609061451422));
        locationMap.put("J.P. Heijestraat", location);

        location = new Location("1e Con. Huygenstraat", new Coordinates(52.36254349278472, 4.874657215159087));
        locationMap.put("1e Con. Huygenstraat", location);

        location = new Location("Leidseplein", new Coordinates(52.36319918251367, 4.88096116191975));
        locationMap.put("Leidseplein" ,location);

        location = new Location("Spiegelgracht", new Coordinates(52.360944518507836, 4.887229085701726));
        locationMap.put("Spiegelgracht", location);

        location = new Location("Vijzelgracht", new Coordinates(52.360010871679954, 4.890934664362703));
        locationMap.put("Vijzelgracht", location);

        location = new Location("Frederiksplein", new Coordinates(52.35953574313031, 4.8988216362092745));
        locationMap.put("Frederiksplein", location);

        location = new Location("Weesperplein", new Coordinates(52.361193631807836, 4.90793745972748));
        locationMap.put("Weesperplein", location);

        location = new Location("K. 's-Gravesanderstr.", new Coordinates(52.36244679461452, 4.9140848511359865));
        locationMap.put("K. 's-Gravesanderstr.", location);

        location = new Location("Beukenweg", new Coordinates(52.358329230151895, 4.917132647131544));
        locationMap.put("Beukenweg", location);

        location = new Location("Linnaeusstraat", new Coordinates(52.35988426799165, 4.924872550369388));
        locationMap.put("Linnaeusstraat", location);

        location = new Location("Dapperstraat", new Coordinates(52.36102895279551, 4.929294971632878));
        locationMap.put("Dapperstraat", location);

        location = new Location("Muiderpoortstation", new Coordinates(52.36120645195584, 4.933425112842298));
        locationMap.put("Muiderpoortstation", location);

        location = new Location("Oudenaardeplantsoen", new Coordinates(52.34520305301754, 4.804289659170173));
        locationMap.put("Oudenaardeplantsoen", location);

        location = new Location("Centrum Nieuwe Sloten", new Coordinates(52.346435390196966, 4.81133428824207));
        locationMap.put("Centrum Nieuwe Sloten", location);

        location = new Location("Laan v.Vlaanderen", new Coordinates(52.34641058282087, 4.815563195100252));
        locationMap.put("Laan v.Vlaanderen", location);

        location = new Location("Louwesweg", new Coordinates(52.3468041066521, 4.824794511732247));
        locationMap.put("Louwesweg", location);

        location = new Location("Johan Huizingalaan", new Coordinates(52.34892723516483, 4.82752295150084));
        locationMap.put("Johan Huizingalaan", location);

        location = new Location("Heemstedestraat", new Coordinates(52.35231360409391, 4.834368813585391));
        locationMap.put("Heemstedestraat", location);

        location = new Location("Delflandlaan", new Coordinates(52.35161404902888, 4.839961648692585));
        locationMap.put("Delflandlaan", location);

        location = new Location("Westlandgracht", new Coordinates(52.3516657396245, 4.845084798240842));
        locationMap.put("Westlandgracht", location);

        location = new Location("Hoofddorpplein", new Coordinates(52.35124846566442, 4.849508110249808));
        locationMap.put("Hoofddorpplein", location);

        location = new Location("Amstelveenseweg", new Coordinates(52.33857248483589, 4.857645593638401));
        locationMap.put("Amstelveenseweg", location);

        location = new Location("Valeriusplein", new Coordinates(52.35318408553042, 4.863081191350177));
        locationMap.put("Valeriusplein", location);

        location = new Location("Corn. Schuytstraat", new Coordinates(52.35611248725141, 4.870680230411595));
        locationMap.put("Corn. Schuytstraat", location);

        location = new Location("Museumplein", new Coordinates(52.35889041185098, 4.881169377419265));
        locationMap.put("Museumplein", location);

        location = new Location("Prinsengracht", new Coordinates(52.3650899699131, 4.8844513808242604));
        locationMap.put("Prinsengracht", location);

        location = new Location("Keizersgracht", new Coordinates(52.36396810981263, 4.898316982896791));
        locationMap.put("Keizersgracht", location);

        location = new Location("Koningsplein", new Coordinates(52.3676050752837, 4.889483587361067));
        locationMap.put("Koningsplein", location);

        location = new Location("Dam", new Coordinates(52.373453142131694, 4.893436538117884));
        locationMap.put("Dam", location);

        location = new Location("Nieuwezijds Kolk", new Coordinates(52.37630814374296, 4.893703932860791));
        locationMap.put("Nieuwezijds Kolk", location);

        location = new Location("Centraal Station", new Coordinates(52.37820335545336, 4.899340331395674));
        locationMap.put("Centraal Station", location);

        location = new Location("Flevopark", new Coordinates(52.365325731169335, 4.950846380235564));
        locationMap.put("Flevopark", location);

        location = new Location("Insulindeweg", new Coordinates(52.36214309865099, 4.94192209027215));
        locationMap.put("Insulindeweg", location);

        location = new Location("Soembawastraat", new Coordinates(52.3621644351036, 4.943454242627414));
        locationMap.put("Soembawastraat", location);

        location = new Location("Molukkenstraat", new Coordinates(52.361824668616954, 4.940499946510193));
        locationMap.put("Molukkenstraat", location);

        location = new Location("Muiderpoortstation", new Coordinates(52.360889601201535, 4.93263382354232));
        locationMap.put("Muiderpoortstation", location);

        location = new Location("Camperstraat", new Coordinates(52.3584123715554, 4.913699417073056));
        locationMap.put("Camperstraat", location);

        location = new Location("Wibautstraat", new Coordinates(52.35752866427795, 4.909512593155682));
        locationMap.put("Wibautstraat", location);

        location = new Location("Van Woustraat", new Coordinates(52.355198282586606, 4.901860048501473));
        locationMap.put("Van Woustraat", location);

        location = new Location("2e v.d.Helststraat", new Coordinates(52.35326113770634, 4.8942221162908925));
        locationMap.put("2e v.d.Helststraat", location);

        location = new Location("De Pijp", new Coordinates(52.35273250538512, 4.890171906520141));
        locationMap.put("De Pijp", location);

        location = new Location("Roelof Hartplein", new Coordinates(52.35284709100283, 4.882438241642234));
        locationMap.put("Roelof Hartplein", location);

        location = new Location("Concertgebouw", new Coordinates(52.356684434220796, 4.87964729403298));
        locationMap.put("Concertgebouw", location);

        location = new Location("Van Baerlestraat", new Coordinates(52.35838602946617, 4.879415213228504));
        locationMap.put("Van Baerlestraat", location);

        location = new Location("Overtoom", new Coordinates(52.36321509359267, 4.875018850194299));
        locationMap.put("Overtoom", location);

        location = new Location("Kinkerstraat", new Coordinates(52.36724901243356, 4.872108708793547));
        locationMap.put("Kinkerstraat", location);

        location = new Location("De Clercqstraat", new Coordinates(52.371320624161264, 4.870634006546343));
        locationMap.put("De Clercqstraat", location);

        location = new Location("Hugo de Grootplein", new Coordinates(52.37466977012476, 4.872747588273263));
        locationMap.put("Hugo de Grootplein", location);

        location = new Location("F.Hendrikplantsoen", new Coordinates(52.378533550658055, 4.875410861903288));
        locationMap.put("F.Hendrikplantsoen", location);

        location = new Location("Nassaukade", new Coordinates(52.3810403397703, 4.879093763841547));
        locationMap.put("Nassaukade", location);

        location = new Location("De Wittenkade", new Coordinates(52.38324215136997, 4.877107977279112));
        locationMap.put("De Wittenkade", location);

        location = new Location("Van L.Stirumstraat", new Coordinates(52.38429297726901, 4.874985553988873));
        locationMap.put("Van L.Stirumstraat", location);

        location = new Location("Van Hallstraat", new Coordinates(52.3841619039718, 4.8698928224320435));
        locationMap.put("Van Hallstraat", location);

        location = new Location("Drentepark", new Coordinates(52.33610119399865, 4.889252666627312));
        locationMap.put("Drentepark", location);

        location = new Location("Station RAI", new Coordinates(52.33683045927534, 4.890696250949636));
        locationMap.put("Station RAI", location);

        location = new Location("Europaplein", new Coordinates(52.34019885391952, 4.892183701774148));
        locationMap.put("Europaplein", location);

        location = new Location("Dintelstraat", new Coordinates(52.341915445790406, 4.893288250623906));
        locationMap.put("Dintelstraat", location);

        location = new Location("Maasstraat", new Coordinates(52.342880724590955, 4.896459589458051));
        locationMap.put("Maasstraat", location);

        location = new Location("Waalstraat", new Coordinates(52.344519578043, 4.900841387655655));
        locationMap.put("Waalstraat", location);

        location = new Location("Victorieplein", new Coordinates(52.34622198519035, 4.905249530819851));
        locationMap.put("Victorieplein", location);

        location = new Location("Amstelkade", new Coordinates(52.348970838093074, 4.904192873737164));
        locationMap.put("Amstelkade", location);

        location = new Location("Lutmastraat", new Coordinates(52.35161832849963, 4.9032314862409985));
        locationMap.put("Lutmastraat", location);

        location = new Location("Ceintuurbaan", new Coordinates(52.354800806643446, 4.901263917884356));
        locationMap.put("Ceintuurbaan", location);

        location = new Location("Stadhouderskade", new Coordinates(52.35811657158197, 4.899259913092726));
        locationMap.put("Stadhouderskade", location);

        location = new Location("Rembrandtplein", new Coordinates(52.366263403783535, 4.896821249425808));
        locationMap.put("Rembrandtplein", location);

        location = new Location("Rokin", new Coordinates(52.369699013019876, 4.892282592064364));
        locationMap.put("Rokin", location);

        location = new Location("Amstelveen Stadshart", new Coordinates(52.30138444679937, 4.867351058138653));
        locationMap.put("Amstelveen Stadshart", location);

        location = new Location("Onderuit", new Coordinates(52.30825368133842, 4.872581660417892));
        locationMap.put("Onderuit", location);

        location = new Location("Zonnestein", new Coordinates(52.31187674760922, 4.872394010720452));
        locationMap.put("Zonnestein", location);

        location = new Location("Kronenburg", new Coordinates(52.316019572371, 4.870452347165598));
        locationMap.put("Kronenburg", location);

        location = new Location("Uilenstede", new Coordinates(52.321870586764796, 4.869213585733472));
        locationMap.put("Uilenstede", location);

        location = new Location("Van Boshuizenstraat", new Coordinates(52.32546937219762, 4.8689830573489035));
        locationMap.put("Van Boshuizenstraat", location);

        location = new Location("A.J. Ernststraat", new Coordinates(52.331506142334305, 4.868768460781734));
        locationMap.put("A.J. Ernststraat", location);

        location = new Location("Parnassusweg", new Coordinates(52.33812484794765, 4.868745732277077));
        locationMap.put("Parnassusweg", location);

        location = new Location("Station Zuid", new Coordinates(52.340838169199, 4.8739224678247295));
        locationMap.put("Station Zuid", location);

        location = new Location("Prinses Irenestraat", new Coordinates(52.34286597870091, 4.8767634049097985));
        locationMap.put("Prinses Irenestraat", location);

        location = new Location("Gerrit v.d. Veenstraat", new Coordinates(52.34907915035701, 4.877332524634532));
        locationMap.put("Gerrit v.d. Veenstraat", location);

        location = new Location("Elandsgracht", new Coordinates(52.368344237061436, 4.878321148425127));
        locationMap.put("Elandsgracht", location);

        location = new Location("Rozengracht", new Coordinates(52.37244119989589, 4.875912306557144));
        locationMap.put("Rozengracht", location);

        location = new Location("Bloemgracht", new Coordinates(52.37413434295955, 4.876216182116327));
        locationMap.put("Bloemgracht", location);

        location = new Location("Marnixplein", new Coordinates(52.37760822966957, 4.878515601639089));
        locationMap.put("Marnixplein", location);

        location = new Location("Nw. Willemsstraat", new Coordinates(52.381434166973115, 4.881014552940185));
        locationMap.put("Nw. Willemsstraat", location);

        location = new Location("Eerste Marnixdwarsstraat", new Coordinates(52.383084525575015, 4.8820156052892845));
        locationMap.put("Eerste Marnixdwarsstraat", location);

        location = new Location("Stadionweg", new Coordinates(52.34692561838709, 4.876807466950763));
        locationMap.put("Stadionweg", location);

        location = new Location("Oranjebaan", new Coordinates(52.30324269547013, 4.87192932047871));
        locationMap.put("Oranjebaan", location);

        for (int hour = 5; hour <= 21; hour++) {
            Route route = new Route(locationMap.get("Eerste Marnixdwarsstraat"), LocalTime.of(hour, 49));
            route.addStopOver(locationMap.get("Nw. Willemsstraat"), LocalTime.of(hour, 49), LocalTime.of(hour, 49));
            route.addStopOver(locationMap.get("Marnixplein"), LocalTime.of(hour, 51), LocalTime.of(hour, 51));
            route.addStopOver(locationMap.get("Bloemgracht"), LocalTime.of(hour, 52), LocalTime.of(hour, 52));
            route.addStopOver(locationMap.get("Rozengracht"), LocalTime.of(hour, 53), LocalTime.of(hour, 53));
            route.addStopOver(locationMap.get("Elandsgracht"), LocalTime.of(hour, 55), LocalTime.of(hour, 55));
            route.addStopOver(locationMap.get("Leidseplein"), LocalTime.of(hour, 57), LocalTime.of(hour, 57));
            route.addStopOver(locationMap.get("Museumplein"), LocalTime.of(hour + 1, 1), LocalTime.of(hour + 1, 1));
            route.addStopOver(locationMap.get("Concertgebouw"), LocalTime.of(hour + 1, 2), LocalTime.of(hour + 1, 2));
            route.addStopOver(locationMap.get("Roelof Hartplein"), LocalTime.of(hour + 1, 3), LocalTime.of(hour + 1, 3));
            route.addStopOver(locationMap.get("Gerrit v.d. Veenstraat"), LocalTime.of(hour + 1, 5), LocalTime.of(hour + 1, 5));
            route.addStopOver(locationMap.get("Stadionweg"), LocalTime.of(hour + 1, 6), LocalTime.of(hour + 1, 6));
            route.addStopOver(locationMap.get("Prinses Irenestraat"), LocalTime.of(hour + 1, 8), LocalTime.of(hour + 1, 8));
            route.addStopOver(locationMap.get("Station Zuid"), LocalTime.of(hour + 1, 10), LocalTime.of(hour + 1, 10));
            route.addStopOver(locationMap.get("Parnassusweg"), LocalTime.of(hour + 1, 10), LocalTime.of(hour + 1, 10));
            route.addStopOver(locationMap.get("A.J. Ernststraat"), LocalTime.of(hour + 1, 12), LocalTime.of(hour + 1, 12));
            route.addStopOver(locationMap.get("Van Boshuizenstraat"), LocalTime.of(hour + 1, 14), LocalTime.of(hour + 1, 14));
            route.addStopOver(locationMap.get("Uilenstede"), LocalTime.of(hour + 1, 14), LocalTime.of(hour + 1, 14));
            route.addStopOver(locationMap.get("Kronenburg"), LocalTime.of(hour + 1, 16), LocalTime.of(hour + 1, 16));
            route.addStopOver(locationMap.get("Zonnestein"), LocalTime.of(hour + 1, 17), LocalTime.of(hour + 1, 17));
            route.addStopOver(locationMap.get("Onderuit"), LocalTime.of(hour + 1, 18), LocalTime.of(hour + 1, 18));
            route.addStopOver(locationMap.get("Oranjebaan"), LocalTime.of(hour + 1, 20), LocalTime.of(hour + 1, 20));
            route.addEndPoint(locationMap.get("Amstelveen Stadshart"), LocalTime.of(hour + 1, 22));
            addRoute(route);

            route = new Route(locationMap.get("Eerste Marnixdwarsstraat"), LocalTime.of(hour + 1, 4));
            route.addStopOver(locationMap.get("Nw. Willemsstraat"), LocalTime.of(hour + 1, 4), LocalTime.of(hour + 1, 4));
            route.addStopOver(locationMap.get("Marnixplein"), LocalTime.of(hour + 1, 6), LocalTime.of(hour + 1, 6));
            route.addStopOver(locationMap.get("Bloemgracht"), LocalTime.of(hour + 1, 7), LocalTime.of(hour + 1, 7));
            route.addStopOver(locationMap.get("Rozengracht"), LocalTime.of(hour + 1, 8), LocalTime.of(hour + 1, 8));
            route.addStopOver(locationMap.get("Elandsgracht"), LocalTime.of(hour + 1, 10), LocalTime.of(hour + 1, 10));
            route.addStopOver(locationMap.get("Leidseplein"), LocalTime.of(hour + 1, 12), LocalTime.of(hour + 1, 12));
            route.addStopOver(locationMap.get("Museumplein"), LocalTime.of(hour + 1, 16), LocalTime.of(hour + 1, 16));
            route.addStopOver(locationMap.get("Concertgebouw"), LocalTime.of(hour + 1, 17), LocalTime.of(hour + 1, 17));
            route.addStopOver(locationMap.get("Roelof Hartplein"), LocalTime.of(hour + 1, 18), LocalTime.of(hour + 1, 18));
            route.addStopOver(locationMap.get("Gerrit v.d. Veenstraat"), LocalTime.of(hour + 1, 20), LocalTime.of(hour + 1, 20));
            route.addStopOver(locationMap.get("Stadionweg"), LocalTime.of(hour + 1, 21), LocalTime.of(hour + 1, 21));
            route.addStopOver(locationMap.get("Prinses Irenestraat"), LocalTime.of(hour + 1, 23), LocalTime.of(hour + 1, 23));
            route.addStopOver(locationMap.get("Station Zuid"), LocalTime.of(hour + 1, 25), LocalTime.of(hour + 1, 25));
            route.addStopOver(locationMap.get("Parnassusweg"), LocalTime.of(hour + 1, 25), LocalTime.of(hour + 1, 25));
            route.addStopOver(locationMap.get("A.J. Ernststraat"), LocalTime.of(hour + 1, 27), LocalTime.of(hour + 1, 27));
            route.addStopOver(locationMap.get("Van Boshuizenstraat"), LocalTime.of(hour + 1, 29), LocalTime.of(hour + 1, 29));
            route.addStopOver(locationMap.get("Uilenstede"), LocalTime.of(hour + 1, 29), LocalTime.of(hour + 1, 29));
            route.addStopOver(locationMap.get("Kronenburg"), LocalTime.of(hour + 1, 31), LocalTime.of(hour + 1, 31));
            route.addStopOver(locationMap.get("Zonnestein"), LocalTime.of(hour + 1, 32), LocalTime.of(hour + 1, 32));
            route.addStopOver(locationMap.get("Onderuit"), LocalTime.of(hour + 1, 33), LocalTime.of(hour + 1, 33));
            route.addStopOver(locationMap.get("Oranjebaan"), LocalTime.of(hour + 1, 35), LocalTime.of(hour + 1, 35));
            route.addEndPoint(locationMap.get("Amstelveen Stadshart"), LocalTime.of(hour + 1, 37));
            addRoute(route);

            route = new Route(locationMap.get("Eerste Marnixdwarsstraat"), LocalTime.of(hour + 1, 19));
            route.addStopOver(locationMap.get("Nw. Willemsstraat"), LocalTime.of(hour + 1, 19), LocalTime.of(hour + 1, 19));
            route.addStopOver(locationMap.get("Marnixplein"), LocalTime.of(hour + 1, 21), LocalTime.of(hour + 1, 21));
            route.addStopOver(locationMap.get("Bloemgracht"), LocalTime.of(hour + 1, 22), LocalTime.of(hour + 1, 22));
            route.addStopOver(locationMap.get("Rozengracht"), LocalTime.of(hour + 1, 23), LocalTime.of(hour + 1, 23));
            route.addStopOver(locationMap.get("Elandsgracht"), LocalTime.of(hour + 1, 25), LocalTime.of(hour + 1, 25));
            route.addStopOver(locationMap.get("Leidseplein"), LocalTime.of(hour + 1, 27), LocalTime.of(hour + 1, 27));
            route.addStopOver(locationMap.get("Museumplein"), LocalTime.of(hour + 1, 31), LocalTime.of(hour + 1, 31));
            route.addStopOver(locationMap.get("Concertgebouw"), LocalTime.of(hour + 1, 32), LocalTime.of(hour + 1, 32));
            route.addStopOver(locationMap.get("Roelof Hartplein"), LocalTime.of(hour + 1, 33), LocalTime.of(hour + 1, 33));
            route.addStopOver(locationMap.get("Gerrit v.d. Veenstraat"), LocalTime.of(hour + 1, 35), LocalTime.of(hour + 1, 35));
            route.addStopOver(locationMap.get("Stadionweg"), LocalTime.of(hour + 1, 36), LocalTime.of(hour + 1, 36));
            route.addStopOver(locationMap.get("Prinses Irenestraat"), LocalTime.of(hour + 1, 38), LocalTime.of(hour + 1, 38));
            route.addStopOver(locationMap.get("Station Zuid"), LocalTime.of(hour + 1, 40), LocalTime.of(hour + 1, 40));
            route.addStopOver(locationMap.get("Parnassusweg"), LocalTime.of(hour + 1, 40), LocalTime.of(hour + 1, 40));
            route.addStopOver(locationMap.get("A.J. Ernststraat"), LocalTime.of(hour + 1, 42), LocalTime.of(hour + 1, 42));
            route.addStopOver(locationMap.get("Van Boshuizenstraat"), LocalTime.of(hour + 1, 44), LocalTime.of(hour + 1, 44));
            route.addStopOver(locationMap.get("Uilenstede"), LocalTime.of(hour + 1, 44), LocalTime.of(hour + 1, 44));
            route.addStopOver(locationMap.get("Kronenburg"), LocalTime.of(hour + 1, 46), LocalTime.of(hour + 1, 46));
            route.addStopOver(locationMap.get("Zonnestein"), LocalTime.of(hour + 1, 47), LocalTime.of(hour + 1, 47));
            route.addStopOver(locationMap.get("Onderuit"), LocalTime.of(hour + 1, 48), LocalTime.of(hour + 1, 48));
            route.addStopOver(locationMap.get("Oranjebaan"), LocalTime.of(hour + 1, 50), LocalTime.of(hour + 1, 50));
            route.addEndPoint(locationMap.get("Amstelveen Stadshart"), LocalTime.of(hour + 1, 52));
            addRoute(route);

            route = new Route(locationMap.get("Eerste Marnixdwarsstraat"), LocalTime.of(hour + 1, 34));
            route.addStopOver(locationMap.get("Nw. Willemsstraat"), LocalTime.of(hour + 1, 34), LocalTime.of(hour + 1, 34));
            route.addStopOver(locationMap.get("Marnixplein"), LocalTime.of(hour + 1, 36), LocalTime.of(hour + 1, 36));
            route.addStopOver(locationMap.get("Bloemgracht"), LocalTime.of(hour + 1, 37), LocalTime.of(hour + 1, 37));
            route.addStopOver(locationMap.get("Rozengracht"), LocalTime.of(hour + 1, 38), LocalTime.of(hour + 1, 38));
            route.addStopOver(locationMap.get("Elandsgracht"), LocalTime.of(hour + 1, 40), LocalTime.of(hour + 1, 40));
            route.addStopOver(locationMap.get("Leidseplein"), LocalTime.of(hour + 1, 42), LocalTime.of(hour + 1, 42));
            route.addStopOver(locationMap.get("Museumplein"), LocalTime.of(hour + 1, 46), LocalTime.of(hour + 1, 46));
            route.addStopOver(locationMap.get("Concertgebouw"), LocalTime.of(hour + 1, 47), LocalTime.of(hour + 1, 47));
            route.addStopOver(locationMap.get("Roelof Hartplein"), LocalTime.of(hour + 1, 48), LocalTime.of(hour + 1, 48));
            route.addStopOver(locationMap.get("Gerrit v.d. Veenstraat"), LocalTime.of(hour + 1, 50), LocalTime.of(hour + 1, 50));
            route.addStopOver(locationMap.get("Stadionweg"), LocalTime.of(hour + 1, 51), LocalTime.of(hour + 1, 51));
            route.addStopOver(locationMap.get("Prinses Irenestraat"), LocalTime.of(hour + 1, 53), LocalTime.of(hour + 1, 53));
            route.addStopOver(locationMap.get("Station Zuid"), LocalTime.of(hour + 1, 55), LocalTime.of(hour + 1, 55));
            route.addStopOver(locationMap.get("Parnassusweg"), LocalTime.of(hour + 1, 55), LocalTime.of(hour + 1, 55));
            route.addStopOver(locationMap.get("A.J. Ernststraat"), LocalTime.of(hour + 1, 57), LocalTime.of(hour + 1, 57));
            route.addStopOver(locationMap.get("Van Boshuizenstraat"), LocalTime.of(hour + 1, 59), LocalTime.of(hour + 1, 59));
            route.addStopOver(locationMap.get("Uilenstede"), LocalTime.of(hour + 1, 59), LocalTime.of(hour + 1, 59));
            route.addStopOver(locationMap.get("Kronenburg"), LocalTime.of(hour + 1, 1), LocalTime.of(hour + 1, 1));
            route.addStopOver(locationMap.get("Zonnestein"), LocalTime.of(hour + 1, 2), LocalTime.of(hour + 1, 2));
            route.addStopOver(locationMap.get("Onderuit"), LocalTime.of(hour + 1, 3), LocalTime.of(hour + 1, 3));
            route.addStopOver(locationMap.get("Oranjebaan"), LocalTime.of(hour + 1, 5), LocalTime.of(hour + 1, 5));
            route.addEndPoint(locationMap.get("Amstelveen Stadshart"), LocalTime.of(hour + 1, 7));
            addRoute(route);
        }

        for (int hour = 5; hour <= 21; hour++) {
            Route route = new Route(locationMap.get("Amstelveen Stadshart"), LocalTime.of(hour, 52));
            route.addStopOver(locationMap.get("Oranjebaan"), LocalTime.of(hour, 53), LocalTime.of(hour, 53));
            route.addStopOver(locationMap.get("Onderuit"), LocalTime.of(hour, 54), LocalTime.of(hour, 54));
            route.addStopOver(locationMap.get("Zonnestein"), LocalTime.of(hour, 55), LocalTime.of(hour, 55));
            route.addStopOver(locationMap.get("Kronenburg"), LocalTime.of(hour, 56), LocalTime.of(hour, 56));
            route.addStopOver(locationMap.get("Uilenstede"), LocalTime.of(hour, 58), LocalTime.of(hour, 58));
            route.addStopOver(locationMap.get("Van Boshuizenstraat"), LocalTime.of(hour, 59), LocalTime.of(hour, 59));
            route.addStopOver(locationMap.get("A.J. Ernststraat"), LocalTime.of(hour + 1, 1), LocalTime.of(hour + 1, 1));
            route.addStopOver(locationMap.get("Parnassusweg"), LocalTime.of(hour + 1, 2), LocalTime.of(hour + 1, 2));
            route.addStopOver(locationMap.get("Station Zuid"), LocalTime.of(hour + 1, 4), LocalTime.of(hour + 1, 4));
            route.addStopOver(locationMap.get("Prinses Irenestraat"), LocalTime.of(hour + 1, 5), LocalTime.of(hour + 1, 5));
            route.addStopOver(locationMap.get("Stadionweg"), LocalTime.of(hour + 1, 6), LocalTime.of(hour + 1, 6));
            route.addStopOver(locationMap.get("Gerrit v.d. Veenstraat"), LocalTime.of(hour + 1, 7), LocalTime.of(hour + 1, 7));
            route.addStopOver(locationMap.get("Roelof Hartplein"), LocalTime.of(hour + 1, 9), LocalTime.of(hour + 1, 9));
            route.addStopOver(locationMap.get("Concertgebouw"), LocalTime.of(hour + 1, 11), LocalTime.of(hour + 1, 11));
            route.addStopOver(locationMap.get("Museumplein"), LocalTime.of(hour + 1, 13), LocalTime.of(hour + 1, 13));
            route.addStopOver(locationMap.get("Leidseplein"), LocalTime.of(hour + 1, 17), LocalTime.of(hour + 1, 17));
            route.addStopOver(locationMap.get("Elandsgracht"), LocalTime.of(hour + 1, 19), LocalTime.of(hour + 1, 19));
            route.addStopOver(locationMap.get("Rozengracht"), LocalTime.of(hour + 1, 20), LocalTime.of(hour + 1, 20));
            route.addStopOver(locationMap.get("Bloemgracht"), LocalTime.of(hour + 1, 21), LocalTime.of(hour + 1, 21));
            route.addStopOver(locationMap.get("Marnixplein"), LocalTime.of(hour + 1, 23), LocalTime.of(hour + 1, 23));
            route.addStopOver(locationMap.get("Nw. Willemsstraat"), LocalTime.of(hour + 1, 24), LocalTime.of(hour + 1, 24));
            route.addEndPoint(locationMap.get("Eerste Marnixdwarsstraat"), LocalTime.of(hour + 1, 25));
            addRoute(route);

            route = new Route(locationMap.get("Amstelveen Stadshart"), LocalTime.of(hour + 1, 7));
            route.addStopOver(locationMap.get("Oranjebaan"), LocalTime.of(hour + 1, 8), LocalTime.of(hour + 1, 8));
            route.addStopOver(locationMap.get("Onderuit"), LocalTime.of(hour + 1, 9), LocalTime.of(hour + 1, 9));
            route.addStopOver(locationMap.get("Zonnestein"), LocalTime.of(hour + 1, 10), LocalTime.of(hour + 1, 10));
            route.addStopOver(locationMap.get("Kronenburg"), LocalTime.of(hour + 1, 11), LocalTime.of(hour + 1, 11));
            route.addStopOver(locationMap.get("Uilenstede"), LocalTime.of(hour + 1, 13), LocalTime.of(hour + 1, 13));
            route.addStopOver(locationMap.get("Van Boshuizenstraat"), LocalTime.of(hour + 1, 14), LocalTime.of(hour + 1, 14));
            route.addStopOver(locationMap.get("A.J. Ernststraat"), LocalTime.of(hour + 1, 16), LocalTime.of(hour + 1, 16));
            route.addStopOver(locationMap.get("Parnassusweg"), LocalTime.of(hour + 1, 17), LocalTime.of(hour + 1, 17));
            route.addStopOver(locationMap.get("Station Zuid"), LocalTime.of(hour + 1, 19), LocalTime.of(hour + 1, 19));
            route.addStopOver(locationMap.get("Prinses Irenestraat"), LocalTime.of(hour + 1, 20), LocalTime.of(hour + 1, 20));
            route.addStopOver(locationMap.get("Stadionweg"), LocalTime.of(hour + 1, 21), LocalTime.of(hour + 1, 21));
            route.addStopOver(locationMap.get("Gerrit v.d. Veenstraat"), LocalTime.of(hour + 1, 22), LocalTime.of(hour + 1, 22));
            route.addStopOver(locationMap.get("Roelof Hartplein"), LocalTime.of(hour + 1, 24), LocalTime.of(hour + 1, 24));
            route.addStopOver(locationMap.get("Concertgebouw"), LocalTime.of(hour + 1, 26), LocalTime.of(hour + 1, 26));
            route.addStopOver(locationMap.get("Museumplein"), LocalTime.of(hour + 1, 28), LocalTime.of(hour + 1, 28));
            route.addStopOver(locationMap.get("Leidseplein"), LocalTime.of(hour + 1, 32), LocalTime.of(hour + 1, 32));
            route.addStopOver(locationMap.get("Elandsgracht"), LocalTime.of(hour + 1, 34), LocalTime.of(hour + 1, 34));
            route.addStopOver(locationMap.get("Rozengracht"), LocalTime.of(hour + 1, 35), LocalTime.of(hour + 1, 35));
            route.addStopOver(locationMap.get("Bloemgracht"), LocalTime.of(hour + 1, 36), LocalTime.of(hour + 1, 36));
            route.addStopOver(locationMap.get("Marnixplein"), LocalTime.of(hour + 1, 38), LocalTime.of(hour + 1, 38));
            route.addStopOver(locationMap.get("Nw. Willemsstraat"), LocalTime.of(hour + 1, 39), LocalTime.of(hour + 1, 39));
            route.addEndPoint(locationMap.get("Eerste Marnixdwarsstraat"), LocalTime.of(hour + 1, 40));
            addRoute(route);

            route = new Route(locationMap.get("Amstelveen Stadshart"), LocalTime.of(hour + 1, 22));
            route.addStopOver(locationMap.get("Oranjebaan"), LocalTime.of(hour + 1, 23), LocalTime.of(hour + 1, 23));
            route.addStopOver(locationMap.get("Onderuit"), LocalTime.of(hour + 1, 24), LocalTime.of(hour + 1, 24));
            route.addStopOver(locationMap.get("Zonnestein"), LocalTime.of(hour + 1, 25), LocalTime.of(hour + 1, 25));
            route.addStopOver(locationMap.get("Kronenburg"), LocalTime.of(hour + 1, 26), LocalTime.of(hour + 1, 26));
            route.addStopOver(locationMap.get("Uilenstede"), LocalTime.of(hour + 1, 28), LocalTime.of(hour + 1, 28));
            route.addStopOver(locationMap.get("Van Boshuizenstraat"), LocalTime.of(hour + 1, 29), LocalTime.of(hour + 1, 29));
            route.addStopOver(locationMap.get("A.J. Ernststraat"), LocalTime.of(hour + 1, 31), LocalTime.of(hour + 1, 31));
            route.addStopOver(locationMap.get("Parnassusweg"), LocalTime.of(hour + 1, 32), LocalTime.of(hour + 1, 32));
            route.addStopOver(locationMap.get("Station Zuid"), LocalTime.of(hour + 1, 34), LocalTime.of(hour + 1, 34));
            route.addStopOver(locationMap.get("Prinses Irenestraat"), LocalTime.of(hour + 1, 35), LocalTime.of(hour + 1, 35));
            route.addStopOver(locationMap.get("Stadionweg"), LocalTime.of(hour + 1, 36), LocalTime.of(hour + 1, 36));
            route.addStopOver(locationMap.get("Gerrit v.d. Veenstraat"), LocalTime.of(hour + 1, 37), LocalTime.of(hour + 1, 37));
            route.addStopOver(locationMap.get("Roelof Hartplein"), LocalTime.of(hour + 1, 39), LocalTime.of(hour + 1, 39));
            route.addStopOver(locationMap.get("Concertgebouw"), LocalTime.of(hour + 1, 41), LocalTime.of(hour + 1, 41));
            route.addStopOver(locationMap.get("Museumplein"), LocalTime.of(hour + 1, 43), LocalTime.of(hour + 1, 43));
            route.addStopOver(locationMap.get("Leidseplein"), LocalTime.of(hour + 1, 47), LocalTime.of(hour + 1, 47));
            route.addStopOver(locationMap.get("Elandsgracht"), LocalTime.of(hour + 1, 49), LocalTime.of(hour + 1, 49));
            route.addStopOver(locationMap.get("Rozengracht"), LocalTime.of(hour + 1, 50), LocalTime.of(hour + 1, 50));
            route.addStopOver(locationMap.get("Bloemgracht"), LocalTime.of(hour + 1, 51), LocalTime.of(hour + 1, 51));
            route.addStopOver(locationMap.get("Marnixplein"), LocalTime.of(hour + 1, 53), LocalTime.of(hour + 1, 53));
            route.addStopOver(locationMap.get("Nw. Willemsstraat"), LocalTime.of(hour + 1, 54), LocalTime.of(hour + 1, 54));
            route.addEndPoint(locationMap.get("Eerste Marnixdwarsstraat"), LocalTime.of(hour + 1, 55));
            addRoute(route);

            route = new Route(locationMap.get("Amstelveen Stadshart"), LocalTime.of(hour + 1, 37));
            route.addStopOver(locationMap.get("Oranjebaan"), LocalTime.of(hour + 1, 38), LocalTime.of(hour + 1, 38));
            route.addStopOver(locationMap.get("Onderuit"), LocalTime.of(hour + 1, 39), LocalTime.of(hour + 1, 39));
            route.addStopOver(locationMap.get("Zonnestein"), LocalTime.of(hour + 1, 40), LocalTime.of(hour + 1, 40));
            route.addStopOver(locationMap.get("Kronenburg"), LocalTime.of(hour + 1, 41), LocalTime.of(hour + 1, 41));
            route.addStopOver(locationMap.get("Uilenstede"), LocalTime.of(hour + 1, 43), LocalTime.of(hour + 1, 43));
            route.addStopOver(locationMap.get("Van Boshuizenstraat"), LocalTime.of(hour + 1, 44), LocalTime.of(hour + 1, 44));
            route.addStopOver(locationMap.get("A.J. Ernststraat"), LocalTime.of(hour + 1, 46), LocalTime.of(hour + 1, 46));
            route.addStopOver(locationMap.get("Parnassusweg"), LocalTime.of(hour + 1, 47), LocalTime.of(hour + 1, 47));
            route.addStopOver(locationMap.get("Station Zuid"), LocalTime.of(hour + 1, 49), LocalTime.of(hour + 1, 49));
            route.addStopOver(locationMap.get("Prinses Irenestraat"), LocalTime.of(hour + 1, 50), LocalTime.of(hour + 1, 50));
            route.addStopOver(locationMap.get("Stadionweg"), LocalTime.of(hour + 1, 51), LocalTime.of(hour + 1, 51));
            route.addStopOver(locationMap.get("Gerrit v.d. Veenstraat"), LocalTime.of(hour + 1, 52), LocalTime.of(hour + 1, 52));
            route.addStopOver(locationMap.get("Roelof Hartplein"), LocalTime.of(hour + 1, 54), LocalTime.of(hour + 1, 54));
            route.addStopOver(locationMap.get("Concertgebouw"), LocalTime.of(hour + 1, 56), LocalTime.of(hour + 1, 56));
            route.addStopOver(locationMap.get("Museumplein"), LocalTime.of(hour + 1, 58), LocalTime.of(hour + 1, 58));
            route.addStopOver(locationMap.get("Leidseplein"), LocalTime.of(hour + 2, 2), LocalTime.of(hour + 2, 2));
            route.addStopOver(locationMap.get("Elandsgracht"), LocalTime.of(hour + 2, 4), LocalTime.of(hour + 2, 4));
            route.addStopOver(locationMap.get("Rozengracht"), LocalTime.of(hour + 2, 5), LocalTime.of(hour + 2, 5));
            route.addStopOver(locationMap.get("Bloemgracht"), LocalTime.of(hour + 2, 6), LocalTime.of(hour + 2, 6));
            route.addStopOver(locationMap.get("Marnixplein"), LocalTime.of(hour + 2, 8), LocalTime.of(hour + 2, 8));
            route.addStopOver(locationMap.get("Nw. Willemsstraat"), LocalTime.of(hour + 2, 9), LocalTime.of(hour + 2, 9));
            route.addEndPoint(locationMap.get("Eerste Marnixdwarsstraat"), LocalTime.of(hour + 2, 10));
        }

        for (int hour = 6; hour <= 22; hour++) {
            Route route = new Route(locationMap.get("Centraal Station"), LocalTime.of(hour ,28));
            route.addStopOver(locationMap.get("Dam"), LocalTime.of(hour, 30), LocalTime.of(hour, 30));
            route.addStopOver(locationMap.get("Rokin"), LocalTime.of(hour, 32), LocalTime.of(hour, 32));
            route.addStopOver(locationMap.get("Rembrandtplein"), LocalTime.of(hour, 35), LocalTime.of(hour, 35));
            route.addStopOver(locationMap.get("Keizersgracht"), LocalTime.of(hour, 36), LocalTime.of(hour, 36));
            route.addStopOver(locationMap.get("Prinsengracht"), LocalTime.of(hour, 37), LocalTime.of(hour, 37));
            route.addStopOver(locationMap.get("Frederiksplein"), LocalTime.of(hour, 39), LocalTime.of(hour, 39));
            route.addStopOver(locationMap.get("Ceintuurbaan"), LocalTime.of(hour, 42), LocalTime.of(hour, 42));
            route.addStopOver(locationMap.get("Lutmastraat"), LocalTime.of(hour, 43), LocalTime.of(hour, 43));
            route.addStopOver(locationMap.get("Amstelkade"), LocalTime.of(hour, 44), LocalTime.of(hour, 44));
            route.addStopOver(locationMap.get("Victorieplein"), LocalTime.of(hour, 45), LocalTime.of(hour, 45));
            route.addStopOver(locationMap.get("Waalstraat"), LocalTime.of(hour, 47), LocalTime.of(hour, 47));
            route.addStopOver(locationMap.get("Maasstraat"), LocalTime.of(hour, 48), LocalTime.of(hour, 48));
            route.addStopOver(locationMap.get("Dintelstraat"), LocalTime.of(hour, 49), LocalTime.of(hour, 49));
            route.addStopOver(locationMap.get("Europaplein"), LocalTime.of(hour, 50), LocalTime.of(hour, 50));
            route.addStopOver(locationMap.get("Station RAI"), LocalTime.of(hour, 51), LocalTime.of(hour, 51));
            route.addEndPoint(locationMap.get("Drentepark"), LocalTime.of(hour, 52));
            addRoute(route);

            route = new Route(locationMap.get("Centraal Station"), LocalTime.of(hour ,43));
            route.addStopOver(locationMap.get("Dam"), LocalTime.of(hour, 45), LocalTime.of(hour, 45));
            route.addStopOver(locationMap.get("Rokin"), LocalTime.of(hour, 47), LocalTime.of(hour, 47));
            route.addStopOver(locationMap.get("Rembrandtplein"), LocalTime.of(hour, 50), LocalTime.of(hour, 50));
            route.addStopOver(locationMap.get("Keizersgracht"), LocalTime.of(hour, 51), LocalTime.of(hour, 51));
            route.addStopOver(locationMap.get("Prinsengracht"), LocalTime.of(hour, 52), LocalTime.of(hour, 52));
            route.addStopOver(locationMap.get("Frederiksplein"), LocalTime.of(hour, 54), LocalTime.of(hour, 54));
            route.addStopOver(locationMap.get("Ceintuurbaan"), LocalTime.of(hour, 57), LocalTime.of(hour, 57));
            route.addStopOver(locationMap.get("Lutmastraat"), LocalTime.of(hour, 58), LocalTime.of(hour, 58));
            route.addStopOver(locationMap.get("Amstelkade"), LocalTime.of(hour, 59), LocalTime.of(hour, 59));
            route.addStopOver(locationMap.get("Victorieplein"), LocalTime.of(hour + 1, 0), LocalTime.of(hour + 1, 0));
            route.addStopOver(locationMap.get("Waalstraat"), LocalTime.of(hour + 1, 2), LocalTime.of(hour + 1, 2));
            route.addStopOver(locationMap.get("Maasstraat"), LocalTime.of(hour + 1, 3), LocalTime.of(hour + 1, 3));
            route.addStopOver(locationMap.get("Dintelstraat"), LocalTime.of(hour + 1, 4), LocalTime.of(hour + 1, 4));
            route.addStopOver(locationMap.get("Europaplein"), LocalTime.of(hour + 1, 5), LocalTime.of(hour + 1, 5));
            route.addStopOver(locationMap.get("Station RAI"), LocalTime.of(hour + 1, 6), LocalTime.of(hour + 1, 6));
            route.addEndPoint(locationMap.get("Drentepark"), LocalTime.of(hour + 1, 7));
            addRoute(route);

            route = new Route(locationMap.get("Centraal Station"), LocalTime.of(hour ,58));
            route.addStopOver(locationMap.get("Dam"), LocalTime.of(hour + 1, 0), LocalTime.of(hour + 1, 0));
            route.addStopOver(locationMap.get("Rokin"), LocalTime.of(hour + 1, 2), LocalTime.of(hour + 1, 2));
            route.addStopOver(locationMap.get("Rembrandtplein"), LocalTime.of(hour + 1, 5), LocalTime.of(hour + 1, 3));
            route.addStopOver(locationMap.get("Keizersgracht"), LocalTime.of(hour + 1, 6), LocalTime.of(hour + 1, 6));
            route.addStopOver(locationMap.get("Prinsengracht"), LocalTime.of(hour + 1, 7), LocalTime.of(hour + 1, 7));
            route.addStopOver(locationMap.get("Frederiksplein"), LocalTime.of(hour + 1, 9), LocalTime.of(hour + 1, 9));
            route.addStopOver(locationMap.get("Ceintuurbaan"), LocalTime.of(hour + 1, 12), LocalTime.of(hour + 1, 12));
            route.addStopOver(locationMap.get("Lutmastraat"), LocalTime.of(hour + 1, 13), LocalTime.of(hour + 1, 13));
            route.addStopOver(locationMap.get("Amstelkade"), LocalTime.of(hour + 1, 14), LocalTime.of(hour + 1, 14));
            route.addStopOver(locationMap.get("Victorieplein"), LocalTime.of(hour + 1, 15), LocalTime.of(hour + 1, 15));
            route.addStopOver(locationMap.get("Waalstraat"), LocalTime.of(hour + 1, 17), LocalTime.of(hour + 1, 17));
            route.addStopOver(locationMap.get("Maasstraat"), LocalTime.of(hour + 1, 18), LocalTime.of(hour + 1, 18));
            route.addStopOver(locationMap.get("Dintelstraat"), LocalTime.of(hour + 1, 19), LocalTime.of(hour + 1, 19));
            route.addStopOver(locationMap.get("Europaplein"), LocalTime.of(hour + 1, 20), LocalTime.of(hour + 1, 20));
            route.addStopOver(locationMap.get("Station RAI"), LocalTime.of(hour + 1, 21), LocalTime.of(hour + 1, 21));
            route.addEndPoint(locationMap.get("Drentepark"), LocalTime.of(hour + 1, 22));
            addRoute(route);

            route = new Route(locationMap.get("Centraal Station"), LocalTime.of(hour + 1, 13));
            route.addStopOver(locationMap.get("Dam"), LocalTime.of(hour + 1, 15), LocalTime.of(hour + 1, 15));
            route.addStopOver(locationMap.get("Rokin"), LocalTime.of(hour + 1, 17), LocalTime.of(hour + 1, 17));
            route.addStopOver(locationMap.get("Rembrandtplein"), LocalTime.of(hour + 1, 20), LocalTime.of(hour + 1, 20));
            route.addStopOver(locationMap.get("Keizersgracht"), LocalTime.of(hour + 1, 21), LocalTime.of(hour + 1, 21));
            route.addStopOver(locationMap.get("Prinsengracht"), LocalTime.of(hour + 1, 22), LocalTime.of(hour + 1, 22));
            route.addStopOver(locationMap.get("Frederiksplein"), LocalTime.of(hour + 1, 24), LocalTime.of(hour + 1, 24));
            route.addStopOver(locationMap.get("Ceintuurbaan"), LocalTime.of(hour + 1, 27), LocalTime.of(hour + 1, 27));
            route.addStopOver(locationMap.get("Lutmastraat"), LocalTime.of(hour + 1, 28), LocalTime.of(hour + 1, 28));
            route.addStopOver(locationMap.get("Amstelkade"), LocalTime.of(hour + 1, 29), LocalTime.of(hour + 1, 29));
            route.addStopOver(locationMap.get("Victorieplein"), LocalTime.of(hour + 1, 30), LocalTime.of(hour + 1, 30));
            route.addStopOver(locationMap.get("Waalstraat"), LocalTime.of(hour + 1, 32), LocalTime.of(hour + 1, 33));
            route.addStopOver(locationMap.get("Dintelstraat"), LocalTime.of(hour + 1, 34), LocalTime.of(hour + 1, 34));
            route.addStopOver(locationMap.get("Europaplein"), LocalTime.of(hour + 1, 35), LocalTime.of(hour + 1, 35));
            route.addStopOver(locationMap.get("Station RAI"), LocalTime.of(hour + 1, 36), LocalTime.of(hour + 1, 36));
            route.addEndPoint(locationMap.get("Drentepark"), LocalTime.of(hour + 1, 37));
            addRoute(route);
        }

        for (int hour = 6; hour <= 22; hour++) {
            Route route = new Route(locationMap.get("Drentepark"), LocalTime.of(hour, 1));
            route.addStopOver(locationMap.get("Station RAI"), LocalTime.of(hour, 1), LocalTime.of(hour, 1));
            route.addStopOver(locationMap.get("Europaplein"), LocalTime.of(hour, 2), LocalTime.of(hour, 2));
            route.addStopOver(locationMap.get("Dintelstraat"), LocalTime.of(hour, 3), LocalTime.of(hour, 3));
            route.addStopOver(locationMap.get("Maasstraat"), LocalTime.of(hour, 4), LocalTime.of(hour, 4));
            route.addStopOver(locationMap.get("Waalstraat"), LocalTime.of(hour, 5), LocalTime.of(hour, 5));
            route.addStopOver(locationMap.get("Victorieplein"), LocalTime.of(hour, 7), LocalTime.of(hour, 7));
            route.addStopOver(locationMap.get("Amstelkade"), LocalTime.of(hour, 7), LocalTime.of(hour, 7));
            route.addStopOver(locationMap.get("Lutmastraat"), LocalTime.of(hour, 9), LocalTime.of(hour, 9));
            route.addStopOver(locationMap.get("Ceintuurbaan"), LocalTime.of(hour, 11), LocalTime.of(hour, 11));
            route.addStopOver(locationMap.get("Stadhouderskade"), LocalTime.of(hour, 13), LocalTime.of(hour, 13));
            route.addStopOver(locationMap.get("Frederiksplein"), LocalTime.of(hour, 14), LocalTime.of(hour, 14));
            route.addStopOver(locationMap.get("Prinsengracht"), LocalTime.of(hour, 15), LocalTime.of(hour, 15));
            route.addStopOver(locationMap.get("Keizersgracht"), LocalTime.of(hour, 16), LocalTime.of(hour, 16));
            route.addStopOver(locationMap.get("Rembrandtplein"), LocalTime.of(hour, 19), LocalTime.of(hour, 19));
            route.addStopOver(locationMap.get("Rokin"), LocalTime.of(hour, 21), LocalTime.of(hour, 21));
            route.addStopOver(locationMap.get("Dam"), LocalTime.of(hour, 23), LocalTime.of(hour, 23));
            route.addEndPoint(locationMap.get("Centraal Station"), LocalTime.of(hour, 26));
            addRoute(route);

            route = new Route(locationMap.get("Drentepark"), LocalTime.of(hour, 16));
            route.addStopOver(locationMap.get("Station RAI"), LocalTime.of(hour, 16), LocalTime.of(hour, 16));
            route.addStopOver(locationMap.get("Europaplein"), LocalTime.of(hour, 17), LocalTime.of(hour, 17));
            route.addStopOver(locationMap.get("Dintelstraat"), LocalTime.of(hour, 18), LocalTime.of(hour, 18));
            route.addStopOver(locationMap.get("Maasstraat"), LocalTime.of(hour, 19), LocalTime.of(hour, 19));
            route.addStopOver(locationMap.get("Waalstraat"), LocalTime.of(hour, 20), LocalTime.of(hour, 20));
            route.addStopOver(locationMap.get("Victorieplein"), LocalTime.of(hour, 22), LocalTime.of(hour, 22));
            route.addStopOver(locationMap.get("Amstelkade"), LocalTime.of(hour, 22), LocalTime.of(hour, 22));
            route.addStopOver(locationMap.get("Lutmastraat"), LocalTime.of(hour, 24), LocalTime.of(hour, 24));
            route.addStopOver(locationMap.get("Ceintuurbaan"), LocalTime.of(hour, 26), LocalTime.of(hour, 26));
            route.addStopOver(locationMap.get("Stadhouderskade"), LocalTime.of(hour, 28), LocalTime.of(hour, 28));
            route.addStopOver(locationMap.get("Frederiksplein"), LocalTime.of(hour, 29), LocalTime.of(hour, 29));
            route.addStopOver(locationMap.get("Prinsengracht"), LocalTime.of(hour, 30), LocalTime.of(hour, 30));
            route.addStopOver(locationMap.get("Keizersgracht"), LocalTime.of(hour, 31), LocalTime.of(hour, 31));
            route.addStopOver(locationMap.get("Rembrandtplein"), LocalTime.of(hour, 34), LocalTime.of(hour, 34));
            route.addStopOver(locationMap.get("Rokin"), LocalTime.of(hour, 36), LocalTime.of(hour, 36));
            route.addStopOver(locationMap.get("Dam"), LocalTime.of(hour, 38), LocalTime.of(hour, 38));
            route.addEndPoint(locationMap.get("Centraal Station"), LocalTime.of(hour, 41));
            addRoute(route);

            route = new Route(locationMap.get("Drentepark"), LocalTime.of(hour, 31));
            route.addStopOver(locationMap.get("Station RAI"), LocalTime.of(hour, 31), LocalTime.of(hour, 31));
            route.addStopOver(locationMap.get("Europaplein"), LocalTime.of(hour, 32), LocalTime.of(hour, 32));
            route.addStopOver(locationMap.get("Dintelstraat"), LocalTime.of(hour, 33), LocalTime.of(hour, 33));
            route.addStopOver(locationMap.get("Maasstraat"), LocalTime.of(hour, 34), LocalTime.of(hour, 34));
            route.addStopOver(locationMap.get("Waalstraat"), LocalTime.of(hour, 35), LocalTime.of(hour, 35));
            route.addStopOver(locationMap.get("Victorieplein"), LocalTime.of(hour, 37), LocalTime.of(hour, 37));
            route.addStopOver(locationMap.get("Amstelkade"), LocalTime.of(hour, 37), LocalTime.of(hour, 37));
            route.addStopOver(locationMap.get("Lutmastraat"), LocalTime.of(hour, 39), LocalTime.of(hour, 39));
            route.addStopOver(locationMap.get("Ceintuurbaan"), LocalTime.of(hour, 41), LocalTime.of(hour, 41));
            route.addStopOver(locationMap.get("Stadhouderskade"), LocalTime.of(hour, 43), LocalTime.of(hour, 43));
            route.addStopOver(locationMap.get("Frederiksplein"), LocalTime.of(hour, 44), LocalTime.of(hour, 44));
            route.addStopOver(locationMap.get("Prinsengracht"), LocalTime.of(hour, 45), LocalTime.of(hour, 45));
            route.addStopOver(locationMap.get("Keizersgracht"), LocalTime.of(hour, 46), LocalTime.of(hour, 46));
            route.addStopOver(locationMap.get("Rembrandtplein"), LocalTime.of(hour, 49), LocalTime.of(hour, 49));
            route.addStopOver(locationMap.get("Rokin"), LocalTime.of(hour, 51), LocalTime.of(hour, 51));
            route.addStopOver(locationMap.get("Dam"), LocalTime.of(hour, 53), LocalTime.of(hour, 53));
            route.addEndPoint(locationMap.get("Centraal Station"), LocalTime.of(hour, 56));
            addRoute(route);

            route = new Route(locationMap.get("Drentepark"), LocalTime.of(hour, 46));
            route.addStopOver(locationMap.get("Station RAI"), LocalTime.of(hour, 46), LocalTime.of(hour, 46));
            route.addStopOver(locationMap.get("Europaplein"), LocalTime.of(hour, 47), LocalTime.of(hour, 47));
            route.addStopOver(locationMap.get("Dintelstraat"), LocalTime.of(hour, 48), LocalTime.of(hour, 48));
            route.addStopOver(locationMap.get("Maasstraat"), LocalTime.of(hour, 49), LocalTime.of(hour, 49));
            route.addStopOver(locationMap.get("Waalstraat"), LocalTime.of(hour, 50), LocalTime.of(hour, 50));
            route.addStopOver(locationMap.get("Victorieplein"), LocalTime.of(hour, 52), LocalTime.of(hour, 52));
            route.addStopOver(locationMap.get("Amstelkade"), LocalTime.of(hour, 52), LocalTime.of(hour, 52));
            route.addStopOver(locationMap.get("Lutmastraat"), LocalTime.of(hour, 54), LocalTime.of(hour, 54));
            route.addStopOver(locationMap.get("Ceintuurbaan"), LocalTime.of(hour, 56), LocalTime.of(hour, 56));
            route.addStopOver(locationMap.get("Stadhouderskade"), LocalTime.of(hour, 58), LocalTime.of(hour, 58));
            route.addStopOver(locationMap.get("Frederiksplein"), LocalTime.of(hour, 59), LocalTime.of(hour, 59));
            route.addStopOver(locationMap.get("Prinsengracht"), LocalTime.of(hour + 1, 0), LocalTime.of(hour + 1, 0));
            route.addStopOver(locationMap.get("Keizersgracht"), LocalTime.of(hour + 1, 1), LocalTime.of(hour + 1, 1));
            route.addStopOver(locationMap.get("Rembrandtplein"), LocalTime.of(hour + 1, 4), LocalTime.of(hour + 1, 4));
            route.addStopOver(locationMap.get("Rokin"), LocalTime.of(hour + 1, 6), LocalTime.of(hour + 1, 6));
            route.addStopOver(locationMap.get("Dam"), LocalTime.of(hour + 1, 8), LocalTime.of(hour + 1, 8));
            route.addEndPoint(locationMap.get("Centraal Station"), LocalTime.of(hour + 1, 11));
            addRoute(route);
        }

        for (int hour = 6; hour <= 22; hour++) {
            Route route = new Route(locationMap.get("Van Hallstraat"), LocalTime.of(hour, 0));
            route.addStopOver(locationMap.get("Van L.Stirumstraat"), LocalTime.of(hour, 1), LocalTime.of(hour, 1));
            route.addStopOver(locationMap.get("De Wittenkade"), LocalTime.of(hour, 1), LocalTime.of(hour, 1));
            route.addStopOver(locationMap.get("Nassaukade"), LocalTime.of(hour, 2), LocalTime.of(hour, 2));
            route.addStopOver(locationMap.get("F.Hendrikplantsoen"), LocalTime.of(hour, 4), LocalTime.of(hour, 4));
            route.addStopOver(locationMap.get("Hugo de Grootplein"), LocalTime.of(hour, 5), LocalTime.of(hour, 5));
            route.addStopOver(locationMap.get("De Clercqstraat"), LocalTime.of(hour, 6), LocalTime.of(hour, 6));
            route.addStopOver(locationMap.get("Kinkerstraat"), LocalTime.of(hour, 8), LocalTime.of(hour, 8));
            route.addStopOver(locationMap.get("Overtoom"), LocalTime.of(hour, 10), LocalTime.of(hour, 10));
            route.addStopOver(locationMap.get("Van Baerlestraat"), LocalTime.of(hour, 12), LocalTime.of(hour, 12));
            route.addStopOver(locationMap.get("Concertgebouw"), LocalTime.of(hour, 13), LocalTime.of(hour, 13));
            route.addStopOver(locationMap.get("Roelof Hartplein"), LocalTime.of(hour, 14), LocalTime.of(hour, 14));
            route.addStopOver(locationMap.get("De Pijp"), LocalTime.of(hour, 17), LocalTime.of(hour, 17));
            route.addStopOver(locationMap.get("2e v.d.Helststraat"), LocalTime.of(hour, 18), LocalTime.of(hour, 18));
            route.addStopOver(locationMap.get("Van Woustraat"), LocalTime.of(hour, 20), LocalTime.of(hour, 20));
            route.addStopOver(locationMap.get("Wibautstraat"), LocalTime.of(hour, 23), LocalTime.of(hour, 23));
            route.addStopOver(locationMap.get("Camperstraat"), LocalTime.of(hour, 23), LocalTime.of(hour, 23));
            route.addStopOver(locationMap.get("Beukenweg"), LocalTime.of(hour, 25), LocalTime.of(hour, 25));
            route.addStopOver(locationMap.get("Linnaeusstraat"), LocalTime.of(hour, 26), LocalTime.of(hour, 26));
            route.addStopOver(locationMap.get("Dapperstraat"), LocalTime.of(hour, 28), LocalTime.of(hour, 28));
            route.addStopOver(locationMap.get("Muiderpoortstation"), LocalTime.of(hour, 29), LocalTime.of(hour, 29));
            route.addStopOver(locationMap.get("Molukkenstraat"), LocalTime.of(hour, 29), LocalTime.of(hour, 29));
            route.addStopOver(locationMap.get("Soembawastraat"), LocalTime.of(hour, 30), LocalTime.of(hour, 30));
            route.addStopOver(locationMap.get("Insulindeweg"), LocalTime.of(hour, 31), LocalTime.of(hour, 31));
            route.addEndPoint(locationMap.get("Flevopark"), LocalTime.of(hour, 32));
            addRoute(route);

            route = new Route(locationMap.get("Van Hallstraat"), LocalTime.of(hour, 15));
            route.addStopOver(locationMap.get("Van L.Stirumstraat"), LocalTime.of(hour, 16), LocalTime.of(hour, 16));
            route.addStopOver(locationMap.get("De Wittenkade"), LocalTime.of(hour, 16), LocalTime.of(hour, 16));
            route.addStopOver(locationMap.get("Nassaukade"), LocalTime.of(hour, 17), LocalTime.of(hour, 17));
            route.addStopOver(locationMap.get("F.Hendrikplantsoen"), LocalTime.of(hour, 19), LocalTime.of(hour, 19));
            route.addStopOver(locationMap.get("Hugo de Grootplein"), LocalTime.of(hour, 20), LocalTime.of(hour, 20));
            route.addStopOver(locationMap.get("De Clercqstraat"), LocalTime.of(hour, 21), LocalTime.of(hour, 21));
            route.addStopOver(locationMap.get("Kinkerstraat"), LocalTime.of(hour, 23), LocalTime.of(hour, 23));
            route.addStopOver(locationMap.get("Overtoom"), LocalTime.of(hour, 25), LocalTime.of(hour, 25));
            route.addStopOver(locationMap.get("Van Baerlestraat"), LocalTime.of(hour, 27), LocalTime.of(hour, 27));
            route.addStopOver(locationMap.get("Concertgebouw"), LocalTime.of(hour, 28), LocalTime.of(hour, 28));
            route.addStopOver(locationMap.get("Roelof Hartplein"), LocalTime.of(hour, 29), LocalTime.of(hour, 29));
            route.addStopOver(locationMap.get("De Pijp"), LocalTime.of(hour, 32), LocalTime.of(hour, 32));
            route.addStopOver(locationMap.get("2e v.d.Helststraat"), LocalTime.of(hour, 33), LocalTime.of(hour, 33));
            route.addStopOver(locationMap.get("Van Woustraat"), LocalTime.of(hour, 35), LocalTime.of(hour, 35));
            route.addStopOver(locationMap.get("Wibautstraat"), LocalTime.of(hour, 38), LocalTime.of(hour, 38));
            route.addStopOver(locationMap.get("Camperstraat"), LocalTime.of(hour, 38), LocalTime.of(hour, 38));
            route.addStopOver(locationMap.get("Beukenweg"), LocalTime.of(hour, 40), LocalTime.of(hour, 40));
            route.addStopOver(locationMap.get("Linnaeusstraat"), LocalTime.of(hour, 41), LocalTime.of(hour, 41));
            route.addStopOver(locationMap.get("Dapperstraat"), LocalTime.of(hour, 43), LocalTime.of(hour, 43));
            route.addStopOver(locationMap.get("Muiderpoortstation"), LocalTime.of(hour, 44), LocalTime.of(hour, 44));
            route.addStopOver(locationMap.get("Molukkenstraat"), LocalTime.of(hour, 44), LocalTime.of(hour, 44));
            route.addStopOver(locationMap.get("Soembawastraat"), LocalTime.of(hour, 45), LocalTime.of(hour, 45));
            route.addStopOver(locationMap.get("Insulindeweg"), LocalTime.of(hour, 46), LocalTime.of(hour, 46));
            route.addEndPoint(locationMap.get("Flevopark"), LocalTime.of(hour, 47));
            addRoute(route);

            route = new Route(locationMap.get("Van Hallstraat"), LocalTime.of(hour, 30));
            route.addStopOver(locationMap.get("Van L.Stirumstraat"), LocalTime.of(hour, 31), LocalTime.of(hour, 31));
            route.addStopOver(locationMap.get("De Wittenkade"), LocalTime.of(hour, 31), LocalTime.of(hour, 31));
            route.addStopOver(locationMap.get("Nassaukade"), LocalTime.of(hour, 32), LocalTime.of(hour, 32));
            route.addStopOver(locationMap.get("F.Hendrikplantsoen"), LocalTime.of(hour, 34), LocalTime.of(hour, 34));
            route.addStopOver(locationMap.get("Hugo de Grootplein"), LocalTime.of(hour, 35), LocalTime.of(hour, 35));
            route.addStopOver(locationMap.get("De Clercqstraat"), LocalTime.of(hour, 36), LocalTime.of(hour, 36));
            route.addStopOver(locationMap.get("Kinkerstraat"), LocalTime.of(hour, 38), LocalTime.of(hour, 38));
            route.addStopOver(locationMap.get("Overtoom"), LocalTime.of(hour, 40), LocalTime.of(hour, 40));
            route.addStopOver(locationMap.get("Van Baerlestraat"), LocalTime.of(hour, 42), LocalTime.of(hour, 42));
            route.addStopOver(locationMap.get("Concertgebouw"), LocalTime.of(hour, 43), LocalTime.of(hour, 43));
            route.addStopOver(locationMap.get("Roelof Hartplein"), LocalTime.of(hour, 44), LocalTime.of(hour, 44));
            route.addStopOver(locationMap.get("De Pijp"), LocalTime.of(hour, 47), LocalTime.of(hour, 47));
            route.addStopOver(locationMap.get("2e v.d.Helststraat"), LocalTime.of(hour, 48), LocalTime.of(hour, 48));
            route.addStopOver(locationMap.get("Van Woustraat"), LocalTime.of(hour, 50), LocalTime.of(hour, 50));
            route.addStopOver(locationMap.get("Wibautstraat"), LocalTime.of(hour, 53), LocalTime.of(hour, 53));
            route.addStopOver(locationMap.get("Camperstraat"), LocalTime.of(hour, 53), LocalTime.of(hour, 53));
            route.addStopOver(locationMap.get("Beukenweg"), LocalTime.of(hour, 55), LocalTime.of(hour, 55));
            route.addStopOver(locationMap.get("Linnaeusstraat"), LocalTime.of(hour, 56), LocalTime.of(hour, 56));
            route.addStopOver(locationMap.get("Dapperstraat"), LocalTime.of(hour, 58), LocalTime.of(hour, 58));
            route.addStopOver(locationMap.get("Muiderpoortstation"), LocalTime.of(hour, 59), LocalTime.of(hour, 59));
            route.addStopOver(locationMap.get("Molukkenstraat"), LocalTime.of(hour, 59), LocalTime.of(hour, 59));
            route.addStopOver(locationMap.get("Soembawastraat"), LocalTime.of(hour + 1, 0), LocalTime.of(hour + 1, 0));
            route.addStopOver(locationMap.get("Insulindeweg"), LocalTime.of(hour + 1, 1), LocalTime.of(hour + 1, 1));
            route.addEndPoint(locationMap.get("Flevopark"), LocalTime.of(hour + 1, 2));
            addRoute(route);

            route = new Route(locationMap.get("Van Hallstraat"), LocalTime.of(hour, 45));
            route.addStopOver(locationMap.get("Van L.Stirumstraat"), LocalTime.of(hour, 46), LocalTime.of(hour, 46));
            route.addStopOver(locationMap.get("De Wittenkade"), LocalTime.of(hour, 46), LocalTime.of(hour, 46));
            route.addStopOver(locationMap.get("Nassaukade"), LocalTime.of(hour, 47), LocalTime.of(hour, 47));
            route.addStopOver(locationMap.get("F.Hendrikplantsoen"), LocalTime.of(hour, 49), LocalTime.of(hour, 49));
            route.addStopOver(locationMap.get("Hugo de Grootplein"), LocalTime.of(hour, 50), LocalTime.of(hour, 50));
            route.addStopOver(locationMap.get("De Clercqstraat"), LocalTime.of(hour, 51), LocalTime.of(hour, 51));
            route.addStopOver(locationMap.get("Kinkerstraat"), LocalTime.of(hour, 53), LocalTime.of(hour, 53));
            route.addStopOver(locationMap.get("Overtoom"), LocalTime.of(hour, 55), LocalTime.of(hour, 55));
            route.addStopOver(locationMap.get("Van Baerlestraat"), LocalTime.of(hour, 57), LocalTime.of(hour, 57));
            route.addStopOver(locationMap.get("Concertgebouw"), LocalTime.of(hour, 58), LocalTime.of(hour, 58));
            route.addStopOver(locationMap.get("Roelof Hartplein"), LocalTime.of(hour, 59), LocalTime.of(hour, 59));
            route.addStopOver(locationMap.get("De Pijp"), LocalTime.of(hour + 1, 2), LocalTime.of(hour + 1, 2));
            route.addStopOver(locationMap.get("2e v.d.Helststraat"), LocalTime.of(hour + 1, 3), LocalTime.of(hour + 1, 3));
            route.addStopOver(locationMap.get("Van Woustraat"), LocalTime.of(hour + 1, 5), LocalTime.of(hour + 1, 5));
            route.addStopOver(locationMap.get("Wibautstraat"), LocalTime.of(hour + 1, 8), LocalTime.of(hour + 1, 8));
            route.addStopOver(locationMap.get("Camperstraat"), LocalTime.of(hour + 1, 8), LocalTime.of(hour + 1, 8));
            route.addStopOver(locationMap.get("Beukenweg"), LocalTime.of(hour + 1, 10), LocalTime.of(hour + 1, 10));
            route.addStopOver(locationMap.get("Linnaeusstraat"), LocalTime.of(hour + 1, 11), LocalTime.of(hour + 1, 11));
            route.addStopOver(locationMap.get("Dapperstraat"), LocalTime.of(hour + 1, 13), LocalTime.of(hour + 1, 13));
            route.addStopOver(locationMap.get("Muiderpoortstation"), LocalTime.of(hour + 1, 14), LocalTime.of(hour + 1, 14));
            route.addStopOver(locationMap.get("Molukkenstraat"), LocalTime.of(hour + 1, 14), LocalTime.of(hour + 1, 14));
            route.addStopOver(locationMap.get("Soembawastraat"), LocalTime.of(hour + 1, 15), LocalTime.of(hour + 1, 15));
            route.addStopOver(locationMap.get("Insulindeweg"), LocalTime.of(hour + 1, 16), LocalTime.of(hour + 1, 16));
            route.addEndPoint(locationMap.get("Flevopark"), LocalTime.of(hour + 1, 17));
            addRoute(route);
        }

        for (int hour = 5; hour <= 20; hour++) {
            Route route = new Route(locationMap.get("Flevopark"), LocalTime.of(hour ,53));
            route.addStopOver(locationMap.get("Soembawastraat"), LocalTime.of(hour, 53), LocalTime.of(hour, 53));
            route.addStopOver(locationMap.get("Molukkenstraat"), LocalTime.of(hour, 55), LocalTime.of(hour, 55));
            route.addStopOver(locationMap.get("Muiderpoortstation"), LocalTime.of(hour, 56), LocalTime.of(hour, 56));
            route.addStopOver(locationMap.get("Dapperstraat"), LocalTime.of(hour, 58), LocalTime.of(hour, 58));
            route.addStopOver(locationMap.get("Linnaeusstraat"), LocalTime.of(hour, 59), LocalTime.of(hour, 59));
            route.addStopOver(locationMap.get("Beukenweg"), LocalTime.of(hour + 1, 2), LocalTime.of(hour + 1, 2));
            route.addStopOver(locationMap.get("Camperstraat"), LocalTime.of(hour + 1, 2), LocalTime.of(hour + 1, 2));
            route.addStopOver(locationMap.get("Wibautstraat"), LocalTime.of(hour + 1, 4), LocalTime.of(hour + 1, 4));
            route.addStopOver(locationMap.get("Van Woustraat"), LocalTime.of(hour + 1, 6), LocalTime.of(hour + 1, 6));
            route.addStopOver(locationMap.get("2e v.d.Helststraat"), LocalTime.of(hour + 1, 7), LocalTime.of(hour + 1, 7));
            route.addStopOver(locationMap.get("De Pijp"), LocalTime.of(hour + 1, 9), LocalTime.of(hour + 1, 9));
            route.addStopOver(locationMap.get("Roelof Hartplein"), LocalTime.of(hour + 1, 10), LocalTime.of(hour + 1, 10));
            route.addStopOver(locationMap.get("Concertgebouw"), LocalTime.of(hour + 1, 13), LocalTime.of(hour + 1, 13));
            route.addStopOver(locationMap.get("Van Baerlestraat"), LocalTime.of(hour + 1, 13), LocalTime.of(hour + 1, 13));
            route.addStopOver(locationMap.get("Overtoom"), LocalTime.of(hour + 1, 15), LocalTime.of(hour + 1, 15));
            route.addStopOver(locationMap.get("Kinkerstraat"), LocalTime.of(hour + 1, 17), LocalTime.of(hour + 1, 17));
            route.addStopOver(locationMap.get("De Clercqstraat"), LocalTime.of(hour + 1, 19), LocalTime.of(hour + 1, 19));
            route.addStopOver(locationMap.get("Hugo de Grootplein"), LocalTime.of(hour + 1, 20), LocalTime.of(hour + 1, 20));
            route.addStopOver(locationMap.get("F.Hendrikplantsoen"), LocalTime.of(hour + 1, 23), LocalTime.of(hour + 1, 23));
            route.addStopOver(locationMap.get("Nassaukade"), LocalTime.of(hour + 1, 24), LocalTime.of(hour + 1, 24));
            route.addStopOver(locationMap.get("De Wittenkade"), LocalTime.of(hour + 1, 25), LocalTime.of(hour + 1, 25));
            route.addStopOver(locationMap.get("Van L.Stirumstraat"), LocalTime.of(hour + 1, 25), LocalTime.of(hour + 1, 25));
            route.addEndPoint(locationMap.get("Van Hallstraat"), LocalTime.of(hour + 1, 27));
            addRoute(route);

            route = new Route(locationMap.get("Flevopark"), LocalTime.of(hour + 1 ,8));
            route.addStopOver(locationMap.get("Soembawastraat"), LocalTime.of(hour + 1, 8), LocalTime.of(hour + 1, 8));
            route.addStopOver(locationMap.get("Molukkenstraat"), LocalTime.of(hour + 1, 10), LocalTime.of(hour + 1, 10));
            route.addStopOver(locationMap.get("Muiderpoortstation"), LocalTime.of(hour + 1, 11), LocalTime.of(hour + 1, 11));
            route.addStopOver(locationMap.get("Dapperstraat"), LocalTime.of(hour + 1, 13), LocalTime.of(hour + 1, 13));
            route.addStopOver(locationMap.get("Linnaeusstraat"), LocalTime.of(hour + 1, 14), LocalTime.of(hour + 1, 14));
            route.addStopOver(locationMap.get("Beukenweg"), LocalTime.of(hour + 1, 17), LocalTime.of(hour + 1, 17));
            route.addStopOver(locationMap.get("Camperstraat"), LocalTime.of(hour + 1, 17), LocalTime.of(hour + 1, 17));
            route.addStopOver(locationMap.get("Wibautstraat"), LocalTime.of(hour + 1, 19), LocalTime.of(hour + 1, 19));
            route.addStopOver(locationMap.get("Van Woustraat"), LocalTime.of(hour + 1, 21), LocalTime.of(hour + 1, 21));
            route.addStopOver(locationMap.get("2e v.d.Helststraat"), LocalTime.of(hour + 1, 22), LocalTime.of(hour + 1, 22));
            route.addStopOver(locationMap.get("De Pijp"), LocalTime.of(hour + 1, 24), LocalTime.of(hour + 1, 24));
            route.addStopOver(locationMap.get("Roelof Hartplein"), LocalTime.of(hour + 1, 25), LocalTime.of(hour + 1, 25));
            route.addStopOver(locationMap.get("Concertgebouw"), LocalTime.of(hour + 1, 28), LocalTime.of(hour + 1, 28));
            route.addStopOver(locationMap.get("Van Baerlestraat"), LocalTime.of(hour + 1, 28), LocalTime.of(hour + 1, 28));
            route.addStopOver(locationMap.get("Overtoom"), LocalTime.of(hour + 1, 30), LocalTime.of(hour + 1, 30));
            route.addStopOver(locationMap.get("Kinkerstraat"), LocalTime.of(hour + 1, 32), LocalTime.of(hour + 1, 32));
            route.addStopOver(locationMap.get("De Clercqstraat"), LocalTime.of(hour + 1, 34), LocalTime.of(hour + 1, 34));
            route.addStopOver(locationMap.get("Hugo de Grootplein"), LocalTime.of(hour + 1, 35), LocalTime.of(hour + 1, 35));
            route.addStopOver(locationMap.get("F.Hendrikplantsoen"), LocalTime.of(hour + 1, 38), LocalTime.of(hour + 1, 38));
            route.addStopOver(locationMap.get("Nassaukade"), LocalTime.of(hour + 1, 39), LocalTime.of(hour + 1, 39));
            route.addStopOver(locationMap.get("De Wittenkade"), LocalTime.of(hour + 1, 40), LocalTime.of(hour + 1, 40));
            route.addStopOver(locationMap.get("Van L.Stirumstraat"), LocalTime.of(hour + 1, 40), LocalTime.of(hour + 1, 40));
            route.addEndPoint(locationMap.get("Van Hallstraat"), LocalTime.of(hour + 1, 42));
            addRoute(route);

            route = new Route(locationMap.get("Flevopark"), LocalTime.of(hour + 1,23));
            route.addStopOver(locationMap.get("Soembawastraat"), LocalTime.of(hour + 1, 23), LocalTime.of(hour + 1, 23));
            route.addStopOver(locationMap.get("Molukkenstraat"), LocalTime.of(hour + 1, 25), LocalTime.of(hour + 1, 25));
            route.addStopOver(locationMap.get("Muiderpoortstation"), LocalTime.of(hour + 1, 26), LocalTime.of(hour + 1, 26));
            route.addStopOver(locationMap.get("Dapperstraat"), LocalTime.of(hour + 1, 28), LocalTime.of(hour + 1, 28));
            route.addStopOver(locationMap.get("Linnaeusstraat"), LocalTime.of(hour + 1, 29), LocalTime.of(hour + 1, 29));
            route.addStopOver(locationMap.get("Beukenweg"), LocalTime.of(hour + 1, 32), LocalTime.of(hour + 1, 32));
            route.addStopOver(locationMap.get("Camperstraat"), LocalTime.of(hour + 1, 32), LocalTime.of(hour + 1, 32));
            route.addStopOver(locationMap.get("Wibautstraat"), LocalTime.of(hour + 1, 34), LocalTime.of(hour + 1, 34));
            route.addStopOver(locationMap.get("Van Woustraat"), LocalTime.of(hour + 1, 36), LocalTime.of(hour + 1, 36));
            route.addStopOver(locationMap.get("2e v.d.Helststraat"), LocalTime.of(hour + 1, 37), LocalTime.of(hour + 1, 37));
            route.addStopOver(locationMap.get("De Pijp"), LocalTime.of(hour + 1, 39), LocalTime.of(hour + 1, 39));
            route.addStopOver(locationMap.get("Roelof Hartplein"), LocalTime.of(hour + 1, 40), LocalTime.of(hour + 1, 40));
            route.addStopOver(locationMap.get("Concertgebouw"), LocalTime.of(hour + 1, 43), LocalTime.of(hour + 1, 43));
            route.addStopOver(locationMap.get("Van Baerlestraat"), LocalTime.of(hour + 1, 43), LocalTime.of(hour + 1, 43));
            route.addStopOver(locationMap.get("Overtoom"), LocalTime.of(hour + 1, 45), LocalTime.of(hour + 1, 45));
            route.addStopOver(locationMap.get("Kinkerstraat"), LocalTime.of(hour + 1, 47), LocalTime.of(hour + 1, 47));
            route.addStopOver(locationMap.get("De Clercqstraat"), LocalTime.of(hour + 1, 49), LocalTime.of(hour + 1, 49));
            route.addStopOver(locationMap.get("Hugo de Grootplein"), LocalTime.of(hour + 1, 50), LocalTime.of(hour + 1, 50));
            route.addStopOver(locationMap.get("F.Hendrikplantsoen"), LocalTime.of(hour + 1, 53), LocalTime.of(hour + 1, 53));
            route.addStopOver(locationMap.get("Nassaukade"), LocalTime.of(hour + 1, 54), LocalTime.of(hour + 1, 54));
            route.addStopOver(locationMap.get("De Wittenkade"), LocalTime.of(hour + 1, 55), LocalTime.of(hour + 1, 55));
            route.addStopOver(locationMap.get("Van L.Stirumstraat"), LocalTime.of(hour + 1, 55), LocalTime.of(hour + 1, 55));
            route.addEndPoint(locationMap.get("Van Hallstraat"), LocalTime.of(hour + 1, 57));
            addRoute(route);

            route = new Route(locationMap.get("Flevopark"), LocalTime.of(hour + 2 ,38));
            route.addStopOver(locationMap.get("Soembawastraat"), LocalTime.of(hour + 2, 38), LocalTime.of(hour + 2, 38));
            route.addStopOver(locationMap.get("Molukkenstraat"), LocalTime.of(hour + 2, 40), LocalTime.of(hour + 2, 40));
            route.addStopOver(locationMap.get("Muiderpoortstation"), LocalTime.of(hour + 2, 41), LocalTime.of(hour + 2, 41));
            route.addStopOver(locationMap.get("Dapperstraat"), LocalTime.of(hour + 2, 43), LocalTime.of(hour + 2, 43));
            route.addStopOver(locationMap.get("Linnaeusstraat"), LocalTime.of(hour + 2, 44), LocalTime.of(hour + 2, 44));
            route.addStopOver(locationMap.get("Beukenweg"), LocalTime.of(hour + 2, 47), LocalTime.of(hour + 2, 47));
            route.addStopOver(locationMap.get("Camperstraat"), LocalTime.of(hour + 2, 47), LocalTime.of(hour + 2, 47));
            route.addStopOver(locationMap.get("Wibautstraat"), LocalTime.of(hour + 2, 49), LocalTime.of(hour + 2, 49));
            route.addStopOver(locationMap.get("Van Woustraat"), LocalTime.of(hour + 2, 51), LocalTime.of(hour + 2, 51));
            route.addStopOver(locationMap.get("2e v.d.Helststraat"), LocalTime.of(hour + 2, 52), LocalTime.of(hour + 2, 52));
            route.addStopOver(locationMap.get("De Pijp"), LocalTime.of(hour + 2, 54), LocalTime.of(hour + 2, 54));
            route.addStopOver(locationMap.get("Roelof Hartplein"), LocalTime.of(hour + 2, 55), LocalTime.of(hour + 2, 55));
            route.addStopOver(locationMap.get("Concertgebouw"), LocalTime.of(hour + 2, 58), LocalTime.of(hour + 2, 58));
            route.addStopOver(locationMap.get("Van Baerlestraat"), LocalTime.of(hour + 2, 58), LocalTime.of(hour + 2, 58));
            route.addStopOver(locationMap.get("Overtoom"), LocalTime.of(hour + 3, 0), LocalTime.of(hour + 3, 0));
            route.addStopOver(locationMap.get("Kinkerstraat"), LocalTime.of(hour + 3, 2), LocalTime.of(hour + 3, 2));
            route.addStopOver(locationMap.get("De Clercqstraat"), LocalTime.of(hour + 3, 4), LocalTime.of(hour + 3, 4));
            route.addStopOver(locationMap.get("Hugo de Grootplein"), LocalTime.of(hour + 3, 5), LocalTime.of(hour + 3, 5));
            route.addStopOver(locationMap.get("F.Hendrikplantsoen"), LocalTime.of(hour + 3, 8), LocalTime.of(hour + 3, 8));
            route.addStopOver(locationMap.get("Nassaukade"), LocalTime.of(hour + 3, 9), LocalTime.of(hour + 3, 9));
            route.addStopOver(locationMap.get("De Wittenkade"), LocalTime.of(hour + 3, 10), LocalTime.of(hour + 3, 10));
            route.addStopOver(locationMap.get("Van L.Stirumstraat"), LocalTime.of(hour + 3, 10), LocalTime.of(hour + 3, 10));
            route.addEndPoint(locationMap.get("Van Hallstraat"), LocalTime.of(hour + 3, 12));
            addRoute(route);
        }

        for (int hour = 5; hour <= 21; hour++) {
            Route route = new Route(locationMap.get("Matterhorn"), LocalTime.of(hour, 49));
            route.addStopOver(locationMap.get("Pilatus"), LocalTime.of(hour, 49), LocalTime.of(hour, 49));
            route.addStopOver(locationMap.get("Inaristraat"), LocalTime.of(hour, 51), LocalTime.of(hour, 51));
            route.addStopOver(locationMap.get("Ecuplein"), LocalTime.of(hour, 52), LocalTime.of(hour, 52));
            route.addStopOver(locationMap.get("Baden Powellweg"), LocalTime.of(hour, 53), LocalTime.of(hour, 53));
            route.addStopOver(locationMap.get("Hoekenes"), LocalTime.of(hour, 55), LocalTime.of(hour, 55));
            route.addStopOver(locationMap.get("Louis Davidsstraat"), LocalTime.of(hour, 56), LocalTime.of(hour, 56));
            route.addStopOver(locationMap.get("Meer en Vaart"), LocalTime.of(hour, 58), LocalTime.of(hour, 58));
            route.addStopOver(locationMap.get("Johan Huizingalaan"), LocalTime.of(hour, 59), LocalTime.of(hour, 59));
            route.addStopOver(locationMap.get("Station Lelylaan"), LocalTime.of(hour + 1, 1), LocalTime.of(hour + 1, 1));
            route.addStopOver(locationMap.get("Derkinderenstraat"), LocalTime.of(hour + 1, 1), LocalTime.of(hour + 1, 1));
            route.addStopOver(locationMap.get("Surinameplein"), LocalTime.of(hour + 1, 4), LocalTime.of(hour + 1, 4));
            route.addStopOver(locationMap.get("Rhijnvis Feithstraat"), LocalTime.of(hour + 1, 6), LocalTime.of(hour + 1, 6));
            route.addStopOver(locationMap.get("J.P. Heijestraat"), LocalTime.of(hour + 1, 7), LocalTime.of(hour + 1, 7));
            route.addStopOver(locationMap.get("1e Con. Huygenstraat"), LocalTime.of(hour + 1, 9), LocalTime.of(hour + 1, 9));
            route.addStopOver(locationMap.get("Leidseplein"), LocalTime.of(hour + 1, 12), LocalTime.of(hour + 1, 12));
            route.addStopOver(locationMap.get("Spiegelgracht"), LocalTime.of(hour + 1, 13), LocalTime.of(hour + 1, 13));
            route.addStopOver(locationMap.get("Vijzelgracht"), LocalTime.of(hour + 1, 15), LocalTime.of(hour + 1, 15));
            route.addStopOver(locationMap.get("Frederiksplein"), LocalTime.of(hour + 1, 17), LocalTime.of(hour + 1, 17));
            route.addStopOver(locationMap.get("Weesperplein"), LocalTime.of(hour + 1, 21), LocalTime.of(hour + 1, 21));
            route.addStopOver(locationMap.get("K. 's-Gravesanderstr."), LocalTime.of(hour + 1, 22), LocalTime.of(hour + 1, 22));
            route.addStopOver(locationMap.get("Beukenweg"), LocalTime.of(hour + 1, 24), LocalTime.of(hour + 1, 24));
            route.addStopOver(locationMap.get("Linnaeusstraat"), LocalTime.of(hour + 1, 25), LocalTime.of(hour + 1, 25));
            route.addStopOver(locationMap.get("Dapperstraat"), LocalTime.of(hour + 1, 26), LocalTime.of(hour + 1, 26));
            route.addEndPoint(locationMap.get("Muiderpoortstation"), LocalTime.of(hour + 1, 28));
            addRoute(route);

            route = new Route(locationMap.get("Matterhorn"), LocalTime.of(hour + 1, 4));
            route.addStopOver(locationMap.get("Pilatus"), LocalTime.of(hour + 1, 4), LocalTime.of(hour + 1, 4));
            route.addStopOver(locationMap.get("Inaristraat"), LocalTime.of(hour + 1, 6), LocalTime.of(hour + 1, 6));
            route.addStopOver(locationMap.get("Ecuplein"), LocalTime.of(hour + 1, 7), LocalTime.of(hour + 1, 7));
            route.addStopOver(locationMap.get("Baden Powellweg"), LocalTime.of(hour + 1, 8), LocalTime.of(hour + 1, 8));
            route.addStopOver(locationMap.get("Hoekenes"), LocalTime.of(hour + 1, 10), LocalTime.of(hour + 1, 10));
            route.addStopOver(locationMap.get("Louis Davidsstraat"), LocalTime.of(hour + 1, 11), LocalTime.of(hour + 1, 11));
            route.addStopOver(locationMap.get("Meer en Vaart"), LocalTime.of(hour + 1, 13), LocalTime.of(hour + 1, 13));
            route.addStopOver(locationMap.get("Johan Huizingalaan"), LocalTime.of(hour + 1, 14), LocalTime.of(hour + 1, 14));
            route.addStopOver(locationMap.get("Station Lelylaan"), LocalTime.of(hour + 1, 16), LocalTime.of(hour + 1, 16));
            route.addStopOver(locationMap.get("Derkinderenstraat"), LocalTime.of(hour + 1, 16), LocalTime.of(hour + 1, 16));
            route.addStopOver(locationMap.get("Surinameplein"), LocalTime.of(hour + 1, 19), LocalTime.of(hour + 1, 19));
            route.addStopOver(locationMap.get("Rhijnvis Feithstraat"), LocalTime.of(hour + 1, 21), LocalTime.of(hour + 1, 21));
            route.addStopOver(locationMap.get("J.P. Heijestraat"), LocalTime.of(hour + 1, 22), LocalTime.of(hour + 1, 22));
            route.addStopOver(locationMap.get("1e Con. Huygenstraat"), LocalTime.of(hour + 1, 24), LocalTime.of(hour + 1, 24));
            route.addStopOver(locationMap.get("Leidseplein"), LocalTime.of(hour + 1, 27), LocalTime.of(hour + 1, 27));
            route.addStopOver(locationMap.get("Spiegelgracht"), LocalTime.of(hour + 1, 28), LocalTime.of(hour + 1, 28));
            route.addStopOver(locationMap.get("Vijzelgracht"), LocalTime.of(hour + 1, 30), LocalTime.of(hour + 1, 30));
            route.addStopOver(locationMap.get("Frederiksplein"), LocalTime.of(hour + 1, 32), LocalTime.of(hour + 1, 32));
            route.addStopOver(locationMap.get("Weesperplein"), LocalTime.of(hour + 1, 36), LocalTime.of(hour + 1, 36));
            route.addStopOver(locationMap.get("K. 's-Gravesanderstr."), LocalTime.of(hour + 1, 37), LocalTime.of(hour + 1, 37));
            route.addStopOver(locationMap.get("Beukenweg"), LocalTime.of(hour + 1, 39), LocalTime.of(hour + 1, 39));
            route.addStopOver(locationMap.get("Linnaeusstraat"), LocalTime.of(hour + 1, 40), LocalTime.of(hour + 1, 40));
            route.addStopOver(locationMap.get("Dapperstraat"), LocalTime.of(hour + 1, 41), LocalTime.of(hour + 1, 41));
            route.addEndPoint(locationMap.get("Muiderpoortstation"), LocalTime.of(hour + 1, 43));
            addRoute(route);

            route = new Route(locationMap.get("Matterhorn"), LocalTime.of(hour + 1, 19));
            route.addStopOver(locationMap.get("Pilatus"), LocalTime.of(hour + 1, 19), LocalTime.of(hour + 1, 19));
            route.addStopOver(locationMap.get("Inaristraat"), LocalTime.of(hour + 1, 21), LocalTime.of(hour + 1, 21));
            route.addStopOver(locationMap.get("Ecuplein"), LocalTime.of(hour + 1, 22), LocalTime.of(hour + 1, 22));
            route.addStopOver(locationMap.get("Baden Powellweg"), LocalTime.of(hour + 1, 23), LocalTime.of(hour + 1, 23));
            route.addStopOver(locationMap.get("Hoekenes"), LocalTime.of(hour + 1, 25), LocalTime.of(hour + 1, 25));
            route.addStopOver(locationMap.get("Louis Davidsstraat"), LocalTime.of(hour + 1, 26), LocalTime.of(hour + 1, 26));
            route.addStopOver(locationMap.get("Meer en Vaart"), LocalTime.of(hour + 1, 28), LocalTime.of(hour + 1, 28));
            route.addStopOver(locationMap.get("Johan Huizingalaan"), LocalTime.of(hour + 1, 29), LocalTime.of(hour + 1, 29));
            route.addStopOver(locationMap.get("Station Lelylaan"), LocalTime.of(hour + 1, 31), LocalTime.of(hour + 1, 31));
            route.addStopOver(locationMap.get("Derkinderenstraat"), LocalTime.of(hour + 1, 31), LocalTime.of(hour + 1, 31));
            route.addStopOver(locationMap.get("Surinameplein"), LocalTime.of(hour + 1, 34), LocalTime.of(hour + 1, 34));
            route.addStopOver(locationMap.get("Rhijnvis Feithstraat"), LocalTime.of(hour + 1, 36), LocalTime.of(hour + 1, 36));
            route.addStopOver(locationMap.get("J.P. Heijestraat"), LocalTime.of(hour + 1, 37), LocalTime.of(hour + 1, 37));
            route.addStopOver(locationMap.get("1e Con. Huygenstraat"), LocalTime.of(hour + 1, 39), LocalTime.of(hour + 1, 39));
            route.addStopOver(locationMap.get("Leidseplein"), LocalTime.of(hour + 1, 42), LocalTime.of(hour + 1, 42));
            route.addStopOver(locationMap.get("Spiegelgracht"), LocalTime.of(hour + 1, 43), LocalTime.of(hour + 1, 43));
            route.addStopOver(locationMap.get("Vijzelgracht"), LocalTime.of(hour + 1, 45), LocalTime.of(hour + 1, 45));
            route.addStopOver(locationMap.get("Frederiksplein"), LocalTime.of(hour + 1, 47), LocalTime.of(hour + 1, 47));
            route.addStopOver(locationMap.get("Weesperplein"), LocalTime.of(hour + 1, 51), LocalTime.of(hour + 1, 51));
            route.addStopOver(locationMap.get("K. 's-Gravesanderstr."), LocalTime.of(hour + 1, 52), LocalTime.of(hour + 1, 52));
            route.addStopOver(locationMap.get("Beukenweg"), LocalTime.of(hour + 1, 54), LocalTime.of(hour + 1, 54));
            route.addStopOver(locationMap.get("Linnaeusstraat"), LocalTime.of(hour + 1, 55), LocalTime.of(hour + 1, 55));
            route.addStopOver(locationMap.get("Dapperstraat"), LocalTime.of(hour + 1, 56), LocalTime.of(hour + 1, 56));
            route.addEndPoint(locationMap.get("Muiderpoortstation"), LocalTime.of(hour + 1, 58));
            addRoute(route);

            route = new Route(locationMap.get("Matterhorn"), LocalTime.of(hour + 1, 34));
            route.addStopOver(locationMap.get("Pilatus"), LocalTime.of(hour + 1, 34), LocalTime.of(hour + 1, 34));
            route.addStopOver(locationMap.get("Inaristraat"), LocalTime.of(hour + 1, 36), LocalTime.of(hour + 1, 36));
            route.addStopOver(locationMap.get("Ecuplein"), LocalTime.of(hour + 1, 37), LocalTime.of(hour + 1, 37));
            route.addStopOver(locationMap.get("Baden Powellweg"), LocalTime.of(hour + 1, 38), LocalTime.of(hour + 1, 38));
            route.addStopOver(locationMap.get("Hoekenes"), LocalTime.of(hour + 1, 40), LocalTime.of(hour + 1, 40));
            route.addStopOver(locationMap.get("Louis Davidsstraat"), LocalTime.of(hour + 1, 41), LocalTime.of(hour + 1, 41));
            route.addStopOver(locationMap.get("Meer en Vaart"), LocalTime.of(hour + 1, 43), LocalTime.of(hour + 1, 43));
            route.addStopOver(locationMap.get("Johan Huizingalaan"), LocalTime.of(hour + 1, 44), LocalTime.of(hour + 1, 44));
            route.addStopOver(locationMap.get("Station Lelylaan"), LocalTime.of(hour + 1, 46), LocalTime.of(hour + 1, 46));
            route.addStopOver(locationMap.get("Derkinderenstraat"), LocalTime.of(hour + 1, 46), LocalTime.of(hour + 1, 46));
            route.addStopOver(locationMap.get("Surinameplein"), LocalTime.of(hour + 1, 49), LocalTime.of(hour + 1, 49));
            route.addStopOver(locationMap.get("Rhijnvis Feithstraat"), LocalTime.of(hour + 1, 51), LocalTime.of(hour + 1, 51));
            route.addStopOver(locationMap.get("J.P. Heijestraat"), LocalTime.of(hour + 1, 52), LocalTime.of(hour + 1, 52));
            route.addStopOver(locationMap.get("1e Con. Huygenstraat"), LocalTime.of(hour + 1, 54), LocalTime.of(hour + 1, 54));
            route.addStopOver(locationMap.get("Leidseplein"), LocalTime.of(hour + 1, 57), LocalTime.of(hour + 1, 57));
            route.addStopOver(locationMap.get("Spiegelgracht"), LocalTime.of(hour + 1, 58), LocalTime.of(hour + 1, 58));
            route.addStopOver(locationMap.get("Vijzelgracht"), LocalTime.of(hour + 2, 0), LocalTime.of(hour + 2, 0));
            route.addStopOver(locationMap.get("Frederiksplein"), LocalTime.of(hour + 2, 2), LocalTime.of(hour + 2, 2));
            route.addStopOver(locationMap.get("Weesperplein"), LocalTime.of(hour + 2, 6), LocalTime.of(hour + 2, 6));
            route.addStopOver(locationMap.get("K. 's-Gravesanderstr."), LocalTime.of(hour + 2, 7), LocalTime.of(hour + 2, 7));
            route.addStopOver(locationMap.get("Beukenweg"), LocalTime.of(hour + 2, 9), LocalTime.of(hour + 2, 9));
            route.addStopOver(locationMap.get("Linnaeusstraat"), LocalTime.of(hour + 2, 10), LocalTime.of(hour + 2, 10));
            route.addStopOver(locationMap.get("Dapperstraat"), LocalTime.of(hour + 2, 11), LocalTime.of(hour + 2, 11));
            route.addEndPoint(locationMap.get("Muiderpoortstation"), LocalTime.of(hour + 2, 13));
            addRoute(route);
        }

        for (int hour = 6; hour <= 21; hour++) {
            Route route = new Route(locationMap.get("Muiderpoortstation"), LocalTime.of(hour, 49));
            route.addStopOver(locationMap.get("Dapperstraat"), LocalTime.of(hour, 49), LocalTime.of(hour, 49));
            route.addStopOver(locationMap.get("Inaristraat"), LocalTime.of(hour, 51), LocalTime.of(hour, 51));
            route.addStopOver(locationMap.get("Linnaeusstraat"), LocalTime.of(hour, 52), LocalTime.of(hour, 52));
            route.addStopOver(locationMap.get("Beukenweg"), LocalTime.of(hour, 53), LocalTime.of(hour, 53));
            route.addStopOver(locationMap.get("Weesperplein"), LocalTime.of(hour, 55), LocalTime.of(hour, 55));
            route.addStopOver(locationMap.get("Frederiksplein"), LocalTime.of(hour, 56), LocalTime.of(hour, 56));
            route.addStopOver(locationMap.get("Vijzelgracht"), LocalTime.of(hour, 58), LocalTime.of(hour, 58));
            route.addStopOver(locationMap.get("Spiegelgracht"), LocalTime.of(hour, 59), LocalTime.of(hour, 59));
            route.addStopOver(locationMap.get("Leidseplein"), LocalTime.of(hour + 1, 1), LocalTime.of(hour + 1, 1));
            route.addStopOver(locationMap.get("1e Con. Huygenstraat"), LocalTime.of(hour + 1, 1), LocalTime.of(hour + 1, 1));
            route.addStopOver(locationMap.get("J.P. Heijestraat"), LocalTime.of(hour + 1, 4), LocalTime.of(hour + 1, 4));
            route.addStopOver(locationMap.get("Rhijnvis Feithstraat"), LocalTime.of(hour + 1, 6), LocalTime.of(hour + 1, 6));
            route.addStopOver(locationMap.get("Surinameplein"), LocalTime.of(hour + 1, 7), LocalTime.of(hour + 1, 7));
            route.addStopOver(locationMap.get("Derkinderenstraat"), LocalTime.of(hour + 1, 9), LocalTime.of(hour + 1, 9));
            route.addStopOver(locationMap.get("Station Lelylaan"), LocalTime.of(hour + 1, 12), LocalTime.of(hour + 1, 12));
            route.addStopOver(locationMap.get("Johan Huizingalaan"), LocalTime.of(hour + 1, 13), LocalTime.of(hour + 1, 13));
            route.addStopOver(locationMap.get("Meer en Vaart"), LocalTime.of(hour + 1, 15), LocalTime.of(hour + 1, 15));
            route.addStopOver(locationMap.get("Louis Davidsstraat"), LocalTime.of(hour + 1, 17), LocalTime.of(hour + 1, 17));
            route.addStopOver(locationMap.get("Hoekenes"), LocalTime.of(hour + 1, 21), LocalTime.of(hour + 1, 21));
            route.addStopOver(locationMap.get("Baden Powellweg"), LocalTime.of(hour + 1, 22), LocalTime.of(hour + 1, 22));
            route.addStopOver(locationMap.get("Ecuplein"), LocalTime.of(hour + 1, 24), LocalTime.of(hour + 1, 24));
            route.addStopOver(locationMap.get("Inaristraat"), LocalTime.of(hour + 1, 25), LocalTime.of(hour + 1, 25));
            route.addStopOver(locationMap.get("Pilatus"), LocalTime.of(hour + 1, 26), LocalTime.of(hour + 1, 26));
            route.addEndPoint(locationMap.get("Matterhorn"), LocalTime.of(hour + 1, 28));
            addRoute(route);

            route = new Route(locationMap.get("Muiderpoortstation"), LocalTime.of(hour + 1, 4));
            route.addStopOver(locationMap.get("Dapperstraat"), LocalTime.of(hour + 1, 4), LocalTime.of(hour + 1, 4));
            route.addStopOver(locationMap.get("Inaristraat"), LocalTime.of(hour + 1, 6), LocalTime.of(hour + 1, 6));
            route.addStopOver(locationMap.get("Linnaeusstraat"), LocalTime.of(hour + 1, 7), LocalTime.of(hour + 1, 7));
            route.addStopOver(locationMap.get("Beukenweg"), LocalTime.of(hour + 1, 8), LocalTime.of(hour + 1, 8));
            route.addStopOver(locationMap.get("Weesperplein"), LocalTime.of(hour + 1, 10), LocalTime.of(hour + 1, 10));
            route.addStopOver(locationMap.get("Frederiksplein"), LocalTime.of(hour + 1, 11), LocalTime.of(hour + 1, 11));
            route.addStopOver(locationMap.get("Vijzelgracht"), LocalTime.of(hour + 1, 13), LocalTime.of(hour + 1, 13));
            route.addStopOver(locationMap.get("Spiegelgracht"), LocalTime.of(hour + 1, 14), LocalTime.of(hour + 1, 14));
            route.addStopOver(locationMap.get("Leidseplein"), LocalTime.of(hour + 1, 16), LocalTime.of(hour + 1, 16));
            route.addStopOver(locationMap.get("1e Con. Huygenstraat"), LocalTime.of(hour + 1, 16), LocalTime.of(hour + 1, 16));
            route.addStopOver(locationMap.get("J.P. Heijestraat"), LocalTime.of(hour + 1, 19), LocalTime.of(hour + 1, 19));
            route.addStopOver(locationMap.get("Rhijnvis Feithstraat"), LocalTime.of(hour + 1, 21), LocalTime.of(hour + 1, 21));
            route.addStopOver(locationMap.get("Surinameplein"), LocalTime.of(hour + 1, 22), LocalTime.of(hour + 1, 22));
            route.addStopOver(locationMap.get("Derkinderenstraat"), LocalTime.of(hour + 1, 24), LocalTime.of(hour + 1, 24));
            route.addStopOver(locationMap.get("Station Lelylaan"), LocalTime.of(hour + 1, 27), LocalTime.of(hour + 1, 27));
            route.addStopOver(locationMap.get("Johan Huizingalaan"), LocalTime.of(hour + 1, 28), LocalTime.of(hour + 1, 28));
            route.addStopOver(locationMap.get("Meer en Vaart"), LocalTime.of(hour + 1, 30), LocalTime.of(hour + 1, 30));
            route.addStopOver(locationMap.get("Louis Davidsstraat"), LocalTime.of(hour + 1, 32), LocalTime.of(hour + 1, 32));
            route.addStopOver(locationMap.get("Hoekenes"), LocalTime.of(hour + 1, 36), LocalTime.of(hour + 1, 36));
            route.addStopOver(locationMap.get("Baden Powellweg"), LocalTime.of(hour + 1, 37), LocalTime.of(hour + 1, 37));
            route.addStopOver(locationMap.get("Ecuplein"), LocalTime.of(hour + 1, 39), LocalTime.of(hour + 1, 39));
            route.addStopOver(locationMap.get("Inaristraat"), LocalTime.of(hour + 1, 40), LocalTime.of(hour + 1, 40));
            route.addStopOver(locationMap.get("Pilatus"), LocalTime.of(hour + 1, 41), LocalTime.of(hour + 1, 41));
            route.addEndPoint(locationMap.get("Matterhorn"), LocalTime.of(hour + 1, 43));
            addRoute(route);

            route = new Route(locationMap.get("Muiderpoortstation"), LocalTime.of(hour + 1, 19));
            route.addStopOver(locationMap.get("Dapperstraat"), LocalTime.of(hour + 1, 19), LocalTime.of(hour + 1, 19));
            route.addStopOver(locationMap.get("Inaristraat"), LocalTime.of(hour + 1, 21), LocalTime.of(hour + 1, 21));
            route.addStopOver(locationMap.get("Linnaeusstraat"), LocalTime.of(hour + 1, 22), LocalTime.of(hour + 1, 22));
            route.addStopOver(locationMap.get("Beukenweg"), LocalTime.of(hour + 1, 23), LocalTime.of(hour + 1, 23));
            route.addStopOver(locationMap.get("Weesperplein"), LocalTime.of(hour + 1, 25), LocalTime.of(hour + 1, 25));
            route.addStopOver(locationMap.get("Frederiksplein"), LocalTime.of(hour + 1, 26), LocalTime.of(hour + 1, 26));
            route.addStopOver(locationMap.get("Vijzelgracht"), LocalTime.of(hour + 1, 28), LocalTime.of(hour + 1, 28));
            route.addStopOver(locationMap.get("Spiegelgracht"), LocalTime.of(hour + 1, 29), LocalTime.of(hour + 1, 29));
            route.addStopOver(locationMap.get("Leidseplein"), LocalTime.of(hour + 1, 31), LocalTime.of(hour + 1, 31));
            route.addStopOver(locationMap.get("1e Con. Huygenstraat"), LocalTime.of(hour + 1, 31), LocalTime.of(hour + 1, 31));
            route.addStopOver(locationMap.get("J.P. Heijestraat"), LocalTime.of(hour + 1, 34), LocalTime.of(hour + 1, 34));
            route.addStopOver(locationMap.get("Rhijnvis Feithstraat"), LocalTime.of(hour + 1, 36), LocalTime.of(hour + 1, 36));
            route.addStopOver(locationMap.get("Surinameplein"), LocalTime.of(hour + 1, 37), LocalTime.of(hour + 1, 37));
            route.addStopOver(locationMap.get("Derkinderenstraat"), LocalTime.of(hour + 1, 39), LocalTime.of(hour + 1, 39));
            route.addStopOver(locationMap.get("Station Lelylaan"), LocalTime.of(hour + 1, 42), LocalTime.of(hour + 1, 42));
            route.addStopOver(locationMap.get("Johan Huizingalaan"), LocalTime.of(hour + 1, 43), LocalTime.of(hour + 1, 43));
            route.addStopOver(locationMap.get("Meer en Vaart"), LocalTime.of(hour + 1, 45), LocalTime.of(hour + 1, 45));
            route.addStopOver(locationMap.get("Louis Davidsstraat"), LocalTime.of(hour + 1, 47), LocalTime.of(hour + 1, 47));
            route.addStopOver(locationMap.get("Hoekenes"), LocalTime.of(hour + 1, 51), LocalTime.of(hour + 1, 51));
            route.addStopOver(locationMap.get("Baden Powellweg"), LocalTime.of(hour + 1, 52), LocalTime.of(hour + 1, 52));
            route.addStopOver(locationMap.get("Ecuplein"), LocalTime.of(hour + 1, 54), LocalTime.of(hour + 1, 54));
            route.addStopOver(locationMap.get("Inaristraat"), LocalTime.of(hour + 1, 55), LocalTime.of(hour + 1, 55));
            route.addStopOver(locationMap.get("Pilatus"), LocalTime.of(hour + 1, 56), LocalTime.of(hour + 1, 56));
            route.addEndPoint(locationMap.get("Matterhorn"), LocalTime.of(hour + 1, 58));
            addRoute(route);

            route = new Route(locationMap.get("Muiderpoortstation"), LocalTime.of(hour + 1, 34));
            route.addStopOver(locationMap.get("Dapperstraat"), LocalTime.of(hour + 1, 34), LocalTime.of(hour + 1, 34));
            route.addStopOver(locationMap.get("Inaristraat"), LocalTime.of(hour + 1, 36), LocalTime.of(hour + 1, 36));
            route.addStopOver(locationMap.get("Linnaeusstraat"), LocalTime.of(hour + 1, 37), LocalTime.of(hour + 1, 37));
            route.addStopOver(locationMap.get("Beukenweg"), LocalTime.of(hour + 1, 38), LocalTime.of(hour + 1, 38));
            route.addStopOver(locationMap.get("Weesperplein"), LocalTime.of(hour + 1, 40), LocalTime.of(hour + 1, 40));
            route.addStopOver(locationMap.get("Frederiksplein"), LocalTime.of(hour + 1, 41), LocalTime.of(hour + 1, 41));
            route.addStopOver(locationMap.get("Vijzelgracht"), LocalTime.of(hour + 1, 43), LocalTime.of(hour + 1, 43));
            route.addStopOver(locationMap.get("Spiegelgracht"), LocalTime.of(hour + 1, 44), LocalTime.of(hour + 1, 44));
            route.addStopOver(locationMap.get("Leidseplein"), LocalTime.of(hour + 1, 46), LocalTime.of(hour + 1, 46));
            route.addStopOver(locationMap.get("1e Con. Huygenstraat"), LocalTime.of(hour + 1, 46), LocalTime.of(hour + 1, 46));
            route.addStopOver(locationMap.get("J.P. Heijestraat"), LocalTime.of(hour + 1, 49), LocalTime.of(hour + 1, 49));
            route.addStopOver(locationMap.get("Rhijnvis Feithstraat"), LocalTime.of(hour + 1, 51), LocalTime.of(hour + 1, 51));
            route.addStopOver(locationMap.get("Surinameplein"), LocalTime.of(hour + 1, 52), LocalTime.of(hour + 1, 52));
            route.addStopOver(locationMap.get("Derkinderenstraat"), LocalTime.of(hour + 1, 54), LocalTime.of(hour + 1, 54));
            route.addStopOver(locationMap.get("Station Lelylaan"), LocalTime.of(hour + 1, 57), LocalTime.of(hour + 1, 57));
            route.addStopOver(locationMap.get("Johan Huizingalaan"), LocalTime.of(hour + 1, 58), LocalTime.of(hour + 1, 58));
            route.addStopOver(locationMap.get("Meer en Vaart"), LocalTime.of(hour + 2, 0), LocalTime.of(hour + 2, 0));
            route.addStopOver(locationMap.get("Louis Davidsstraat"), LocalTime.of(hour + 2, 2), LocalTime.of(hour + 2, 2));
            route.addStopOver(locationMap.get("Hoekenes"), LocalTime.of(hour + 2, 6), LocalTime.of(hour + 2, 6));
            route.addStopOver(locationMap.get("Baden Powellweg"), LocalTime.of(hour + 2, 7), LocalTime.of(hour + 2, 7));
            route.addStopOver(locationMap.get("Ecuplein"), LocalTime.of(hour + 2, 9), LocalTime.of(hour + 2, 9));
            route.addStopOver(locationMap.get("Inaristraat"), LocalTime.of(hour + 2, 10), LocalTime.of(hour + 2, 10));
            route.addStopOver(locationMap.get("Pilatus"), LocalTime.of(hour + 2, 11), LocalTime.of(hour + 2, 11));
            route.addEndPoint(locationMap.get("Matterhorn"), LocalTime.of(hour + 2, 13));
            addRoute(route);
        }
        for (int hour = 5; hour <= 21; hour++) {
            Route route = new Route(locationMap.get("Oudenaardeplantsoen"), LocalTime.of(hour, 50));
            route.addStopOver(locationMap.get("Centrum Nieuwe Sloten"), LocalTime.of(hour, 51), LocalTime.of(hour, 51));
            route.addStopOver(locationMap.get("Laan v.Vlaanderen"), LocalTime.of(hour, 52), LocalTime.of(hour, 52));
            route.addStopOver(locationMap.get("Louwesweg"), LocalTime.of(hour, 53), LocalTime.of(hour, 53));
            route.addStopOver(locationMap.get("Johan Huizingalaan"), LocalTime.of(hour, 55), LocalTime.of(hour, 55));
            route.addStopOver(locationMap.get("Heemstedestraat"), LocalTime.of(hour, 57), LocalTime.of(hour, 57));
            route.addStopOver(locationMap.get("Delflandlaan"), LocalTime.of(hour, 59), LocalTime.of(hour, 59));
            route.addStopOver(locationMap.get("Westlandgracht"), LocalTime.of(hour +1,0 ), LocalTime.of(hour +1, 0));
            route.addStopOver(locationMap.get("Hoofddorpplein"), LocalTime.of(hour +1, 1), LocalTime.of(hour +1, 1));
            route.addStopOver(locationMap.get("Amstelveenseweg"), LocalTime.of(hour +1, 2), LocalTime.of(hour +1, 2));
            route.addStopOver(locationMap.get("Valeriusplein"), LocalTime.of(hour +1, 4), LocalTime.of(hour +1, 4));
            route.addStopOver(locationMap.get("Corn. Schuytstraat"), LocalTime.of(hour +1, 5), LocalTime.of(hour +1, 5));
            route.addStopOver(locationMap.get("Museumplein"), LocalTime.of(hour +1, 8), LocalTime.of(hour +1, 8));
            route.addStopOver(locationMap.get("Leidseplein"), LocalTime.of(hour +1, 12), LocalTime.of(hour +1, 12));
            route.addStopOver(locationMap.get("Prinsengracht"), LocalTime.of(hour +1, 13), LocalTime.of(hour +1, 13));
            route.addStopOver(locationMap.get("Keizersgracht"), LocalTime.of(hour +1, 14), LocalTime.of(hour +1, 14));
            route.addStopOver(locationMap.get("Koningsplein"), LocalTime.of(hour +1, 15), LocalTime.of(hour +1, 15));
            route.addStopOver(locationMap.get("Dam"), LocalTime.of(hour +1, 18), LocalTime.of(hour +1, 18));
            route.addStopOver(locationMap.get("Nieuwezijds Kolk"), LocalTime.of(hour +1, 19), LocalTime.of(hour +1, 19));
            route.addEndPoint(locationMap.get("Centraal Station"), LocalTime.of(hour +1, 23));
            addRoute(route);
        }

        for (int hour = 6; hour <= 22; hour++) {
            Route route = new Route(locationMap.get("Oudenaardeplantsoen"), LocalTime.of(hour, 20));
            route.addStopOver(locationMap.get("Centrum Nieuwe Sloten"), LocalTime.of(hour, 21), LocalTime.of(hour, 21));
            route.addStopOver(locationMap.get("Laan v.Vlaanderen"), LocalTime.of(hour, 22), LocalTime.of(hour, 22));
            route.addStopOver(locationMap.get("Louwesweg"), LocalTime.of(hour, 23), LocalTime.of(hour, 23));
            route.addStopOver(locationMap.get("Johan Huizingalaan"), LocalTime.of(hour, 25), LocalTime.of(hour, 25));
            route.addStopOver(locationMap.get("Heemstedestraat"), LocalTime.of(hour, 27), LocalTime.of(hour, 27));
            route.addStopOver(locationMap.get("Delflandlaan"), LocalTime.of(hour, 29), LocalTime.of(hour, 29));
            route.addStopOver(locationMap.get("Westlandgracht"), LocalTime.of(hour ,30 ), LocalTime.of(hour , 30));
            route.addStopOver(locationMap.get("Hoofddorpplein"), LocalTime.of(hour , 31), LocalTime.of(hour , 31));
            route.addStopOver(locationMap.get("Amstelveenseweg"), LocalTime.of(hour , 32), LocalTime.of(hour , 32));
            route.addStopOver(locationMap.get("Valeriusplein"), LocalTime.of(hour , 34), LocalTime.of(hour , 34));
            route.addStopOver(locationMap.get("Corn. Schuytstraat"), LocalTime.of(hour , 35), LocalTime.of(hour , 35));
            route.addStopOver(locationMap.get("Museumplein"), LocalTime.of(hour , 38), LocalTime.of(hour , 38));
            route.addStopOver(locationMap.get("Leidseplein"), LocalTime.of(hour , 42), LocalTime.of(hour , 42));
            route.addStopOver(locationMap.get("Prinsengracht"), LocalTime.of(hour , 43), LocalTime.of(hour , 43));
            route.addStopOver(locationMap.get("Keizersgracht"), LocalTime.of(hour , 44), LocalTime.of(hour, 44));
            route.addStopOver(locationMap.get("Koningsplein"), LocalTime.of(hour , 45), LocalTime.of(hour , 45));
            route.addStopOver(locationMap.get("Dam"), LocalTime.of(hour , 48), LocalTime.of(hour , 48));
            route.addStopOver(locationMap.get("Nieuwezijds Kolk"), LocalTime.of(hour , 49), LocalTime.of(hour , 49));
            route.addEndPoint(locationMap.get("Centraal Station"), LocalTime.of(hour , 53));
            addRoute(route);
        }

        for (int hour = 6; hour <= 22; hour++) {
            Route route = new Route(locationMap.get("Centraal Station"), LocalTime.of(hour, 25));
            route.addStopOver(locationMap.get("Nieuwezijds Kolk"), LocalTime.of(hour, 27), LocalTime.of(hour, 27));
            route.addStopOver(locationMap.get("Dam"), LocalTime.of(hour, 29), LocalTime.of(hour, 29));
            route.addStopOver(locationMap.get("Koningsplein"), LocalTime.of(hour, 31), LocalTime.of(hour, 31));
            route.addStopOver(locationMap.get("Keizersgracht"), LocalTime.of(hour, 33), LocalTime.of(hour, 33));
            route.addStopOver(locationMap.get("Prinsengracht"), LocalTime.of(hour, 34), LocalTime.of(hour, 34));
            route.addStopOver(locationMap.get("Leidseplein"), LocalTime.of(hour, 36), LocalTime.of(hour, 36));
            route.addStopOver(locationMap.get("Museumplein"), LocalTime.of(hour ,40 ), LocalTime.of(hour , 40));
            route.addStopOver(locationMap.get("Corn. Schuytstraat"), LocalTime.of(hour , 41), LocalTime.of(hour , 41));
            route.addStopOver(locationMap.get("Valeriusplein"), LocalTime.of(hour , 43), LocalTime.of(hour , 43));
            route.addStopOver(locationMap.get("Amstelveenseweg"), LocalTime.of(hour , 44), LocalTime.of(hour , 44));
            route.addStopOver(locationMap.get("Hoofddorpplein"), LocalTime.of(hour , 47), LocalTime.of(hour , 47));
            route.addStopOver(locationMap.get("Westlandgracht"), LocalTime.of(hour , 48), LocalTime.of(hour , 48));
            route.addStopOver(locationMap.get("Delflandlaan"), LocalTime.of(hour , 49), LocalTime.of(hour , 49));
            route.addStopOver(locationMap.get("Heemstedestraat"), LocalTime.of(hour , 50), LocalTime.of(hour , 50));
            route.addStopOver(locationMap.get("Johan Huizingalaan"), LocalTime.of(hour , 52), LocalTime.of(hour, 52));
            route.addStopOver(locationMap.get("Louwesweg"), LocalTime.of(hour , 54), LocalTime.of(hour , 54));
            route.addStopOver(locationMap.get("Laan v.Vlaanderen"), LocalTime.of(hour , 55), LocalTime.of(hour , 55));
            route.addStopOver(locationMap.get("Centrum Nieuwe Sloten"), LocalTime.of(hour , 56), LocalTime.of(hour , 56));
            route.addEndPoint(locationMap.get("Oudenaardeplantsoen"), LocalTime.of(hour , 58));
            addRoute(route);
        }

        for (int hour = 6; hour <= 22; hour++) {
            Route route = new Route(locationMap.get("Centraal Station"), LocalTime.of(hour, 55));
            route.addStopOver(locationMap.get("Nieuwezijds Kolk"), LocalTime.of(hour, 57), LocalTime.of(hour, 57));
            route.addStopOver(locationMap.get("Dam"), LocalTime.of(hour, 59), LocalTime.of(hour, 59));
            route.addStopOver(locationMap.get("Koningsplein"), LocalTime.of(hour +1, 1), LocalTime.of(hour +1, 1));
            route.addStopOver(locationMap.get("Keizersgracht"), LocalTime.of(hour +1, 3), LocalTime.of(hour +1, 3));
            route.addStopOver(locationMap.get("Prinsengracht"), LocalTime.of(hour +1, 4), LocalTime.of(hour +1, 4));
            route.addStopOver(locationMap.get("Leidseplein"), LocalTime.of(hour +1, 6), LocalTime.of(hour +1, 6));
            route.addStopOver(locationMap.get("Museumplein"), LocalTime.of(hour +1,10 ), LocalTime.of(hour +1, 10));
            route.addStopOver(locationMap.get("Corn. Schuytstraat"), LocalTime.of(hour +1, 11), LocalTime.of(hour +1, 11));
            route.addStopOver(locationMap.get("Valeriusplein"), LocalTime.of(hour +1, 13), LocalTime.of(hour +1, 13));
            route.addStopOver(locationMap.get("Amstelveenseweg"), LocalTime.of(hour +1, 14), LocalTime.of(hour +1, 14));
            route.addStopOver(locationMap.get("Hoofddorpplein"), LocalTime.of(hour +1, 17), LocalTime.of(hour +1, 17));
            route.addStopOver(locationMap.get("Westlandgracht"), LocalTime.of(hour +1, 18), LocalTime.of(hour +1, 18));
            route.addStopOver(locationMap.get("Delflandlaan"), LocalTime.of(hour +1, 19), LocalTime.of(hour +1, 19));
            route.addStopOver(locationMap.get("Heemstedestraat"), LocalTime.of(hour +1, 20), LocalTime.of(hour +1, 20));
            route.addStopOver(locationMap.get("Johan Huizingalaan"), LocalTime.of(hour +1, 22), LocalTime.of(hour +1, 22));
            route.addStopOver(locationMap.get("Louwesweg"), LocalTime.of(hour +1, 24), LocalTime.of(hour +1, 24));
            route.addStopOver(locationMap.get("Laan v.Vlaanderen"), LocalTime.of(hour +1, 25), LocalTime.of(hour +1, 25));
            route.addStopOver(locationMap.get("Centrum Nieuwe Sloten"), LocalTime.of(hour +1, 26), LocalTime.of(hour +1, 26));
            route.addEndPoint(locationMap.get("Oudenaardeplantsoen"), LocalTime.of(hour +1, 28));
            addRoute(route);
        }
    }
}
