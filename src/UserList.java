//Nicky Hellemons
import java.util.ArrayList;

public class UserList
{
    private ArrayList<User> userList;

    public void add(User user){userList.add(user);}

    public User get(int index){return userList.get(index);}

    public void remove(int index){userList.remove(index);}

    public int sizeOf(){return userList.size();}
}
