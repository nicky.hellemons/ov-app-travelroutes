import javax.imageio.ImageIO;
import javax.swing.*;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.Date;

public class PrintScreen extends GUI {

    public static void printScreen(Frame frame) throws AWTException, IOException {
        JDialog di = new JDialog(frame, rb.getString("screen.shot"), false);
        di.setBounds(10, 10, frame.getWidth(), frame.getHeight());
        di.setVisible(true);
        di.setDefaultCloseOperation(JDialog.HIDE_ON_CLOSE);

        JLabel imageLabel = new JLabel();
        imageLabel.setSize(frame.getSize());

        Rectangle area = frame.getBounds();
        BufferedImage bi = new BufferedImage(area.width, area.height, BufferedImage.TYPE_INT_ARGB);
        frame.paint(bi.getGraphics());

        imageLabel.setIcon(new ImageIcon(bi));

        di.add(imageLabel);
        {
        }
        //Change the pathname to the pathname of your laptop
        String home = System.getProperty("user.home");
        ImageIO.write(bi, "png", new File(home + "/Downloads/" +
                "" + new Date().getTime() + ".png"));
    }
}
