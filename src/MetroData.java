import java.time.LocalTime;

//Nicky & Koen
public class MetroData extends Data
{
    public MetroData()
    {
        //////////Locations
        Location location = new Location("Hoek van Holland Haven", new Coordinates(51.97506246375524, 4.128332936847622));
        locationMap.put(location.getLocationName(), location);

        location = new Location("Steendijkpolder", new Coordinates(51.932827684660836, 4.224785841905367));
        locationMap.put(location.getLocationName(), location);

        location = new Location("Maassluis West", new Coordinates(51.92647521958823, 4.235092868805831));
        locationMap.put(location.getLocationName(), location);

        location = new Location("Maassluis Centrum", new Coordinates(51.91650980323297, 4.253953147367186));
        locationMap.put(location.getLocationName(), location);

        location = new Location("Vlaardingen West", new Coordinates(51.90407193043859, 4.314310697640346));
        locationMap.put(location.getLocationName(), location);

        location = new Location("Vlaardingen Centrum", new Coordinates(51.91650980323297, 4.253953147367186));
        locationMap.put(location.getLocationName(), location);

        location = new Location("Vlaardingen Oost", new Coordinates(51.91030148822687, 4.361604611133774));
        locationMap.put(location.getLocationName(), location);

        location = new Location("Schiedam Nieuwland", new Coordinates(51.92298723038481, 4.383773784148193));
        locationMap.put(location.getLocationName(), location);

        location = new Location("Schiedam Centrum", new Coordinates(51.9219089674606, 4.409519568805597));
        locationMap.put(location.getLocationName(), location);

        location = new Location("Marconiplein", new Coordinates(51.9133863970205, 4.432750639969553));
        locationMap.put(location.getLocationName(), location);

        location = new Location("Delfshaven", new Coordinates(51.9100904516908, 4.445955768805007));
        locationMap.put(location.getLocationName(), location);

        location = new Location("Coolhaven", new Coordinates(51.909423634949285, 4.457762526476221));
        locationMap.put(location.getLocationName(), location);

        location = new Location("Dijkzigt", new Coordinates(51.91250109935154, 4.466419012983261));
        locationMap.put(location.getLocationName(), location);

        location = new Location("Eendrachtsplein", new Coordinates(51.91637508012267, 4.473577668805335));
        locationMap.put(location.getLocationName(), location);

        location = new Location("Beurs", new Coordinates(51.918404492419384, 4.481229924627306));
        locationMap.put(location.getLocationName(), location);

        location = new Location("Blaak", new Coordinates(51.92005923944753, 4.489718353462996));
        locationMap.put(location.getLocationName(), location);

        location = new Location("Oostplein", new Coordinates(51.923234246293546, 4.496553439970053));
        locationMap.put(location.getLocationName(), location);

        location = new Location("Gerdesiaweg", new Coordinates(51.92603012096599, 4.50604305531267));
        locationMap.put(location.getLocationName(), location);

        location = new Location("Voorschoterlaan", new Coordinates(51.92521462349025, 4.512629355312632));
        locationMap.put(location.getLocationName(), location);

        location = new Location("Kralingse Zoom", new Coordinates(51.921630700767146, 4.5339358111343495));
        locationMap.put(location.getLocationName(), location);

        location = new Location("Capelsebrug", new Coordinates(51.921240118826255, 4.557447468805571));
        locationMap.put(location.getLocationName(), location);

        location = new Location("Schenkel", new Coordinates(51.93246005512764, 4.563414339970514));
        locationMap.put(location.getLocationName(), location);

        location = new Location("Prinsenlaan", new Coordinates(51.94056629008424, 4.556572997642178));
        locationMap.put(location.getLocationName(), location);

        location = new Location("Oosterflank", new Coordinates(51.94540019398681, 4.554784712984907));
        locationMap.put(location.getLocationName(), location);

        location = new Location("Alexander", new Coordinates(51.9517268414101, 4.552088184149594));
        locationMap.put(location.getLocationName(), location);

        location = new Location("Graskruid", new Coordinates(51.957059566480275, 4.549524282300515));
        locationMap.put(location.getLocationName(), location);

        location = new Location("Hesseplaats", new Coordinates(51.96314588447406, 4.552473155314589));
        locationMap.put(location.getLocationName(), location);

        location = new Location("Nieuw Verlaat", new Coordinates(51.9651416683903, 4.562083068807786));
        locationMap.put(location.getLocationName(), location);

        location = new Location("Ambachtsland", new Coordinates(51.96608481765757, 4.570290382300965));
        locationMap.put(location.getLocationName(), location);

        location = new Location("De Tochten", new Coordinates(51.96880094872553, 4.57802606880798));
        locationMap.put(location.getLocationName(), location);

        location = new Location("Nesselande", new Coordinates(51.979869554249454, 4.586635726479802));
        locationMap.put(location.getLocationName(), location);

        location = new Location("Den Haag Centraal", new Coordinates(52.08071422822952, 4.325807664050018));
        locationMap.put(location.getLocationName(), location);

        location = new Location("Laan van NOI", new Coordinates(52.07930880502875, 4.34300868163147));
        locationMap.put(location.getLocationName(), location);

        location = new Location("Voorburg 't Loo", new Coordinates(52.08218401487736, 4.365602442871816));
        locationMap.put(location.getLocationName(), location);

        location = new Location("Leidschendam Voorburg", new Coordinates(52.077565863617416, 4.382104868813493));
        locationMap.put(location.getLocationName(), location);

        location = new Location("Forepark", new Coordinates(52.070282804168556, 4.392409211141902));
        locationMap.put(location.getLocationName(), location);

        location = new Location("Leidschenveen", new Coordinates(52.06494489903368, 4.400221839977222));
        locationMap.put(location.getLocationName(), location);

        location = new Location("Nootdorp", new Coordinates(52.04777134891777, 4.414501939976368));
        locationMap.put(location.getLocationName(), location);

        location = new Location("Pijnacker Centrum", new Coordinates(52.02070271854929, 4.436735212988738));
        locationMap.put(location.getLocationName(), location);

        location = new Location("Pijnacker Zuid", new Coordinates(52.00520695742426, 4.446140268809831));
        locationMap.put(location.getLocationName(), location);

        location = new Location("Berkel Westpolder", new Coordinates(51.990187215487566, 4.454247268809054));
        locationMap.put(location.getLocationName(), location);

        location = new Location("Rodenrijs", new Coordinates(51.97579337579903, 4.460675626479578));
        locationMap.put(location.getLocationName(), location);

        location = new Location("Meijersplein", new Coordinates(51.95568915291801, 4.462388597642963));
        locationMap.put(location.getLocationName(), location);

        location = new Location("Melanchthonweg", new Coordinates(51.948997349861784, 4.464543726478228));
        locationMap.put(location.getLocationName(), location);

        location = new Location("Blijdorp", new Coordinates(51.93060583024297, 4.458181413813709));
        locationMap.put(location.getLocationName(), location);

        location = new Location("Rotterdam Centraal", new Coordinates(51.925471117081976, 4.468738837366524));
        locationMap.put(location.getLocationName(), location);

        location = new Location("Stadhuis", new Coordinates(51.92345524574693, 4.478339526476917));
        locationMap.put(location.getLocationName(), location);

        location = new Location("Leuvehaven", new Coordinates(51.914137567719415, 4.481976397640847));
        locationMap.put(location.getLocationName(), location);

        location = new Location("Wilhelminaplein", new Coordinates(51.90743645697183, 4.4903917671111495));
        locationMap.put(location.getLocationName(), location);

        location = new Location("Rijnhaven", new Coordinates(51.90364559245745, 4.496829026475924));
        locationMap.put(location.getLocationName(), location);

        location = new Location("Maashaven", new Coordinates(51.89731884834961, 4.494742197639992));
        locationMap.put(location.getLocationName(), location);

        location = new Location("Zuidplein", new Coordinates(51.88712905132876, 4.488438555310722));
        locationMap.put(location.getLocationName(), location);

        location = new Location("Slinge", new Coordinates(51.8747806366901, 4.4777358129813445));
        locationMap.put(location.getLocationName(), location);

        //////////ROUTES

        //////////ROUTE Hoek van Holland-Nesselande (eerste half uur)
        for (int hour = 5; hour <= 22; hour++) {
            Route route = new Route(locationMap.get("Hoek van Holland Haven"), LocalTime.of(hour, 47));
            route.addStopOver(locationMap.get("Steendijkpolder"), LocalTime.of(hour, 54), LocalTime.of(hour, 54));
            route.addStopOver(locationMap.get("Maassluis West"), LocalTime.of(hour, 56), LocalTime.of(hour, 56));
            route.addStopOver(locationMap.get("Maassluis Centrum"), LocalTime.of(hour, 57), LocalTime.of(hour, 57));
            route.addStopOver(locationMap.get("Vlaardingen West"), LocalTime.of(hour + 1, 2), LocalTime.of(hour + 1, 2));
            route.addStopOver(locationMap.get("Vlaardingen Centrum"), LocalTime.of(hour + 1, 5), LocalTime.of(hour + 1, 5));
            route.addStopOver(locationMap.get("Vlaardingen Oost"), LocalTime.of(hour + 1, 7), LocalTime.of(hour + 1, 7));
            route.addStopOver(locationMap.get("Schiedam Nieuwland"), LocalTime.of(hour + 1, 9), LocalTime.of(hour + 1, 9));
            route.addStopOver(locationMap.get("Schiedam Centrum"), LocalTime.of(hour + 1, 12), LocalTime.of(hour + 1, 12));
            route.addStopOver(locationMap.get("Marconiplein"), LocalTime.of(hour + 1, 14), LocalTime.of(hour + 1, 14));
            route.addStopOver(locationMap.get("Delfshaven"), LocalTime.of(hour + 1, 16), LocalTime.of(hour + 1, 16));
            route.addStopOver(locationMap.get("Coolhaven"), LocalTime.of(hour + 1, 17), LocalTime.of(hour + 1, 17));
            route.addStopOver(locationMap.get("Dijkzigt"), LocalTime.of(hour + 1, 19), LocalTime.of(hour + 1, 19));
            route.addStopOver(locationMap.get("Eendrachtsplein"), LocalTime.of(hour + 1, 20), LocalTime.of(hour + 1, 20));
            route.addStopOver(locationMap.get("Beurs"), LocalTime.of(hour + 1, 21), LocalTime.of(hour + 1, 21));
            route.addStopOver(locationMap.get("Blaak"), LocalTime.of(hour + 1, 23), LocalTime.of(hour + 1, 23));
            route.addStopOver(locationMap.get("Oostplein"), LocalTime.of(hour + 1, 23), LocalTime.of(hour + 1, 23));
            route.addStopOver(locationMap.get("Gerdesiaweg"), LocalTime.of(hour + 1, 24), LocalTime.of(hour + 1, 24));
            route.addStopOver(locationMap.get("Voorschoterlaan"), LocalTime.of(hour + 1, 26), LocalTime.of(hour + 1, 26));
            route.addStopOver(locationMap.get("Kralingse Zoom"), LocalTime.of(hour + 1, 28), LocalTime.of(hour + 1, 28));
            route.addStopOver(locationMap.get("Capelsebrug"), LocalTime.of(hour + 1, 30), LocalTime.of(hour + 1, 30));
            route.addStopOver(locationMap.get("Schenkel"), LocalTime.of(hour + 1, 33), LocalTime.of(hour + 1, 33));
            route.addStopOver(locationMap.get("Prinsenlaan"), LocalTime.of(hour + 1, 35), LocalTime.of(hour + 1, 35));
            route.addStopOver(locationMap.get("Oosterflank"), LocalTime.of(hour + 1, 36), LocalTime.of(hour + 1, 36));
            route.addStopOver(locationMap.get("Alexander"), LocalTime.of(hour + 1, 37), LocalTime.of(hour + 1, 37));
            route.addEndPoint(locationMap.get("Nesselande"), LocalTime.of(hour + 1, 48));
            addRoute(route);
        }

        //////////ROUTE Hoek van Holland-Nesselande (tweede half uur)
        for (int hour = 6; hour <= 22; hour++) {
            Route route = new Route(locationMap.get("Hoek van Holland Haven"), LocalTime.of(hour, 17));
            route.addStopOver(locationMap.get("Steendijkpolder"), LocalTime.of(hour, 23), LocalTime.of(hour, 23));
            route.addStopOver(locationMap.get("Maassluis West"), LocalTime.of(hour, 25), LocalTime.of(hour, 25));
            route.addStopOver(locationMap.get("Maassluis Centrum"), LocalTime.of(hour, 27), LocalTime.of(hour, 27));
            route.addStopOver(locationMap.get("Vlaardingen West"), LocalTime.of(hour, 31), LocalTime.of(hour, 31));
            route.addStopOver(locationMap.get("Vlaardingen Centrum"), LocalTime.of(hour, 34), LocalTime.of(hour, 34));
            route.addStopOver(locationMap.get("Vlaardingen Oost"), LocalTime.of(hour, 36), LocalTime.of(hour, 36));
            route.addStopOver(locationMap.get("Schiedam Nieuwland"), LocalTime.of(hour, 39), LocalTime.of(hour, 39));
            route.addStopOver(locationMap.get("Schiedam Centrum"), LocalTime.of(hour, 42), LocalTime.of(hour, 42));
            route.addStopOver(locationMap.get("Marconiplein"), LocalTime.of(hour, 44), LocalTime.of(hour, 44));
            route.addStopOver(locationMap.get("Delfshaven"), LocalTime.of(hour, 46), LocalTime.of(hour, 46));
            route.addStopOver(locationMap.get("Coolhaven"), LocalTime.of(hour, 47), LocalTime.of(hour, 47));
            route.addStopOver(locationMap.get("Dijkzigt"), LocalTime.of(hour, 49), LocalTime.of(hour, 49));
            route.addStopOver(locationMap.get("Eendrachtsplein"), LocalTime.of(hour, 50), LocalTime.of(hour, 50));
            route.addStopOver(locationMap.get("Beurs"), LocalTime.of(hour, 51), LocalTime.of(hour, 51));
            route.addStopOver(locationMap.get("Blaak"), LocalTime.of(hour, 53), LocalTime.of(hour, 53));
            route.addStopOver(locationMap.get("Oostplein"), LocalTime.of(hour, 54), LocalTime.of(hour, 54));
            route.addStopOver(locationMap.get("Gerdesiaweg"), LocalTime.of(hour, 55), LocalTime.of(hour, 55));
            route.addStopOver(locationMap.get("Voorschoterlaan"), LocalTime.of(hour, 57), LocalTime.of(hour, 57));
            route.addStopOver(locationMap.get("Kralingse Zoom"), LocalTime.of(hour, 58), LocalTime.of(hour, 58));
            route.addStopOver(locationMap.get("Capelsebrug"), LocalTime.of(hour + 1, 0), LocalTime.of(hour + 1, 0));
            route.addStopOver(locationMap.get("Schenkel"), LocalTime.of(hour + 1, 4), LocalTime.of(hour + 1, 4));
            route.addStopOver(locationMap.get("Prinsenlaan"), LocalTime.of(hour + 1, 6), LocalTime.of(hour + 1, 6));
            route.addStopOver(locationMap.get("Oosterflank"), LocalTime.of(hour + 1, 7), LocalTime.of(hour + 1, 7));
            route.addStopOver(locationMap.get("Alexander"), LocalTime.of(hour + 1, 8), LocalTime.of(hour + 1, 8));
            route.addEndPoint(locationMap.get("Nesselande"), LocalTime.of(hour + 1, 18));
            addRoute(route);
        }

        //////////ROUTE Nesselande-Hoek van Holland (eerste half uur)
        for (int hour = 5; hour <= 22; hour++) {
            Route route = new Route(locationMap.get("Nesselande"), LocalTime.of(hour, 42));
            route.addStopOver(locationMap.get("De Tochten"), LocalTime.of(hour, 45), LocalTime.of(hour, 45));
            route.addStopOver(locationMap.get("Ambachtsland"), LocalTime.of(hour, 46), LocalTime.of(hour, 46));
            route.addStopOver(locationMap.get("Nieuw Verlaat"), LocalTime.of(hour, 48), LocalTime.of(hour, 48));
            route.addStopOver(locationMap.get("Hesseplaats"), LocalTime.of(hour, 49), LocalTime.of(hour, 49));
            route.addStopOver(locationMap.get("Graskruid"), LocalTime.of(hour, 51), LocalTime.of(hour, 51));
            route.addStopOver(locationMap.get("Alexander"), LocalTime.of(hour, 52), LocalTime.of(hour, 52));
            route.addStopOver(locationMap.get("Oosterflank"), LocalTime.of(hour, 54), LocalTime.of(hour, 54));
            route.addStopOver(locationMap.get("Prinsenlaan"), LocalTime.of(hour, 55), LocalTime.of(hour, 55));
            route.addStopOver(locationMap.get("Schenkel"), LocalTime.of(hour, 57), LocalTime.of(hour, 57));
            route.addStopOver(locationMap.get("Capelsebrug"), LocalTime.of(hour + 1, 0), LocalTime.of(hour + 1, 0));
            route.addStopOver(locationMap.get("Kralingse Zoom"), LocalTime.of(hour + 1, 2), LocalTime.of(hour + 1, 2));
            route.addStopOver(locationMap.get("Voorschoterlaan"), LocalTime.of(hour + 1, 5), LocalTime.of(hour + 1, 5));
            route.addStopOver(locationMap.get("Gerdesiaweg"), LocalTime.of(hour + 1, 6), LocalTime.of(hour + 1, 6));
            route.addStopOver(locationMap.get("Oostplein"), LocalTime.of(hour + 1, 7), LocalTime.of(hour + 1, 7));
            route.addStopOver(locationMap.get("Blaak"), LocalTime.of(hour + 1, 8), LocalTime.of(hour + 1, 8));
            route.addStopOver(locationMap.get("Beurs"), LocalTime.of(hour + 1, 10), LocalTime.of(hour + 1, 10));
            route.addStopOver(locationMap.get("Eendrachtsplein"), LocalTime.of(hour + 1, 11), LocalTime.of(hour + 1, 11));
            route.addStopOver(locationMap.get("Dijkzigt"), LocalTime.of(hour + 1, 13), LocalTime.of(hour + 1, 13));
            route.addStopOver(locationMap.get("Coolhaven"), LocalTime.of(hour + 1, 14), LocalTime.of(hour + 1, 14));
            route.addStopOver(locationMap.get("Delfshaven"), LocalTime.of(hour + 1, 17), LocalTime.of(hour + 1, 17));
            route.addStopOver(locationMap.get("Marconiplein"), LocalTime.of(hour + 1, 18), LocalTime.of(hour + 1, 18));
            route.addStopOver(locationMap.get("Schiedam Centrum"), LocalTime.of(hour + 1, 21), LocalTime.of(hour + 1, 21));
            route.addStopOver(locationMap.get("Schiedam Nieuwland"), LocalTime.of(hour + 1, 24), LocalTime.of(hour + 1, 24));
            route.addStopOver(locationMap.get("Vlaardingen Oost"), LocalTime.of(hour + 1, 26), LocalTime.of(hour + 1, 26));
            route.addEndPoint(locationMap.get("Hoek van Holland Haven"), LocalTime.of(hour + 1, 43));
            addRoute(route);
        }

        //////////ROUTE Nesselande-Hoek van Holland (tweede half uur)
        for (int hour = 6; hour <= 22; hour++) {
            Route route = new Route(locationMap.get("Nesselande"), LocalTime.of(hour, 12));
            route.addStopOver(locationMap.get("De Tochten"), LocalTime.of(hour, 15), LocalTime.of(hour, 15));
            route.addStopOver(locationMap.get("Ambachtsland"), LocalTime.of(hour, 17), LocalTime.of(hour, 17));
            route.addStopOver(locationMap.get("Nieuw Verlaat"), LocalTime.of(hour, 18), LocalTime.of(hour, 18));
            route.addStopOver(locationMap.get("Hesseplaats"), LocalTime.of(hour, 20), LocalTime.of(hour, 20));
            route.addStopOver(locationMap.get("Graskruid"), LocalTime.of(hour, 22), LocalTime.of(hour, 22));
            route.addStopOver(locationMap.get("Alexander"), LocalTime.of(hour, 23), LocalTime.of(hour, 23));
            route.addStopOver(locationMap.get("Oosterflank"), LocalTime.of(hour, 25), LocalTime.of(hour, 25));
            route.addStopOver(locationMap.get("Prinsenlaan"), LocalTime.of(hour, 26), LocalTime.of(hour, 26));
            route.addStopOver(locationMap.get("Schenkel"), LocalTime.of(hour, 28), LocalTime.of(hour, 28));
            route.addStopOver(locationMap.get("Capelsebrug"), LocalTime.of(hour, 30), LocalTime.of(hour, 30));
            route.addStopOver(locationMap.get("Kralingse Zoom"), LocalTime.of(hour, 32), LocalTime.of(hour, 32));
            route.addStopOver(locationMap.get("Voorschoterlaan"), LocalTime.of(hour, 34), LocalTime.of(hour, 34));
            route.addStopOver(locationMap.get("Gerdesiaweg"), LocalTime.of(hour, 35), LocalTime.of(hour, 35));
            route.addStopOver(locationMap.get("Oostplein"), LocalTime.of(hour, 37), LocalTime.of(hour, 37));
            route.addStopOver(locationMap.get("Blaak"), LocalTime.of(hour, 38), LocalTime.of(hour, 38));
            route.addStopOver(locationMap.get("Beurs"), LocalTime.of(hour, 40), LocalTime.of(hour, 40));
            route.addStopOver(locationMap.get("Eendrachtsplein"), LocalTime.of(hour, 41), LocalTime.of(hour, 41));
            route.addStopOver(locationMap.get("Dijkzigt"), LocalTime.of(hour, 42), LocalTime.of(hour, 42));
            route.addStopOver(locationMap.get("Coolhaven"), LocalTime.of(hour, 44), LocalTime.of(hour, 44));
            route.addStopOver(locationMap.get("Delfshaven"), LocalTime.of(hour, 45), LocalTime.of(hour, 45));
            route.addStopOver(locationMap.get("Marconiplein"), LocalTime.of(hour, 47), LocalTime.of(hour, 47));
            route.addStopOver(locationMap.get("Schiedam Centrum"), LocalTime.of(hour, 52), LocalTime.of(hour, 52));
            route.addStopOver(locationMap.get("Schiedam Nieuwland"), LocalTime.of(hour, 55), LocalTime.of(hour, 55));
            route.addStopOver(locationMap.get("Vlaardingen Oost"), LocalTime.of(hour, 57), LocalTime.of(hour, 57));
            route.addEndPoint(locationMap.get("Hoek van Holland Haven"), LocalTime.of(hour + 1, 14));
            addRoute(route);
        }

        //////////ROUTE Den Haag Centraal-SLinge (eerste half uur)
        for (int hour = 5; hour <= 21; hour++) {
            Route route = new Route(locationMap.get("Den Haag Centraal"), LocalTime.of(hour, 57));
            route.addStopOver(locationMap.get("Laan van NOI"), LocalTime.of(hour, 59), LocalTime.of(5, 59));
            route.addStopOver(locationMap.get("Voorburg 't Loo"), LocalTime.of(hour + 1, 2), LocalTime.of(hour + 1, 2));
            route.addStopOver(locationMap.get("Leidschendam Voorburg"), LocalTime.of(hour + 1, 4), LocalTime.of(hour + 1, 4));
            route.addStopOver(locationMap.get("Forepark"), LocalTime.of(hour + 1, 5), LocalTime.of(hour + 1, 5));
            route.addStopOver(locationMap.get("Leidschenveen"), LocalTime.of(hour + 1, 7), LocalTime.of(hour + 1, 7));
            route.addStopOver(locationMap.get("Nootdorp"), LocalTime.of(hour + 1, 10), LocalTime.of(hour + 1, 10));
            route.addStopOver(locationMap.get("Pijnacker Centrum"), LocalTime.of(hour + 1, 13), LocalTime.of(hour + 1, 13));
            route.addStopOver(locationMap.get("Pijnacker Zuid"), LocalTime.of(hour + 1, 16), LocalTime.of(hour + 1, 16));
            route.addStopOver(locationMap.get("Berkel Westpolder"), LocalTime.of(hour + 1, 19), LocalTime.of(hour + 1, 19));
            route.addStopOver(locationMap.get("Rodenrijs"), LocalTime.of(hour + 1, 21), LocalTime.of(hour + 1, 21));
            route.addStopOver(locationMap.get("Meijersplein"), LocalTime.of(hour + 1, 23), LocalTime.of(hour + 1, 23));
            route.addStopOver(locationMap.get("Melanchthonweg"), LocalTime.of(hour + 1, 25), LocalTime.of(hour + 1, 25));
            route.addStopOver(locationMap.get("Blijdorp"), LocalTime.of(hour + 1,28), LocalTime.of(hour +1, 28));
            route.addStopOver(locationMap.get("Rotterdam Centraal"), LocalTime.of(hour + 1,30), LocalTime.of(hour +1, 30));
            route.addStopOver(locationMap.get("Stadhuis"), LocalTime.of(hour + 1,32), LocalTime.of(hour +1, 32));
            route.addStopOver(locationMap.get("Beurs"), LocalTime.of(hour + 1,33), LocalTime.of(hour +1, 33));
            route.addStopOver(locationMap.get("Leuvehaven"), LocalTime.of(hour + 1,34), LocalTime.of(hour +1, 34));
            route.addStopOver(locationMap.get("Wilhelminaplein"), LocalTime.of(hour + 1,36), LocalTime.of(hour +1, 36));
            route.addStopOver(locationMap.get("Rijnhaven"), LocalTime.of(hour + 1,38), LocalTime.of(hour +1, 38));
            route.addStopOver(locationMap.get("Maashaven"), LocalTime.of(hour + 1,39), LocalTime.of(hour +1, 39));
            route.addStopOver(locationMap.get("Zuidplein"), LocalTime.of(hour + 1,42), LocalTime.of(hour +1, 42));
            route.addEndPoint(locationMap.get("Slinge"), LocalTime.of(hour + 1, 44));
            addRoute(route);
        }

//////////ROUTE Den Haag Centraal-SLinge (tweede half uur)
        for (int hour = 6; hour <= 21; hour++) {
            Route route = new Route(locationMap.get("Den Haag Centraal"), LocalTime.of(hour, 28));
            route.addStopOver(locationMap.get("Laan van NOI"), LocalTime.of(hour, 30), LocalTime.of(5, 30));
            route.addStopOver(locationMap.get("Voorburg 't Loo"), LocalTime.of(hour, 33), LocalTime.of(hour, 33));
            route.addStopOver(locationMap.get("Leidschendam Voorburg"), LocalTime.of(hour, 35), LocalTime.of(hour, 35));
            route.addStopOver(locationMap.get("Forepark"), LocalTime.of(hour, 36), LocalTime.of(hour, 36));
            route.addStopOver(locationMap.get("Leidschenveen"), LocalTime.of(hour, 38), LocalTime.of(hour, 38));
            route.addStopOver(locationMap.get("Nootdorp"), LocalTime.of(hour, 41), LocalTime.of(hour, 41));
            route.addStopOver(locationMap.get("Pijnacker Centrum"), LocalTime.of(hour, 44), LocalTime.of(hour, 44));
            route.addStopOver(locationMap.get("Pijnacker Zuid"), LocalTime.of(hour, 47), LocalTime.of(hour, 47));
            route.addStopOver(locationMap.get("Berkel Westpolder"), LocalTime.of(hour, 50), LocalTime.of(hour, 50));
            route.addStopOver(locationMap.get("Rodenrijs"), LocalTime.of(hour, 52), LocalTime.of(hour, 52));
            route.addStopOver(locationMap.get("Meijersplein"), LocalTime.of(hour, 54), LocalTime.of(hour, 54));
            route.addStopOver(locationMap.get("Melanchthonweg"), LocalTime.of(hour, 56), LocalTime.of(hour, 56));
            route.addStopOver(locationMap.get("Blijdorp"), LocalTime.of(hour,59), LocalTime.of(hour, 59));
            route.addStopOver(locationMap.get("Rotterdam Centraal"), LocalTime.of(hour + 1,1), LocalTime.of(hour +1, 1));
            route.addStopOver(locationMap.get("Stadhuis"), LocalTime.of(hour + 1,3), LocalTime.of(hour +1, 3));
            route.addStopOver(locationMap.get("Beurs"), LocalTime.of(hour + 1,4), LocalTime.of(hour +1, 4));
            route.addStopOver(locationMap.get("Leuvehaven"), LocalTime.of(hour + 1,5), LocalTime.of(hour +1, 5));
            route.addStopOver(locationMap.get("Wilhelminaplein"), LocalTime.of(hour + 1,7), LocalTime.of(hour +1, 7));
            route.addStopOver(locationMap.get("Rijnhaven"), LocalTime.of(hour + 1,9), LocalTime.of(hour +1, 9));
            route.addStopOver(locationMap.get("Maashaven"), LocalTime.of(hour + 1,10), LocalTime.of(hour +1, 10));
            route.addStopOver(locationMap.get("Zuidplein"), LocalTime.of(hour + 1,13), LocalTime.of(hour +1, 13));
            route.addEndPoint(locationMap.get("Slinge"), LocalTime.of(hour + 1, 15));
            addRoute(route);
        }

        //////////ROUTE Slinge-Den Haag Centraal (eerste half uur)
        for (int hour = 5; hour <= 21; hour++) {
            Route route = new Route(locationMap.get("Slinge"), LocalTime.of(hour, 33));
            route.addStopOver(locationMap.get("Zuidplein"), LocalTime.of(hour, 35), LocalTime.of(hour, 35));
            route.addStopOver(locationMap.get("Maashaven"), LocalTime.of(hour, 37), LocalTime.of(hour, 37));
            route.addStopOver(locationMap.get("Rijnhaven"), LocalTime.of(hour, 39), LocalTime.of(hour, 39));
            route.addStopOver(locationMap.get("Wilhelminaplein"), LocalTime.of(hour, 40), LocalTime.of(hour, 40));
            route.addStopOver(locationMap.get("Leuvehaven"), LocalTime.of(hour, 42), LocalTime.of(hour, 42));
            route.addStopOver(locationMap.get("Beurs"), LocalTime.of(hour, 43), LocalTime.of(hour, 43));
            route.addStopOver(locationMap.get("Stadhuis"), LocalTime.of(hour, 45), LocalTime.of(hour, 45));
            route.addStopOver(locationMap.get("Rotterdam Centraal"), LocalTime.of(hour, 47), LocalTime.of(hour, 47));
            route.addStopOver(locationMap.get("Blijdorp"), LocalTime.of(hour, 49), LocalTime.of(hour, 49));
            route.addStopOver(locationMap.get("Melanchthonweg"), LocalTime.of(hour, 52), LocalTime.of(hour, 52));
            route.addStopOver(locationMap.get("Meijersplein"), LocalTime.of(hour, 53), LocalTime.of(hour, 53));
            route.addStopOver(locationMap.get("Rodenrijs"), LocalTime.of(hour, 55), LocalTime.of(hour, 55));
            route.addStopOver(locationMap.get("Berkel Westpolder"), LocalTime.of(hour, 57), LocalTime.of(hour, 57));
            route.addStopOver(locationMap.get("Pijnacker Zuid"), LocalTime.of(hour +1, 0), LocalTime.of(hour +1, 0));
            route.addStopOver(locationMap.get("Pijnacker Centrum"), LocalTime.of(hour +1, 2), LocalTime.of(hour +1, 2));
            route.addStopOver(locationMap.get("Nootdorp"), LocalTime.of(hour +1, 5), LocalTime.of(hour +1, 5));
            route.addStopOver(locationMap.get("Leidschenveen"), LocalTime.of(hour +1, 8), LocalTime.of(hour +1, 8));
            route.addStopOver(locationMap.get("Forepark"), LocalTime.of(hour +1, 10), LocalTime.of(hour +1, 10));
            route.addStopOver(locationMap.get("Leidschendam Voorburg"), LocalTime.of(hour +1, 11), LocalTime.of(hour +1, 11));
            route.addStopOver(locationMap.get("Voorburg 't Loo"), LocalTime.of(hour +1, 13), LocalTime.of(hour +1, 13));
            route.addStopOver(locationMap.get("Laan van NOI"), LocalTime.of(hour +1, 16), LocalTime.of(hour +1, 16));
            route.addEndPoint(locationMap.get("Den Haag Centraal"), LocalTime.of(hour +1, 18));
            addRoute(route);
        }

        //////////ROUTE Slinge-Den Haag Centraal (tweede half uur)
        for (int hour = 6; hour <= 21; hour++) {
            Route route = new Route(locationMap.get("Slinge"), LocalTime.of(hour, 3));
            route.addStopOver(locationMap.get("Zuidplein"), LocalTime.of(hour, 5), LocalTime.of(hour, 5));
            route.addStopOver(locationMap.get("Maashaven"), LocalTime.of(hour, 7), LocalTime.of(hour, 7));
            route.addStopOver(locationMap.get("Rijnhaven"), LocalTime.of(hour, 9), LocalTime.of(hour, 9));
            route.addStopOver(locationMap.get("Wilhelminaplein"), LocalTime.of(hour, 10), LocalTime.of(hour, 10));
            route.addStopOver(locationMap.get("Leuvehaven"), LocalTime.of(hour, 12), LocalTime.of(hour, 12));
            route.addStopOver(locationMap.get("Beurs"), LocalTime.of(hour, 13), LocalTime.of(hour, 13));
            route.addStopOver(locationMap.get("Stadhuis"), LocalTime.of(hour, 15), LocalTime.of(hour, 15));
            route.addStopOver(locationMap.get("Rotterdam Centraal"), LocalTime.of(hour, 17), LocalTime.of(hour, 17));
            route.addStopOver(locationMap.get("Blijdorp"), LocalTime.of(hour, 19), LocalTime.of(hour, 19));
            route.addStopOver(locationMap.get("Melanchthonweg"), LocalTime.of(hour, 22), LocalTime.of(hour, 22));
            route.addStopOver(locationMap.get("Meijersplein"), LocalTime.of(hour, 23), LocalTime.of(hour, 23));
            route.addStopOver(locationMap.get("Rodenrijs"), LocalTime.of(hour, 25), LocalTime.of(hour, 25));
            route.addStopOver(locationMap.get("Berkel Westpolder"), LocalTime.of(hour, 27), LocalTime.of(hour, 27));
            route.addStopOver(locationMap.get("Pijnacker Zuid"), LocalTime.of(hour, 30), LocalTime.of(hour, 30));
            route.addStopOver(locationMap.get("Pijnacker Centrum"), LocalTime.of(hour, 32), LocalTime.of(hour, 32));
            route.addStopOver(locationMap.get("Nootdorp"), LocalTime.of(hour, 35), LocalTime.of(hour, 35));
            route.addStopOver(locationMap.get("Leidschenveen"), LocalTime.of(hour, 38), LocalTime.of(hour, 38));
            route.addStopOver(locationMap.get("Forepark"), LocalTime.of(hour, 40), LocalTime.of(hour, 40));
            route.addStopOver(locationMap.get("Leidschendam Voorburg"), LocalTime.of(hour, 41), LocalTime.of(hour, 41));
            route.addStopOver(locationMap.get("Voorburg 't Loo"), LocalTime.of(hour, 43), LocalTime.of(hour, 43));
            route.addStopOver(locationMap.get("Laan van NOI"), LocalTime.of(hour, 46), LocalTime.of(hour, 46));
            route.addEndPoint(locationMap.get("Den Haag Centraal"), LocalTime.of(hour, 48));
            addRoute(route);
        }
    }

    //////////Methods specific to data


}


