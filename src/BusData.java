import java.time.LocalTime;

public class BusData extends Data {
    public BusData() {

        //////////ROUTES
        Location location = new Location("Amersfoort Centraal Station", new Coordinates(52.15309574492724, 5.3736233227641055));
        locationMap.put(location.getLocationName(), location);

        location = new Location("Amersfoort Eemplein", new Coordinates(52.15919740454207, 5.379072635555095));
        locationMap.put(location.getLocationName(), location);

        location = new Location("Amersfoort Brabantsestraat", new Coordinates(52.16185251560454, 5.3785959980114235));
        locationMap.put(location.getLocationName(), location);

        location = new Location("Amersfoort Kwekersweg", new Coordinates(52.16444622263774, 5.37908385269625));
        locationMap.put(location.getLocationName(), location);

        location = new Location("Amersfoort Industrieweg", new Coordinates(52.16589784497106, 5.3707662907185805));
        locationMap.put(location.getLocationName(), location);

        location = new Location("Amersfoort Mijnbouwweg", new Coordinates(52.167151012057076, 5.3666047268471715));
        locationMap.put(location.getLocationName(), location);

        location = new Location("Amersfoort Argonweg", new Coordinates(52.169364166499484, 5.357248642189313));
        locationMap.put(location.getLocationName(), location);

        location = new Location("Amersfoort Heliumweg", new Coordinates(52.16980936250872, 5.353521811505164));
        locationMap.put(location.getLocationName(), location);

        location = new Location("Amersfoort Kryptonweg", new Coordinates(52.16650701357081, 5.351407663281104));
        locationMap.put(location.getLocationName(), location);

        location = new Location("Amersfoort Birkhoven Park", new Coordinates(52.15962203321079, 5.346426726846872));
        locationMap.put(location.getLocationName(), location);

        location = new Location("Amersfoort Stadhuis", new Coordinates(52.156127131606105, 5.383542455682267));
        locationMap.put(location.getLocationName(), location);

        location = new Location("Amersfoort Meander MC", new Coordinates(52.17158858934702, 5.371847435635819));
        locationMap.put(location.getLocationName(), location);

        location = new Location("Hoogland Zevenhuizerstraat", new Coordinates(52.1822450775319, 5.3698254556832214));
        locationMap.put(location.getLocationName(), location);

        location = new Location("Hoogland Pastoor Pieckweg", new Coordinates(52.19238651806277, 5.36820708267043));
        locationMap.put(location.getLocationName(), location);

        location = new Location("Amersfoort Hooglandsepoort", new Coordinates(52.19486647484194, 5.370874540341579));
        locationMap.put(location.getLocationName(), location);

        location = new Location("Amersfoort Het Rode Hert", new Coordinates(52.19561935560603, 5.377967540341584));
        locationMap.put(location.getLocationName(), location);

        location = new Location("Amersfoort Centrum", new Coordinates(52.15398088385197, 5.384040540340075));
        locationMap.put(location.getLocationName(), location);

        location = new Location("Amersfoort De Kei", new Coordinates(52.15273713209584, 5.386992540340031));
        locationMap.put(location.getLocationName(), location);

        location = new Location("Amersfoort Hendrik van Viandenstraat", new Coordinates(52.15322938204048, 5.391603082669018));
        locationMap.put(location.getLocationName(), location);

        location = new Location("Amersfoort Blekerseiland", new Coordinates(52.15532755535786, 5.395980417203449));
        locationMap.put(location.getLocationName(), location);

        location = new Location("Amersfoort Hogeweg", new Coordinates(52.15729271130361, 5.400728540340188));
        locationMap.put(location.getLocationName(), location);

        location = new Location("Amersfoort Amerena", new Coordinates(52.15884943538152, 5.4115220980113214));
        locationMap.put(location.getLocationName(), location);

        location = new Location("Amersfoort Zonnehof-West", new Coordinates(52.15270421823401, 5.382796371024215));
        locationMap.put(location.getLocationName(), location);

        location = new Location("Amersfoort Van Stolbergpark", new Coordinates(52.14741042680662, 5.383070593920868));
        locationMap.put(location.getLocationName(), location);

        location = new Location("Amersfoort Daltonstraat", new Coordinates(52.144499571672455, 5.382522624997638));
        locationMap.put(location.getLocationName(), location);

        location = new Location("Amersfoort Borneoplein", new Coordinates(52.14014133398703, 5.37717872684615));
        locationMap.put(location.getLocationName(), location);

        location = new Location("Amersfoort Einsteinstraat", new Coordinates(52.13931050549537, 5.3805451845171754));
        locationMap.put(location.getLocationName(), location);

        location = new Location("Amersfoort Amerhorst", new Coordinates(52.14046817925747, 5.387003269175108));
        locationMap.put(location.getLocationName(), location);

        location = new Location("Amersfoort Bergkerk", new Coordinates(52.14876448033774, 5.36890237102412));
        locationMap.put(location.getLocationName(), location);

        location = new Location("Amersfoort Vondellaan", new Coordinates(52.144870738852354, 5.367869557530538));
        locationMap.put(location.getLocationName(), location);

        location = new Location("Amersfoort Hugo de Grootlaan", new Coordinates(52.14224063022385, 5.370098160072665));
        locationMap.put(location.getLocationName(), location);

        location = new Location("Amersfoort Curacaolaan", new Coordinates(52.13920809055603, 5.372545540339526));
        locationMap.put(location.getLocationName(), location);

        location = new Location("Amersfoort Timorstraat", new Coordinates(52.1374699253486, 5.3764025403394795));
        locationMap.put(location.getLocationName(), location);

        location = new Location("Leusden ISVW", new Coordinates(52.131377698495164, 5.384841269174804));
        locationMap.put(location.getLocationName(), location);

        //////////ROUTES

        //////////ROUTE AMERSFOORT CENTRAAL STATION - AMERSFOORT BIRKHOVEN PARK (HELE UREN)
        for (int hour = 6; hour <= 22; hour++) {
            Route route = new Route(locationMap.get("Amersfoort Centraal Station"), LocalTime.of(hour, 46));
            route.addStopOver(locationMap.get("Amersfoort Eemplein"), LocalTime.of(hour , 50), LocalTime.of(hour, 50));
            route.addStopOver(locationMap.get("Amersfoort Brabantsestraat"), LocalTime.of(hour, 50), LocalTime.of(hour, 50));
            route.addStopOver(locationMap.get("Amersfoort Kwekersweg"), LocalTime.of(hour , 51), LocalTime.of(hour, 51));
            route.addStopOver(locationMap.get("Amersfoort Industrieweg"), LocalTime.of(hour , 52), LocalTime.of(hour, 52));
            route.addStopOver(locationMap.get("Amersfoort Mijnbouwweg"), LocalTime.of(hour , 55), LocalTime.of(hour, 55));
            route.addStopOver(locationMap.get("Amersfoort Argonweg"), LocalTime.of(hour , 57), LocalTime.of(hour, 57));
            route.addStopOver(locationMap.get("Amersfoort Heliumweg"), LocalTime.of(hour , 58), LocalTime.of(hour, 58));
            route.addStopOver(locationMap.get("Amersfoort Kryptonweg"), LocalTime.of(hour , 59), LocalTime.of(hour, 59));
            route.addEndPoint(locationMap.get("Amersfoort Birkhoven Park"), LocalTime.of(hour +1, 4));
            addRoute(route);
        }

        //////////ROUTE AMERSFOORT CENTRAAL STATION - AMERSFOORT BIRKHOVEN PARK (HALVE UREN)
        for (int hour = 7; hour <= 22; hour++) {
            Route route = new Route(locationMap.get("Amersfoort Centraal Station"), LocalTime.of(hour, 16));
            route.addStopOver(locationMap.get("Amersfoort Eemplein"), LocalTime.of(hour , 20), LocalTime.of(hour, 20));
            route.addStopOver(locationMap.get("Amersfoort Brabantsestraat"), LocalTime.of(hour, 20), LocalTime.of(hour, 20));
            route.addStopOver(locationMap.get("Amersfoort Kwekersweg"), LocalTime.of(hour , 21), LocalTime.of(hour, 21));
            route.addStopOver(locationMap.get("Amersfoort Industrieweg"), LocalTime.of(hour , 22), LocalTime.of(hour, 22));
            route.addStopOver(locationMap.get("Amersfoort Mijnbouwweg"), LocalTime.of(hour , 25), LocalTime.of(hour, 25));
            route.addStopOver(locationMap.get("Amersfoort Argonweg"), LocalTime.of(hour , 27), LocalTime.of(hour, 27));
            route.addStopOver(locationMap.get("Amersfoort Heliumweg"), LocalTime.of(hour , 28), LocalTime.of(hour, 28));
            route.addStopOver(locationMap.get("Amersfoort Kryptonweg"), LocalTime.of(hour , 29), LocalTime.of(hour, 29));
            route.addEndPoint(locationMap.get("Amersfoort Birkhoven Park"), LocalTime.of(hour , 34));
            addRoute(route);
        }

        //////////ROUTE AMERSFOORT BIRKHOVEN PARK - AMERSFOORT CENTRAAL STATION (HELE UREN)
        for (int hour = 7; hour <= 22; hour++) {
            Route route = new Route(locationMap.get("Amersfoort Birkhoven Park"), LocalTime.of(hour, 14));
            route.addStopOver(locationMap.get("Amersfoort Kryptonweg"), LocalTime.of(hour , 19), LocalTime.of(hour, 19));
            route.addStopOver(locationMap.get("Amersfoort Heliumweg"), LocalTime.of(hour, 20), LocalTime.of(hour, 20));
            route.addStopOver(locationMap.get("Amersfoort Argonweg"), LocalTime.of(hour , 21), LocalTime.of(hour, 21));
            route.addStopOver(locationMap.get("Amersfoort Mijnbouwweg"), LocalTime.of(hour , 23), LocalTime.of(hour, 23));
            route.addStopOver(locationMap.get("Amersfoort Industrieweg"), LocalTime.of(hour , 26), LocalTime.of(hour, 26));
            route.addStopOver(locationMap.get("Amersfoort Kwekersweg"), LocalTime.of(hour , 27), LocalTime.of(hour, 27));
            route.addStopOver(locationMap.get("Amersfoort Brabantsestraat"), LocalTime.of(hour , 28), LocalTime.of(hour, 28));
            route.addStopOver(locationMap.get("Amersfoort Eemplein"), LocalTime.of(hour , 28), LocalTime.of(hour, 28));
            route.addEndPoint(locationMap.get("Amersfoort Centraal Station"), LocalTime.of(hour, 32));
            addRoute(route);
        }

        //////////ROUTE AMERSFOORT BIRKHOVEN PARK - AMERSFOORT CENTRAAL STATION (HALVE UREN)
        for (int hour = 7; hour <= 22; hour++) {
            Route route = new Route(locationMap.get("Amersfoort Birkhoven Park"), LocalTime.of(hour, 44));
            route.addStopOver(locationMap.get("Amersfoort Kryptonweg"), LocalTime.of(hour , 49), LocalTime.of(hour, 49));
            route.addStopOver(locationMap.get("Amersfoort Heliumweg"), LocalTime.of(hour, 50), LocalTime.of(hour, 50));
            route.addStopOver(locationMap.get("Amersfoort Argonweg"), LocalTime.of(hour , 51), LocalTime.of(hour, 51));
            route.addStopOver(locationMap.get("Amersfoort Mijnbouwweg"), LocalTime.of(hour , 53), LocalTime.of(hour, 53));
            route.addStopOver(locationMap.get("Amersfoort Industrieweg"), LocalTime.of(hour , 56), LocalTime.of(hour, 56));
            route.addStopOver(locationMap.get("Amersfoort Kwekersweg"), LocalTime.of(hour , 57), LocalTime.of(hour, 57));
            route.addStopOver(locationMap.get("Amersfoort Brabantsestraat"), LocalTime.of(hour , 58), LocalTime.of(hour, 58));
            route.addStopOver(locationMap.get("Amersfoort Eemplein"), LocalTime.of(hour , 58), LocalTime.of(hour, 58));
            route.addEndPoint(locationMap.get("Amersfoort Centraal Station"), LocalTime.of(hour +1, 2));
            addRoute(route);
        }

        //////////ROUTE AMERSFOORT CENTRAAL STATION - AMERSFOORT HET RODE HERT (HELE UREN)
        for (int hour = 5; hour <= 21; hour++) {
            Route route = new Route(locationMap.get("Amersfoort Centraal Station"), LocalTime.of(hour, 27));
            route.addStopOver(locationMap.get("Amersfoort Stadhuis"), LocalTime.of(hour , 29), LocalTime.of(hour, 29));
            route.addStopOver(locationMap.get("Amersfoort Eemplein"), LocalTime.of(hour, 30), LocalTime.of(hour, 30));
            route.addStopOver(locationMap.get("Amersfoort Brabantsestraat"), LocalTime.of(hour , 31), LocalTime.of(hour, 31));
            route.addStopOver(locationMap.get("Amersfoort Kwekersweg"), LocalTime.of(hour , 32), LocalTime.of(hour, 32));
            route.addStopOver(locationMap.get("Amersfoort Meander MC"), LocalTime.of(hour , 33), LocalTime.of(hour, 33));
            route.addStopOver(locationMap.get("Hoogland Zevenhuizerstraat"), LocalTime.of(hour , 36), LocalTime.of(hour, 36));
            route.addStopOver(locationMap.get("Hoogland Pastoor Pieckweg"), LocalTime.of(hour , 38), LocalTime.of(hour, 38));
            route.addStopOver(locationMap.get("Amersfoort Hooglandsepoort"), LocalTime.of(hour , 38), LocalTime.of(hour, 38));
            route.addEndPoint(locationMap.get("Amersfoort Het Rode Hert"), LocalTime.of(hour , 40));
            addRoute(route);
        }

        //////////ROUTE AMERSFOORT CENTRAAL STATION - AMERSFOORT HET RODE HERT (HALVE UREN)
        for (int hour = 5; hour <= 21; hour++) {
            Route route = new Route(locationMap.get("Amersfoort Centraal Station"), LocalTime.of(hour, 57));
            route.addStopOver(locationMap.get("Amersfoort Stadhuis"), LocalTime.of(hour , 59), LocalTime.of(hour, 59));
            route.addStopOver(locationMap.get("Amersfoort Eemplein"), LocalTime.of(hour +1, 0), LocalTime.of(hour +1, 0));
            route.addStopOver(locationMap.get("Amersfoort Brabantsestraat"), LocalTime.of(hour +1, 1), LocalTime.of(hour +1, 1));
            route.addStopOver(locationMap.get("Amersfoort Kwekersweg"), LocalTime.of(hour +1 , 2), LocalTime.of(hour +1, 2));
            route.addStopOver(locationMap.get("Amersfoort Meander MC"), LocalTime.of(hour +1, 3), LocalTime.of(hour +1, 3));
            route.addStopOver(locationMap.get("Hoogland Zevenhuizerstraat"), LocalTime.of(hour +1, 6), LocalTime.of(hour +1, 6));
            route.addStopOver(locationMap.get("Hoogland Pastoor Pieckweg"), LocalTime.of(hour +1, 8), LocalTime.of(hour +1, 8));
            route.addStopOver(locationMap.get("Amersfoort Hooglandsepoort"), LocalTime.of(hour +1, 8), LocalTime.of(hour +1, 8));
            route.addEndPoint(locationMap.get("Amersfoort Het Rode Hert"), LocalTime.of(hour +1, 10));
            addRoute(route);

        }

        //////////ROUTE AMERSFOORT HET RODE HERT - AMERSFOORT CENTRAAL STATION (HELE UREN)
        for (int hour = 5; hour <= 21; hour++) {
            Route route = new Route(locationMap.get("Amersfoort Het Rode Hert"), LocalTime.of(hour, 50));
            route.addStopOver(locationMap.get("Amersfoort Hooglandsepoort"), LocalTime.of(hour , 52), LocalTime.of(hour, 52));
            route.addStopOver(locationMap.get("Hoogland Pastoor Pieckweg"), LocalTime.of(hour, 52), LocalTime.of(hour, 52));
            route.addStopOver(locationMap.get("Hoogland Zevenhuizerstraat"), LocalTime.of(hour , 54), LocalTime.of(hour, 54));
            route.addStopOver(locationMap.get("Amersfoort Meander MC"), LocalTime.of(hour , 57), LocalTime.of(hour, 57));
            route.addStopOver(locationMap.get("Amersfoort Kwekersweg"), LocalTime.of(hour , 58), LocalTime.of(hour, 58));
            route.addStopOver(locationMap.get("Amersfoort Brabantsestraat"), LocalTime.of(hour , 59), LocalTime.of(hour, 59));
            route.addStopOver(locationMap.get("Amersfoort Eemplein"), LocalTime.of(hour +1, 0), LocalTime.of(hour +1, 0));
            route.addStopOver(locationMap.get("Amersfoort Stadhuis"), LocalTime.of(hour +1, 1), LocalTime.of(hour +1, 1));
            route.addEndPoint(locationMap.get("Amersfoort Centraal Station"), LocalTime.of(hour +1, 3));
            addRoute(route);
        }

        //////////ROUTE AMERSFOORT HET RODE HERT - AMERSFOORT CENTRAAL STATION (HALVE UREN)
        for (int hour = 6; hour <= 21; hour++) {
            Route route = new Route(locationMap.get("Amersfoort Het Rode Hert"), LocalTime.of(hour, 20));
            route.addStopOver(locationMap.get("Amersfoort Hooglandsepoort"), LocalTime.of(hour , 22), LocalTime.of(hour, 22));
            route.addStopOver(locationMap.get("Hoogland Pastoor Pieckweg"), LocalTime.of(hour, 22), LocalTime.of(hour, 22));
            route.addStopOver(locationMap.get("Hoogland Zevenhuizerstraat"), LocalTime.of(hour , 24), LocalTime.of(hour, 24));
            route.addStopOver(locationMap.get("Amersfoort Meander MC"), LocalTime.of(hour , 27), LocalTime.of(hour, 27));
            route.addStopOver(locationMap.get("Amersfoort Kwekersweg"), LocalTime.of(hour , 28), LocalTime.of(hour, 28));
            route.addStopOver(locationMap.get("Amersfoort Brabantsestraat"), LocalTime.of(hour , 29), LocalTime.of(hour, 29));
            route.addStopOver(locationMap.get("Amersfoort Eemplein"), LocalTime.of(hour , 30), LocalTime.of(hour , 30));
            route.addStopOver(locationMap.get("Amersfoort Stadhuis"), LocalTime.of(hour , 31), LocalTime.of(hour , 31));
            route.addEndPoint(locationMap.get("Amersfoort Centraal Station"), LocalTime.of(hour , 33));
            addRoute(route);
        }

        //////////ROUTE AMERSFOORT CENTRAAL STATION - AMERSFOORT AMERENA (HELE UREN)
        for (int hour = 5; hour <= 22; hour++) {
            Route route = new Route(locationMap.get("Amersfoort Centraal Station"), LocalTime.of(hour, 58));
            route.addStopOver(locationMap.get("Amersfoort Centrum"), LocalTime.of(hour +1, 0), LocalTime.of(hour +1, 0));
            route.addStopOver(locationMap.get("Amersfoort De Kei"), LocalTime.of(hour +1, 1), LocalTime.of(hour +1, 1));
            route.addStopOver(locationMap.get("Amersfoort Hendrik van Viandenstraat"), LocalTime.of(hour +1, 2), LocalTime.of(hour +1, 2));
            route.addStopOver(locationMap.get("Amersfoort Blekerseiland"), LocalTime.of(hour +1, 2), LocalTime.of(hour +1, 2));
            route.addStopOver(locationMap.get("Amersfoort Hogeweg"), LocalTime.of(hour +1, 3), LocalTime.of(hour +1, 3));
            route.addEndPoint(locationMap.get("Amersfoort Amerena"), LocalTime.of(hour +1, 5));
            addRoute(route);
        }

        //////////ROUTE AMERSFOORT CENTRAAL STATION - AMERSFOORT AMERENA (HALVE UREN)
        for (int hour = 6; hour <= 22; hour++) {
            Route route = new Route(locationMap.get("Amersfoort Centraal Station"), LocalTime.of(hour, 28));
            route.addStopOver(locationMap.get("Amersfoort Centrum"), LocalTime.of(hour, 30), LocalTime.of(hour, 30));
            route.addStopOver(locationMap.get("Amersfoort De Kei"), LocalTime.of(hour, 31), LocalTime.of(hour, 31));
            route.addStopOver(locationMap.get("Amersfoort Hendrik van Viandenstraat"), LocalTime.of(hour, 32), LocalTime.of(hour, 32));
            route.addStopOver(locationMap.get("Amersfoort Blekerseiland"), LocalTime.of(hour, 32), LocalTime.of(hour, 32));
            route.addStopOver(locationMap.get("Amersfoort Hogeweg"), LocalTime.of(hour, 33), LocalTime.of(hour, 33));
            route.addEndPoint(locationMap.get("Amersfoort Amerena"), LocalTime.of(hour, 35));
            addRoute(route);
        }

        //////////ROUTE AMERSFOORT AMERENA - AMERSFOORT CENTRAAL STATION (HELE UREN)
        for (int hour = 6; hour <= 22; hour++) {
            Route route = new Route(locationMap.get("Amersfoort Amerena"), LocalTime.of(hour, 15));
            route.addStopOver(locationMap.get("Amersfoort Hogeweg"), LocalTime.of(hour, 17), LocalTime.of(hour, 17));
            route.addStopOver(locationMap.get("Amersfoort Blekerseiland"), LocalTime.of(hour, 18), LocalTime.of(hour, 18));
            route.addStopOver(locationMap.get("Amersfoort Hendrik van Viandenstraat"), LocalTime.of(hour, 18), LocalTime.of(hour, 18));
            route.addStopOver(locationMap.get("Amersfoort De Kei"), LocalTime.of(hour, 19), LocalTime.of(hour, 19));
            route.addStopOver(locationMap.get("Amersfoort Centrum"), LocalTime.of(hour, 20), LocalTime.of(hour, 20));
            route.addEndPoint(locationMap.get("Amersfoort Centraal Station"), LocalTime.of(hour, 22));
            addRoute(route);
        }

        //////////ROUTE AMERSFOORT AMERENA - AMERSFOORT CENTRAAL STATION (HALVE UREN)
        for (int hour = 6; hour <= 22; hour++) {
            Route route = new Route(locationMap.get("Amersfoort Amerena"), LocalTime.of(hour, 45));
            route.addStopOver(locationMap.get("Amersfoort Hogeweg"), LocalTime.of(hour, 47), LocalTime.of(hour, 47));
            route.addStopOver(locationMap.get("Amersfoort Blekerseiland"), LocalTime.of(hour, 48), LocalTime.of(hour, 48));
            route.addStopOver(locationMap.get("Amersfoort Hendrik van Viandenstraat"), LocalTime.of(hour, 48), LocalTime.of(hour, 48));
            route.addStopOver(locationMap.get("Amersfoort De Kei"), LocalTime.of(hour, 49), LocalTime.of(hour, 49));
            route.addStopOver(locationMap.get("Amersfoort Centrum"), LocalTime.of(hour, 50), LocalTime.of(hour, 50));
            route.addEndPoint(locationMap.get("Amersfoort Centraal Station"), LocalTime.of(hour, 52));
            addRoute(route);
        }

        //////////ROUTE AMERSFOORT CENTRAAL STATION - AMERSFOORT AMERHORST (HELE UREN)
        for (int hour = 6; hour <= 22; hour++) {
            Route route = new Route(locationMap.get("Amersfoort Centraal Station"), LocalTime.of(hour, 29));
            route.addStopOver(locationMap.get("Amersfoort Centrum"), LocalTime.of(hour, 30), LocalTime.of(hour, 30));
            route.addStopOver(locationMap.get("Amersfoort Zonnehof-West"), LocalTime.of(hour, 31), LocalTime.of(hour, 31));
            route.addStopOver(locationMap.get("Amersfoort Van Stolbergpark"), LocalTime.of(hour, 32), LocalTime.of(hour, 32));
            route.addStopOver(locationMap.get("Amersfoort Daltonstraat"), LocalTime.of(hour, 32), LocalTime.of(hour, 32));
            route.addStopOver(locationMap.get("Amersfoort Borneoplein"), LocalTime.of(hour, 34), LocalTime.of(hour, 34));
            route.addStopOver(locationMap.get("Amersfoort Einsteinstraat"), LocalTime.of(hour, 34), LocalTime.of(hour, 34));
            route.addEndPoint(locationMap.get("Amersfoort Amerhorst"), LocalTime.of(hour, 36));
            addRoute(route);
        }

        //////////ROUTE AMERSFOORT CENTRAAL STATION - AMERSFOORT AMERHORST (HALVE UREN)
        for (int hour = 6; hour <= 22; hour++) {
            Route route = new Route(locationMap.get("Amersfoort Centraal Station"), LocalTime.of(hour, 59));
            route.addStopOver(locationMap.get("Amersfoort Centrum"), LocalTime.of(hour +1, 0), LocalTime.of(hour +1, 0));
            route.addStopOver(locationMap.get("Amersfoort Zonnehof-West"), LocalTime.of(hour +1, 1), LocalTime.of(hour +1, 1));
            route.addStopOver(locationMap.get("Amersfoort Van Stolbergpark"), LocalTime.of(hour +1, 2), LocalTime.of(hour +1, 2));
            route.addStopOver(locationMap.get("Amersfoort Daltonstraat"), LocalTime.of(hour +1, 2), LocalTime.of(hour +1, 2));
            route.addStopOver(locationMap.get("Amersfoort Borneoplein"), LocalTime.of(hour +1, 4), LocalTime.of(hour +1, 4));
            route.addStopOver(locationMap.get("Amersfoort Einsteinstraat"), LocalTime.of(hour +1, 4), LocalTime.of(hour +1, 4));
            route.addEndPoint(locationMap.get("Amersfoort Amerhorst"), LocalTime.of(hour +1, 6));
            addRoute(route);
        }

        //////////ROUTE AMERSFOORT AMERHORST - AMERSFOORT CENTRAAL STATION (HELE UREN)
        for (int hour = 6; hour <= 22; hour++) {
            Route route = new Route(locationMap.get("Amersfoort Amerhorst"), LocalTime.of(hour, 12));
            route.addStopOver(locationMap.get("Amersfoort Einsteinstraat"), LocalTime.of(hour, 14), LocalTime.of(hour, 14));
            route.addStopOver(locationMap.get("Amersfoort Borneoplein"), LocalTime.of(hour, 14), LocalTime.of(hour, 14));
            route.addStopOver(locationMap.get("Amersfoort Daltonstraat"), LocalTime.of(hour, 16), LocalTime.of(hour, 16));
            route.addStopOver(locationMap.get("Amersfoort Van Stolbergpark"), LocalTime.of(hour, 16), LocalTime.of(hour, 16));
            route.addStopOver(locationMap.get("Amersfoort Zonnehof-West"), LocalTime.of(hour, 17), LocalTime.of(hour, 17));
            route.addStopOver(locationMap.get("Amersfoort Centrum"), LocalTime.of(hour, 18), LocalTime.of(hour, 18));
            route.addEndPoint(locationMap.get("Amersfoort Centraal Station"), LocalTime.of(hour, 19));
            addRoute(route);
        }

        //////////ROUTE AMERSFOORT AMERHORST - AMERSFOORT CENTRAAL STATION (HALVE UREN)
        for (int hour = 6; hour <= 22; hour++) {
            Route route = new Route(locationMap.get("Amersfoort Amerhorst"), LocalTime.of(hour, 42));
            route.addStopOver(locationMap.get("Amersfoort Einsteinstraat"), LocalTime.of(hour, 44), LocalTime.of(hour, 44));
            route.addStopOver(locationMap.get("Amersfoort Borneoplein"), LocalTime.of(hour, 44), LocalTime.of(hour, 44));
            route.addStopOver(locationMap.get("Amersfoort Daltonstraat"), LocalTime.of(hour, 46), LocalTime.of(hour, 46));
            route.addStopOver(locationMap.get("Amersfoort Van Stolbergpark"), LocalTime.of(hour, 46), LocalTime.of(hour, 46));
            route.addStopOver(locationMap.get("Amersfoort Zonnehof-West"), LocalTime.of(hour, 47), LocalTime.of(hour, 47));
            route.addStopOver(locationMap.get("Amersfoort Centrum"), LocalTime.of(hour, 48), LocalTime.of(hour, 48));
            route.addEndPoint(locationMap.get("Amersfoort Centraal Station"), LocalTime.of(hour, 49));
            addRoute(route);
        }

        //////////ROUTE AMERSFOORT CENTRAAL STATION - LEUSDEN ISVW (HELE UREN)
        for (int hour = 7; hour <= 19; hour++) {
            Route route = new Route(locationMap.get("Amersfoort Centraal Station"), LocalTime.of(hour, 40));
            route.addStopOver(locationMap.get("Amersfoort Bergkerk"), LocalTime.of(hour, 42), LocalTime.of(hour, 42));
            route.addStopOver(locationMap.get("Amersfoort Vondellaan"), LocalTime.of(hour, 43), LocalTime.of(hour, 43));
            route.addStopOver(locationMap.get("Amersfoort Hugo de Grootlaan"), LocalTime.of(hour, 44), LocalTime.of(hour, 44));
            route.addStopOver(locationMap.get("Amersfoort Curacaolaan"), LocalTime.of(hour, 45), LocalTime.of(hour, 45));
            route.addStopOver(locationMap.get("Amersfoort Borneoplein"), LocalTime.of(hour, 46), LocalTime.of(hour, 46));
            route.addStopOver(locationMap.get("Amersfoort Timorstraat"), LocalTime.of(hour, 47), LocalTime.of(hour, 47));
            route.addEndPoint(locationMap.get("Leusden ISVW"), LocalTime.of(hour, 52));
            addRoute(route);
        }

        //////////ROUTE AMERSFOORT CENTRAAL STATION - LEUSDEN ISVW (HALVE UREN)
        for (int hour = 8; hour <= 19; hour++) {
            Route route = new Route(locationMap.get("Amersfoort Centraal Station"), LocalTime.of(hour, 10));
            route.addStopOver(locationMap.get("Amersfoort Bergkerk"), LocalTime.of(hour, 12), LocalTime.of(hour, 12));
            route.addStopOver(locationMap.get("Amersfoort Vondellaan"), LocalTime.of(hour, 13), LocalTime.of(hour, 13));
            route.addStopOver(locationMap.get("Amersfoort Hugo de Grootlaan"), LocalTime.of(hour, 14), LocalTime.of(hour, 14));
            route.addStopOver(locationMap.get("Amersfoort Curacaolaan"), LocalTime.of(hour, 15), LocalTime.of(hour, 15));
            route.addStopOver(locationMap.get("Amersfoort Borneoplein"), LocalTime.of(hour, 16), LocalTime.of(hour, 16));
            route.addStopOver(locationMap.get("Amersfoort Timorstraat"), LocalTime.of(hour, 17), LocalTime.of(hour, 17));
            route.addEndPoint(locationMap.get("Leusden ISVW"), LocalTime.of(hour, 22));
            addRoute(route);
        }

        //////////ROUTE LEUSDEN ISVW - AMERSFOORT CENTRAAL STATION (HELE UREN)
        for (int hour = 7; hour <= 19; hour++) {
            Route route = new Route(locationMap.get("Leusden ISVW"), LocalTime.of(hour, 18));
            route.addStopOver(locationMap.get("Amersfoort Timorstraat"), LocalTime.of(hour, 23), LocalTime.of(hour, 23));
            route.addStopOver(locationMap.get("Amersfoort Borneoplein"), LocalTime.of(hour, 24), LocalTime.of(hour, 24));
            route.addStopOver(locationMap.get("Amersfoort Curacaolaan"), LocalTime.of(hour, 25), LocalTime.of(hour, 25));
            route.addStopOver(locationMap.get("Amersfoort Hugo de Grootlaan"), LocalTime.of(hour, 26), LocalTime.of(hour, 26));
            route.addStopOver(locationMap.get("Amersfoort Vondellaan"), LocalTime.of(hour, 27), LocalTime.of(hour, 27));
            route.addStopOver(locationMap.get("Amersfoort Bergkerk"), LocalTime.of(hour, 28), LocalTime.of(hour, 28));
            route.addEndPoint(locationMap.get("Amersfoort Centraal Station"), LocalTime.of(hour, 30));
            addRoute(route);
        }

        //////////ROUTE LEUSDEN ISVW - AMERSFOORT CENTRAAL STATION (HALVE UREN)
        for (int hour = 7; hour <= 19; hour++) {
            Route route = new Route(locationMap.get("Leusden ISVW"), LocalTime.of(hour, 48));
            route.addStopOver(locationMap.get("Amersfoort Timorstraat"), LocalTime.of(hour, 53), LocalTime.of(hour, 53));
            route.addStopOver(locationMap.get("Amersfoort Borneoplein"), LocalTime.of(hour, 54), LocalTime.of(hour, 54));
            route.addStopOver(locationMap.get("Amersfoort Curacaolaan"), LocalTime.of(hour, 55), LocalTime.of(hour, 55));
            route.addStopOver(locationMap.get("Amersfoort Hugo de Grootlaan"), LocalTime.of(hour, 56), LocalTime.of(hour, 56));
            route.addStopOver(locationMap.get("Amersfoort Vondellaan"), LocalTime.of(hour, 57), LocalTime.of(hour, 57));
            route.addStopOver(locationMap.get("Amersfoort Bergkerk"), LocalTime.of(hour, 58), LocalTime.of(hour, 58));
            route.addEndPoint(locationMap.get("Amersfoort Centraal Station"), LocalTime.of(hour +1, 0));
            addRoute(route);
        }
    }
}

