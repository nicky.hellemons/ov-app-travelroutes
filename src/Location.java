//Nicky & Ivo
public class Location
{
    private String locationName;
    protected Coordinates coordinates;

    public Location(String locationName, Coordinates coordinates) {
        this.locationName=locationName;
        this.coordinates = coordinates;
    }

    public String getLocationName() {
        return locationName;
    }

    public Coordinates getCoordinates() {
        return coordinates;
    }

}
