import java.time.LocalTime;

//Nicky & Ivo
public class Stopover extends Location {

    private LocalTime arrival;
    private LocalTime departure;


    //Constructor
    public Stopover(String locationName, LocalTime arrival, LocalTime departure) {
        super(locationName, null);
        this.arrival = arrival;
        this.departure = departure;
    }

    public LocalTime getArrival() {
        return arrival;
    }

    public LocalTime getDeparture() {
        return departure;
    }

    public String getDepartureString() {
        return departure.toString();
    }
}


