import java.time.LocalTime;

public class TestData extends Data {
    public TestData() {

        //////////ROUTES
        Location location = new Location("A", new Coordinates(0,0));
        locationMap.put(location.getLocationName(), location);

        location = new Location("B", new Coordinates(0,0));
        locationMap.put(location.getLocationName(), location);

        location = new Location("C", new Coordinates(0,0));
        locationMap.put(location.getLocationName(), location);

        location = new Location("D", new Coordinates(0,0));
        locationMap.put(location.getLocationName(), location);

        location = new Location("E", new Coordinates(0,0));
        locationMap.put(location.getLocationName(), location);

        location = new Location("F", new Coordinates(0,0));
        locationMap.put(location.getLocationName(), location);


        //////////ROUTES

        //////////TEST
        int hour = 6;
        int minutes = 15;
            Route route = new Route(locationMap.get("A"), LocalTime.of(hour, minutes));
            route.addStopOver(locationMap.get("B"), LocalTime.of(hour , 50), LocalTime.of(hour, minutes + 15));
            route.addStopOver(locationMap.get("C"), LocalTime.of(hour, 50), LocalTime.of(hour +1, minutes + 30));
            route.addStopOver(locationMap.get("D"), LocalTime.of(hour , 51), LocalTime.of(hour +2, minutes));
            route.addStopOver(locationMap.get("E"), LocalTime.of(hour , 52), LocalTime.of(hour +3, minutes + 15));
            route.addEndPoint(locationMap.get("F"), LocalTime.of(hour +4, minutes + 30));
            addRoute(route);
        }
    }


