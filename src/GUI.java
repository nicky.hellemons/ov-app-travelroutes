import javax.imageio.ImageIO;
import javax.swing.*;
import javax.swing.border.Border;
import javax.swing.border.EmptyBorder;
import javax.swing.border.LineBorder;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.text.DecimalFormat;
import java.time.LocalTime;
import java.util.List;
import java.util.Locale;
import java.util.Objects;
import java.util.ResourceBundle;

//Koen
public class GUI implements ActionListener, ItemListener {
    final static String PLAN = "plannen";
    final static String LOGIN = "login";
    final static String REGISTER = "register";
    final static String RECENT = "recente routes";
    final static String LOGINREGISTER = "login of registreren";
    final static String PROFILE = "profielpagina";
    Login login = new Login();
    Data data = new TrainData();
    private boolean darkmodeStatus = false;
    Trips trips;
    LocalTime lt = LocalTime.now();
    JFrame frame;
    JPanel mainPanel;
    JPanel home;
    JPanel homeLeft;
    JPanel homeCenter;
    JPanel recentRoutes;
    JPanel loginMain;
    JButton loginButton;
    JButton registerButton;
    JButton goToPlan;
    JButton goToPlan1;
    JButton goToPlan2;
    JButton goToPlan3;
    JButton goToLogin;
    JButton goToLogin1;
    JButton goToRegister;
    JButton goToRegister1;
    JButton timeSelectNow;
    JButton planButton;
    JButton logoutButton;
    JButton language;
    JButton darkModeButton;
    JPanel profilePage;
    JPanel loginPanel;
    JRadioButton trainSelect;
    JRadioButton planeSelect;
    JRadioButton tramSelect;
    JRadioButton metroSelect;
    JRadioButton busSelect;
    JComboBox<String> comboBoxStart;
    JComboBox<String> comboBoxEnd;
    JComboBox<Integer> timeHourSelect;
    JComboBox<Integer> timeMinutesSelect;
    JLabel route1Start;
    JLabel route1End;
    JLabel route1TimeStart;
    JLabel route1TimeEnd;
    JLabel route2Start;
    JLabel route2End;
    JLabel route2TimeStart;
    JLabel route2TimeEnd;
    JLabel route3Start;
    JLabel route3End;
    JLabel route3TimeStart;
    JLabel route3TimeEnd;
    JLabel route4Start;
    JLabel route4End;
    JLabel route4TimeStart;
    JLabel route4TimeEnd;
    JLabel route1DistanceText;
    JLabel route2DistanceText;
    JLabel route3DistanceText;
    JLabel route4DistanceText;
    JLabel separator;
    JLabel separator1;
    JLabel homeCenterLabel;
    JLabel homeCenterLabel1;
    JLabel homeCenterLabel2;
    JLabel homeCenterLabel3;
    JLabel homeCenterLabel4;
    JLabel temp;
    JLabel inlogStatus;
    JLabel inlogStatus2;
    JPanel plan;
    JPanel planInp;
    JPanel planStartSearch;
    JPanel planEndSearch;
    JPanel timeInp;
    JPanel timeHour;
    JPanel hourMinuteSeparator;
    JPanel timeMinutes;
    JPanel timeNowSeparator;
    JPanel nowButton;
    JPanel transType;
    JPanel homeCenterText;
    JPanel buttons;
    JPanel betweenRoute1;
    JPanel betweenRoute2;
    JPanel betweenRoute3;
    JPanel betweenRoute4;
    JPanel route1TimeAndDistance;
    JPanel route1Distance;
    JPanel route2TimeAndDistance;
    JPanel route2Distance;
    JPanel route3TimeAndDistance;
    JPanel route3Distance;
    JPanel route4TimeAndDistance;
    JPanel route4Distance;
    JPanel logoutButtonPanel;
    JPanel planButtonPanel;
    JPanel profilePage1;
    JPanel emailText1;
    JLabel email1;
    JPanel pwText2;
    JLabel password2;
    JTextField textField6;
    JPanel emailInp1;
    JPasswordField passwordField2;
    JPanel pwInp2;
    JPanel panel;
    JPanel panel3;
    JLabel registerStatus;
    JLabel inputUsernameStatus;
    JLabel inputEmailSatus;
    JLabel inputPasswordStatus;
    JLabel inputRepeatPasswordStatus;
    JLabel foutUserStatus;
    //registratiepagina
    JPanel registerMain;
    JPanel register;
    JPanel register1;
    JPanel firstNameText;
    JLabel firstName;
    JPanel firstNameInp;
    JTextField textField2;
    JPanel lastNameText;
    JLabel lastName;
    JPanel lastNameInp;
    JTextField textField3;
    JPanel usernameText;
    JLabel username;
    JPanel usernameInp;
    JTextField textField4;
    JPanel emailText;
    JLabel email;
    JPanel emailInp;
    JTextField textField5;
    JPanel pwText;
    JLabel password;
    JPanel pwInp;
    JPasswordField passwordField;
    JPanel pwText1;
    JLabel password1;
    JPanel pwInp1;
    JPasswordField passwordField1;
    JPanel panel1;
    JPanel panel2;
    //profilepage
    JLabel updateStatus;
    JPanel profileMain;
    JButton updateButton;
    JButton profileButton;
    JButton logOutButton;
    JPanel updateFirstNameText;
    JPanel firstNameInput;
    JLabel updateFirstName;
    JTextField inpFirstName;
    JPanel lastNameUpdateText;
    JLabel lastName2;
    JPanel lastNameInp2;
    JPanel usernameText2;
    JLabel username2;
    JTextField inpLastName;
    JPanel usernameInp2;
    JTextField inpUsername;
    JPanel emailText2;
    JLabel email2;
    JPanel emailInp2;
    JTextField inpEmail;
    JPanel pwText3;
    JLabel password3;
    JPanel pwInp3;
    JPasswordField inpPassword;
    JPanel panel5;
    JPanel panel4;
    JPanel avatarPanel;
    //plan panel
    JPanel routeMain;
    JPanel routeTitle;
    JLabel routeTitleText;
    JPanel routes;
    JPanel showRoute1;
    JPanel showRoute1Text;
    JPanel showRoute1Time;
    JLabel route1TimeSeparator;
    JPanel showRoute3;
    JPanel showRoute3Text;
    JLabel route2TimeSeparator;
    JLabel route3Separator;
    JLabel route3TimeSeparator;
    JPanel showRoute4;
    JPanel showRoute4Text;
    JLabel route4Separator;
    JPanel showRoute4Time;
    JLabel route4TimeSeparator;
    JPanel panel6;
    JLabel route1Separator;
    JPanel showRoute2;
    JPanel showRoute2Text;
    JLabel route2Separator;
    JPanel showRoute2Time;
    JPanel showRoute3Time;

    JLabel routeLabel1;
    JLabel routeLabel2;
    JLabel routeLabel3;
    JLabel routeLabel4;

    JLabel arrow1;
    JLabel arrow2;
    JLabel arrow3;
    JLabel arrow4;

    static Locale localeGetDefault = Locale.getDefault();
    static ResourceBundle rb = ResourceBundle.getBundle("info", localeGetDefault);
    Locale localeUS = new Locale("en", "US");
    Locale localeNL = new Locale("nl", "NL");
    boolean languageSwitchBoolean;
    JButton printScreen;

    ImageIcon icon;
    ImageIcon icon1;

    ImageIcon avatar1 = new ImageIcon("pictures/vrouwAvatarPNG.png");
    ImageIcon avatar2 = new ImageIcon("pictures/manAvatarJPG.jpg");


    Color backgroundColorLight = new Color(238, 238, 238);
    Color textColorLight = new Color(0, 0, 0);
    Color buttonColorLight = new Color(255, 98, 0);
    Color buttonTextColorLight = new Color(0, 0, 0);
    Color backgroundColorDark = new Color(31, 31, 31);
    Color textColorDark = new Color(238, 238, 238);
    Color buttonColorDark = new Color(255, 98, 0);
    Color buttonTextColorDark = new Color(238, 238, 238);
    ActionListener colourSwitch = new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
            if (e.getSource() == darkModeButton) {

                if (darkModeButton.getText().equals("Dark Mode")) {

                    darkModeButton.setText("Light Mode");
                    darkModeButton.setBackground(buttonColorDark);
                    darkModeButton.setForeground(buttonTextColorDark);
                    homeLeft.setBackground(backgroundColorDark);
                    homeLeft.setBorder(new LineBorder(textColorDark, 1));
                    plan.setBackground(backgroundColorDark);
                    planInp.setBackground(backgroundColorDark);
                    planStartSearch.setBackground(backgroundColorDark);
                    planEndSearch.setBackground(backgroundColorDark);
                    timeInp.setBackground(backgroundColorDark);
                    timeHour.setBackground(backgroundColorDark);
                    timeHourSelect.setBackground(buttonColorDark);
                    timeHourSelect.setForeground(buttonTextColorDark);
                    hourMinuteSeparator.setBackground(backgroundColorDark);
                    separator.setForeground(textColorDark);
                    timeMinutes.setBackground(backgroundColorDark);
                    timeMinutesSelect.setBackground(buttonColorDark);
                    timeMinutesSelect.setForeground(buttonTextColorDark);
                    timeNowSeparator.setBackground(backgroundColorDark);
                    separator1.setForeground(textColorDark);
                    nowButton.setBackground(backgroundColorDark);
                    timeSelectNow.setBackground(buttonColorDark);
                    timeSelectNow.setForeground(buttonTextColorDark);
                    transType.setBackground(backgroundColorDark);
                    trainSelect.setBackground(backgroundColorDark);
                    trainSelect.setForeground(textColorDark);
                    planeSelect.setBackground(backgroundColorDark);
                    planeSelect.setForeground(textColorDark);
                    tramSelect.setBackground(backgroundColorDark);
                    tramSelect.setForeground(textColorDark);
                    metroSelect.setBackground(backgroundColorDark);
                    metroSelect.setForeground(textColorDark);
                    busSelect.setBackground(backgroundColorDark);
                    busSelect.setForeground(textColorDark);
                    comboBoxStart.setBackground(buttonColorDark);
                    comboBoxStart.setForeground(buttonTextColorDark);
                    comboBoxEnd.setBackground(buttonColorDark);
                    comboBoxEnd.setForeground(buttonTextColorDark);
                    goToRegister.setBackground(buttonColorDark);
                    goToRegister.setForeground(buttonTextColorDark);
                    goToLogin.setBackground(buttonColorDark);
                    goToLogin.setForeground(buttonTextColorDark);
                    homeCenter.setBackground(backgroundColorDark);
                    homeCenterText.setBackground(backgroundColorDark);
                    homeCenterLabel.setForeground(textColorDark);
                    homeCenterLabel1.setForeground(textColorDark);
                    homeCenterLabel2.setForeground(textColorDark);
                    homeCenterLabel3.setForeground(textColorDark);
                    homeCenterLabel4.setForeground(textColorDark);
                    buttons.setBackground(backgroundColorDark);
                    recentRoutes.setBackground(backgroundColorDark);
                    temp.setForeground(textColorDark);
                    logoutButtonPanel.setBackground(backgroundColorDark);
                    logoutButton.setBackground(buttonColorDark);
                    logoutButton.setForeground(buttonTextColorDark);
                    planButtonPanel.setBackground(backgroundColorDark);
                    planButton.setBackground(buttonColorDark);
                    planButton.setForeground(buttonTextColorDark);
                    language.setBackground(buttonColorDark);
                    language.setForeground(buttonTextColorDark);
                    loginMain.setBackground(backgroundColorDark);
                    loginPanel.setBackground(backgroundColorDark);
                    emailText1.setBackground(backgroundColorDark);
                    email1.setForeground(textColorDark);
                    pwText2.setBackground(backgroundColorDark);
                    textField6.setForeground(buttonTextColorDark);
                    textField6.setBackground(buttonColorDark);
                    password2.setForeground(textColorDark);
                    emailInp1.setBackground(backgroundColorDark);
                    passwordField2.setForeground(buttonTextColorDark);
                    passwordField2.setBackground(buttonColorDark);
                    pwInp2.setBackground(backgroundColorDark);
                    panel.setBackground(backgroundColorDark);
                    goToRegister1.setBackground(buttonColorDark);
                    goToRegister1.setForeground(buttonTextColorDark);
                    goToPlan1.setBackground(buttonColorDark);
                    goToPlan1.setForeground(buttonTextColorDark);
                    loginButton.setBackground(buttonColorDark);
                    loginButton.setForeground(buttonTextColorDark);
                    panel3.setBackground(backgroundColorDark);
                    //
                    avatarPanel.setBackground(backgroundColorDark);
                    profilePage.setBackground(backgroundColorDark);
                    profilePage1.setBackground(backgroundColorDark);
                    panel4.setBackground(backgroundColorDark);
                    profileMain.setBackground(backgroundColorDark);
                    profilePage.setBackground(backgroundColorDark);
                    profilePage1.setBackground(backgroundColorDark);
                    updateFirstNameText.setBackground(backgroundColorDark);
                    updateFirstName.setForeground(textColorDark);
                    firstNameInput.setBackground(backgroundColorDark);
                    inpFirstName.setForeground(buttonTextColorDark);
                    inpFirstName.setBackground(buttonColorDark);
                    lastNameUpdateText.setBackground(backgroundColorDark);
                    lastName2.setForeground(textColorDark);
                    lastNameInp2.setBackground(backgroundColorDark);
                    inpLastName.setForeground(buttonTextColorDark);
                    inpLastName.setBackground(buttonColorDark);
                    usernameText2.setBackground(backgroundColorDark);
                    username2.setForeground(textColorDark);
                    usernameInp2.setBackground(backgroundColorDark);
                    inpUsername.setForeground(buttonTextColorDark);
                    inpUsername.setBackground(buttonColorDark);
                    emailText2.setBackground(backgroundColorDark);
                    email2.setForeground(textColorDark);
                    emailInp2.setBackground(backgroundColorDark);
                    inpEmail.setForeground(buttonTextColorDark);
                    inpEmail.setBackground(buttonColorDark);
                    inpPassword.setBackground(buttonColorDark);
                    inpPassword.setForeground(buttonTextColorDark);
                    pwInp3.setBackground(backgroundColorDark);
                    password3.setForeground(textColorDark);
                    pwText3.setBackground(backgroundColorDark);
                    panel5.setBackground(backgroundColorDark);
                    updateButton.setBackground(buttonColorDark);
                    updateButton.setForeground(buttonTextColorDark);
                    panel4.setBackground(backgroundColorDark);
                    goToPlan3.setBackground(buttonColorDark);
                    goToPlan3.setForeground(buttonTextColorDark);
                    //register
                    register.setBackground(backgroundColorDark);
                    registerMain.setBackground(backgroundColorDark);
                    register1.setBackground(backgroundColorDark);
                    firstNameText.setBackground(backgroundColorDark);
                    firstName.setForeground(textColorDark);
                    firstNameInp.setBackground(backgroundColorDark);
                    textField2.setForeground(buttonTextColorDark);
                    textField2.setBackground(buttonColorDark);
                    lastNameText.setBackground(backgroundColorDark);
                    lastNameInp.setBackground(backgroundColorDark);
                    lastName.setForeground(textColorDark);
                    textField3.setForeground(buttonTextColorDark);
                    usernameText.setBackground(backgroundColorDark);
                    textField3.setBackground(buttonColorDark);
                    textField4.setForeground(buttonTextColorDark);
                    textField4.setBackground(buttonColorDark);
                    usernameInp.setBackground(backgroundColorDark);
                    emailText.setBackground(backgroundColorDark);
                    email.setForeground(textColorDark);
                    emailInp.setBackground(backgroundColorDark);
                    textField5.setForeground(buttonTextColorDark);
                    textField5.setBackground(buttonColorDark);
                    passwordField.setBackground(buttonColorDark);
                    passwordField.setForeground(buttonTextColorDark);
                    pwText.setBackground(backgroundColorDark);
                    pwText1.setBackground(backgroundColorDark);
                    password1.setForeground(textColorDark);
                    pwInp1.setBackground(backgroundColorDark);
                    passwordField1.setForeground(buttonTextColorDark);
                    passwordField1.setBackground(buttonColorDark);
                    panel1.setBackground(backgroundColorDark);
                    registerButton.setBackground(buttonColorDark);
                    registerButton.setForeground(buttonTextColorDark);
                    panel2.setBackground(backgroundColorDark);
                    goToPlan.setBackground(buttonColorDark);
                    goToPlan.setForeground(buttonTextColorDark);
                    goToLogin1.setBackground(buttonColorDark);
                    goToLogin1.setForeground(buttonTextColorDark);
                    username.setBackground(backgroundColorDark);
                    username.setForeground(textColorDark);
                    password.setBackground(backgroundColorDark);
                    password.setForeground(textColorDark);
                    pwInp.setBackground(backgroundColorDark);

                    logOutButton.setBackground(buttonColorDark);
                    logOutButton.setForeground(buttonTextColorDark);
                    profileButton.setBackground(buttonColorDark);
                    profileButton.setForeground(buttonTextColorDark);

                    inlogStatus.setForeground(textColorDark);
                    inlogStatus2.setForeground(textColorDark);

                    registerStatus.setForeground(textColorDark);
                    registerStatus.setForeground(textColorDark);
                    inputUsernameStatus.setForeground(textColorDark);
                    inputEmailSatus.setForeground(textColorDark);
                    inputPasswordStatus.setForeground(textColorDark);
                    inputRepeatPasswordStatus.setForeground(textColorDark);
                    foutUserStatus.setForeground(textColorDark);
                    updateStatus.setForeground(textColorDark);

                    //plan panel
                    routes.setBackground(backgroundColorDark);
                    routeTitleText.setForeground(textColorDark);
                    routeTitle.setBackground(backgroundColorDark);
                    routeMain.setBackground(backgroundColorDark);
                    routeMain.setBorder(new LineBorder(textColorDark, 1));
                    showRoute1.setBackground(backgroundColorDark);
                    showRoute1Text.setBackground(backgroundColorDark);
                    route1Start.setForeground(textColorDark);
                    route1Separator.setForeground(textColorDark);
                    route1End.setForeground(textColorDark);
                    showRoute1Time.setBackground(backgroundColorDark);
                    route1TimeStart.setForeground(textColorDark);
                    route1TimeSeparator.setForeground(textColorDark);
                    route1TimeEnd.setForeground(textColorDark);
                    betweenRoute1.setBackground(backgroundColorDark);
                    showRoute2.setBackground(backgroundColorDark);
                    showRoute2Text.setBackground(backgroundColorDark);
                    route2Start.setForeground(textColorDark);
                    route2Separator.setForeground(textColorDark);
                    route2End.setForeground(textColorDark);
                    showRoute2Time.setBackground(backgroundColorDark);
                    route2TimeStart.setForeground(textColorDark);
                    route2TimeSeparator.setForeground(textColorDark);
                    route2TimeEnd.setForeground(textColorDark);
                    betweenRoute2.setBackground(backgroundColorDark);
                    showRoute3.setBackground(backgroundColorDark);
                    showRoute3Text.setBackground(backgroundColorDark);
                    route3Start.setForeground(textColorDark);
                    route3Separator.setForeground(textColorDark);
                    route3End.setForeground(textColorDark);
                    showRoute3Time.setBackground(backgroundColorDark);
                    route3TimeStart.setForeground(textColorDark);
                    route3TimeSeparator.setForeground(textColorDark);
                    route3TimeEnd.setForeground(textColorDark);
                    betweenRoute3.setBackground(backgroundColorDark);
                    showRoute4.setBackground(backgroundColorDark);
                    showRoute4Text.setBackground(backgroundColorDark);
                    route4Start.setForeground(textColorDark);
                    route4Separator.setForeground(textColorDark);
                    route4End.setForeground(textColorDark);
                    showRoute4Time.setBackground(backgroundColorDark);
                    route4TimeStart.setForeground(textColorDark);
                    route4TimeSeparator.setForeground(textColorDark);
                    route4TimeEnd.setForeground(textColorDark);
                    betweenRoute4.setBackground(backgroundColorDark);
                    panel4.setBackground(backgroundColorDark);
                    goToPlan2.setBackground(buttonColorDark);
                    goToPlan2.setForeground(buttonTextColorDark);
                    panel6.setBackground(backgroundColorDark);
                    route1TimeAndDistance.setBackground(backgroundColorDark);
                    route1Distance.setBackground(backgroundColorDark);
                    route1DistanceText.setForeground(textColorDark);
                    route2TimeAndDistance.setBackground(backgroundColorDark);
                    route2Distance.setBackground(backgroundColorDark);
                    route2DistanceText.setForeground(textColorDark);
                    route3TimeAndDistance.setBackground(backgroundColorDark);
                    route3Distance.setBackground(backgroundColorDark);
                    route3DistanceText.setForeground(textColorDark);
                    route4TimeAndDistance.setBackground(backgroundColorDark);
                    route4Distance.setBackground(backgroundColorDark);
                    route4DistanceText.setForeground(textColorDark);


                    darkmodeStatus = true;

                    if (comboBoxStart.getSelectedIndex() != -1) {
                        planRoute(getStartTime(), getEndPoint(), getStartPoint());
                    }

                } else {
                    darkmodeStatus = false;

                    if (comboBoxStart.getSelectedIndex() != -1) {
                        planRoute(getStartTime(), getEndPoint(), getStartPoint());
                    }
                    darkModeButton.setText("Dark Mode");
                    darkModeButton.setBackground(buttonColorLight);
                    darkModeButton.setForeground(buttonTextColorLight);
                    homeLeft.setBackground(backgroundColorLight);
                    homeLeft.setBorder(new LineBorder(textColorLight, 1));
                    plan.setBackground(backgroundColorLight);
                    planInp.setBackground(backgroundColorLight);
                    planStartSearch.setBackground(backgroundColorLight);
                    planEndSearch.setBackground(backgroundColorLight);
                    timeInp.setBackground(backgroundColorLight);
                    timeHour.setBackground(backgroundColorLight);
                    timeHourSelect.setBackground(buttonColorLight);
                    timeHourSelect.setForeground(buttonTextColorLight);
                    hourMinuteSeparator.setBackground(backgroundColorLight);
                    separator.setForeground(textColorLight);
                    timeMinutes.setBackground(backgroundColorLight);
                    timeMinutesSelect.setBackground(buttonColorLight);
                    timeMinutesSelect.setForeground(buttonTextColorLight);
                    timeNowSeparator.setBackground(backgroundColorLight);
                    separator1.setForeground(textColorLight);
                    nowButton.setBackground(backgroundColorLight);
                    timeSelectNow.setBackground(buttonColorLight);
                    timeSelectNow.setForeground(buttonTextColorLight);
                    transType.setBackground(backgroundColorLight);
                    trainSelect.setBackground(backgroundColorLight);
                    trainSelect.setForeground(textColorLight);
                    planeSelect.setBackground(backgroundColorLight);
                    planeSelect.setForeground(textColorLight);
                    tramSelect.setBackground(backgroundColorLight);
                    tramSelect.setForeground(textColorLight);
                    metroSelect.setBackground(backgroundColorLight);
                    metroSelect.setForeground(textColorLight);
                    busSelect.setBackground(backgroundColorLight);
                    busSelect.setForeground(textColorLight);
                    comboBoxStart.setBackground(buttonColorLight);
                    comboBoxStart.setForeground(buttonTextColorLight);
                    comboBoxEnd.setBackground(buttonColorLight);
                    comboBoxEnd.setForeground(buttonTextColorLight);
                    goToRegister.setBackground(buttonColorLight);
                    goToRegister.setForeground(buttonTextColorLight);
                    goToLogin.setBackground(buttonColorLight);
                    goToLogin.setForeground(buttonTextColorLight);
                    homeCenter.setBackground(backgroundColorLight);
                    homeCenterText.setBackground(backgroundColorLight);
                    homeCenterLabel.setForeground(textColorLight);
                    homeCenterLabel1.setForeground(textColorLight);
                    homeCenterLabel2.setForeground(textColorLight);
                    homeCenterLabel3.setForeground(textColorLight);
                    homeCenterLabel4.setForeground(textColorLight);
                    buttons.setBackground(backgroundColorLight);
                    recentRoutes.setBackground(backgroundColorLight);
                    temp.setForeground(textColorLight);
                    logoutButtonPanel.setBackground(backgroundColorLight);
                    logoutButton.setBackground(buttonColorLight);
                    logoutButton.setForeground(buttonTextColorLight);
                    planButtonPanel.setBackground(backgroundColorLight);
                    planButton.setBackground(buttonColorLight);
                    planButton.setForeground(buttonTextColorLight);
                    language.setBackground(buttonColorLight);
                    language.setForeground(buttonTextColorLight);
                    loginMain.setBackground(backgroundColorLight);
                    loginPanel.setBackground(backgroundColorLight);
                    emailText1.setBackground(backgroundColorLight);
                    email1.setForeground(textColorLight);
                    pwText2.setBackground(backgroundColorLight);
                    textField6.setForeground(buttonTextColorLight);
                    textField6.setBackground(buttonColorLight);
                    password2.setForeground(textColorLight);
                    emailInp1.setBackground(backgroundColorLight);
                    passwordField2.setForeground(buttonTextColorLight);
                    passwordField2.setBackground(buttonColorLight);
                    pwInp2.setBackground(backgroundColorLight);
                    panel.setBackground(backgroundColorLight);
                    goToRegister1.setBackground(buttonColorLight);
                    goToRegister1.setForeground(buttonTextColorLight);
                    goToPlan1.setBackground(buttonColorLight);
                    goToPlan1.setForeground(buttonTextColorLight);
                    loginButton.setBackground(buttonColorLight);
                    loginButton.setForeground(buttonTextColorLight);
                    panel3.setBackground(backgroundColorLight);
                    panel6.setBackground(backgroundColorLight);


                    //profilepage
                    avatarPanel.setBackground(backgroundColorLight);
                    profilePage.setBackground(backgroundColorLight);
                    profilePage1.setBackground(backgroundColorLight);
                    panel4.setBackground(backgroundColorLight);
                    profileMain.setBackground(backgroundColorLight);
                    profilePage.setBackground(backgroundColorLight);
                    profilePage1.setBackground(backgroundColorLight);
                    updateFirstNameText.setBackground(backgroundColorLight);
                    updateFirstName.setForeground(textColorLight);
                    firstNameInput.setBackground(backgroundColorLight);
                    inpFirstName.setForeground(buttonTextColorLight);
                    inpFirstName.setBackground(buttonColorLight);
                    lastNameUpdateText.setBackground(backgroundColorLight);
                    lastName2.setForeground(textColorLight);
                    lastNameInp2.setBackground(backgroundColorLight);
                    inpLastName.setForeground(buttonTextColorLight);
                    inpLastName.setBackground(buttonColorLight);
                    usernameText2.setBackground(backgroundColorLight);
                    username2.setForeground(textColorLight);
                    usernameInp2.setBackground(backgroundColorLight);
                    inpUsername.setForeground(buttonTextColorLight);
                    inpUsername.setBackground(buttonColorLight);
                    emailText2.setBackground(backgroundColorLight);
                    email2.setForeground(textColorLight);
                    emailInp2.setBackground(backgroundColorLight);
                    inpEmail.setForeground(buttonTextColorLight);
                    inpEmail.setBackground(buttonColorLight);
                    inpEmail.setBackground(buttonColorLight);
                    inpEmail.setForeground(buttonTextColorLight);
                    inpPassword.setBackground(buttonColorLight);
                    inpPassword.setForeground(buttonTextColorLight);
                    pwInp3.setBackground(backgroundColorLight);
                    password3.setForeground(textColorLight);
                    pwText3.setBackground(backgroundColorLight);
                    panel5.setBackground(backgroundColorLight);
                    updateButton.setBackground(buttonColorLight);
                    updateButton.setForeground(buttonTextColorLight);
                    panel4.setBackground(backgroundColorLight);
                    goToPlan3.setBackground(buttonColorLight);
                    goToPlan3.setForeground(buttonTextColorLight);
                    //register
                    register.setBackground(backgroundColorLight);
                    username.setBackground(backgroundColorLight);
                    username.setForeground(textColorLight);
                    registerMain.setBackground(backgroundColorLight);
                    register1.setBackground(backgroundColorLight);
                    firstNameText.setBackground(backgroundColorLight);
                    firstName.setForeground(textColorLight);
                    firstNameInp.setBackground(backgroundColorLight);
                    textField2.setForeground(buttonTextColorLight);
                    textField2.setBackground(buttonColorLight);
                    lastNameText.setBackground(backgroundColorLight);
                    lastNameInp.setBackground(backgroundColorLight);
                    lastName.setForeground(textColorLight);
                    textField3.setForeground(buttonTextColorLight);
                    usernameText.setBackground(backgroundColorLight);
                    textField3.setBackground(buttonColorLight);
                    textField4.setForeground(buttonTextColorLight);
                    textField4.setBackground(buttonColorLight);
                    usernameInp.setBackground(backgroundColorLight);
                    emailText.setBackground(backgroundColorLight);
                    email.setForeground(textColorLight);
                    emailInp.setBackground(backgroundColorLight);
                    textField5.setForeground(buttonTextColorLight);
                    textField5.setBackground(buttonColorLight);
                    passwordField.setBackground(buttonColorLight);
                    passwordField.setForeground(buttonTextColorLight);
                    pwText.setBackground(backgroundColorLight);
                    pwText1.setBackground(backgroundColorLight);
                    password1.setForeground(textColorLight);
                    pwInp1.setBackground(backgroundColorLight);
                    passwordField1.setForeground(buttonTextColorLight);
                    passwordField1.setBackground(buttonColorLight);
                    panel1.setBackground(backgroundColorLight);
                    registerButton.setBackground(buttonColorLight);
                    registerButton.setForeground(buttonTextColorLight);
                    panel2.setBackground(backgroundColorLight);
                    goToPlan.setBackground(buttonColorLight);
                    goToPlan.setForeground(buttonTextColorLight);
                    goToLogin1.setBackground(buttonColorLight);
                    goToLogin1.setForeground(buttonTextColorLight);
                    password.setBackground(backgroundColorLight);
                    password.setForeground(textColorLight);
                    pwInp.setBackground(backgroundColorLight);

                    logOutButton.setBackground(buttonColorLight);
                    logOutButton.setForeground(buttonTextColorLight);
                    profileButton.setBackground(buttonColorLight);
                    profileButton.setForeground(buttonTextColorLight);

                    inlogStatus.setForeground(textColorLight);
                    inlogStatus2.setForeground(textColorLight);

                    registerStatus.setForeground(textColorLight);
                    registerStatus.setForeground(textColorLight);
                    inputUsernameStatus.setForeground(textColorLight);
                    inputEmailSatus.setForeground(textColorLight);
                    inputPasswordStatus.setForeground(textColorLight);
                    inputRepeatPasswordStatus.setForeground(textColorLight);
                    foutUserStatus.setForeground(textColorLight);
                    updateStatus.setForeground(textColorLight);

                    //plan panel
                    routes.setBackground(backgroundColorLight);
                    routeTitleText.setForeground(textColorLight);
                    routeTitle.setBackground(backgroundColorLight);
                    routeMain.setBackground(backgroundColorLight);
                    routeMain.setBorder(new LineBorder(textColorLight, 1));
                    showRoute1.setBackground(backgroundColorLight);
                    showRoute1Text.setBackground(backgroundColorLight);
                    route1Start.setForeground(textColorLight);
                    route1Separator.setForeground(textColorLight);
                    route1End.setForeground(textColorLight);
                    showRoute1Time.setBackground(backgroundColorLight);
                    route1TimeStart.setForeground(textColorLight);
                    route1TimeSeparator.setForeground(textColorLight);
                    route1TimeEnd.setForeground(textColorLight);
                    betweenRoute1.setBackground(backgroundColorLight);
                    showRoute2.setBackground(backgroundColorLight);
                    showRoute2Text.setBackground(backgroundColorLight);
                    route2Start.setForeground(textColorLight);
                    route2Separator.setForeground(textColorLight);
                    route2End.setForeground(textColorLight);
                    showRoute2Time.setBackground(backgroundColorLight);
                    route2TimeStart.setForeground(textColorLight);
                    route2TimeSeparator.setForeground(textColorLight);
                    route2TimeEnd.setForeground(textColorLight);
                    betweenRoute2.setBackground(backgroundColorLight);
                    showRoute3.setBackground(backgroundColorLight);
                    showRoute3Text.setBackground(backgroundColorLight);
                    route3Start.setForeground(textColorLight);
                    route3Separator.setForeground(textColorLight);
                    route3End.setForeground(textColorLight);
                    showRoute3Time.setBackground(backgroundColorLight);
                    route3TimeStart.setForeground(textColorLight);
                    route3TimeSeparator.setForeground(textColorLight);
                    route3TimeEnd.setForeground(textColorLight);
                    betweenRoute3.setBackground(backgroundColorLight);
                    showRoute4.setBackground(backgroundColorLight);
                    showRoute4Text.setBackground(backgroundColorLight);
                    route4Start.setForeground(textColorLight);
                    route4Separator.setForeground(textColorLight);
                    route4End.setForeground(textColorLight);
                    showRoute4Time.setBackground(backgroundColorLight);
                    route4TimeStart.setForeground(textColorLight);
                    route4TimeSeparator.setForeground(textColorLight);
                    route4TimeEnd.setForeground(textColorLight);
                    betweenRoute4.setBackground(backgroundColorLight);
                    panel4.setBackground(backgroundColorLight);
                    goToPlan2.setBackground(buttonColorLight);
                    goToPlan2.setForeground(buttonTextColorLight);
                    route1TimeAndDistance.setBackground(backgroundColorLight);
                    route1Distance.setBackground(backgroundColorLight);
                    route1DistanceText.setForeground(textColorLight);
                    route2TimeAndDistance.setBackground(backgroundColorLight);
                    route2Distance.setBackground(backgroundColorLight);
                    route2DistanceText.setForeground(textColorLight);
                    route3TimeAndDistance.setBackground(backgroundColorLight);
                    route3Distance.setBackground(backgroundColorLight);
                    route3DistanceText.setForeground(textColorLight);
                    route4TimeAndDistance.setBackground(backgroundColorLight);
                    route4Distance.setBackground(backgroundColorLight);
                    route4DistanceText.setForeground(textColorLight);

                }
            }
        }
    };


    public GUI() {
        languageSwitchBoolean = localeGetDefault != localeNL;
        frame = new JFrame();
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setLayout(new BorderLayout());
        frame.setLocationRelativeTo(null);
        frame.setExtendedState(JFrame.MAXIMIZED_BOTH);
        frame.setResizable(false);

        mainPanel = new JPanel();
        mainPanel.setLayout(new CardLayout());

        // TOP panel, TITLE
        {
            JPanel title = new JPanel();
            title.setBackground(new Color(255, 98, 0));
            title.setSize(new Dimension(0, 150));
            title.setLayout(new BorderLayout());
            title.setBorder(new LineBorder(textColorLight, 2));
            JPanel logo = new JPanel();
            logo.setBackground(new Color(255, 98, 0));
            logo.setLayout(new FlowLayout(FlowLayout.CENTER));
            JPanel languageScreenShotButtons = new JPanel();
            languageScreenShotButtons.setBackground(new Color(255, 98, 0));
            languageScreenShotButtons.setLayout(new FlowLayout(FlowLayout.RIGHT));
            languageScreenShotButtons.setPreferredSize(new Dimension(200, 0));
            language = new JButton();
            language.setSize(40, 25);
            icon = new ImageIcon("pictures/engelseVlag.jpg");
            icon1 = new ImageIcon("pictures/nederlandseVlag.jpg");

            Image temp1 = icon.getImage().getScaledInstance(language.getWidth(),
                    language.getHeight(), icon.getImage().SCALE_SMOOTH);
            Image temp2 = icon1.getImage().getScaledInstance(language.getWidth(),
                    language.getHeight(), icon1.getImage().SCALE_SMOOTH);
            icon = new ImageIcon(temp1);
            icon1 = new ImageIcon(temp2);
            language = new JButton();
            if (!languageSwitchBoolean) {
                language.setIcon(icon);
            } else {
                language.setIcon(icon1);
            }
            language.addActionListener(this);
            language.setBorder(new EmptyBorder(0, 0, 0, 0));
            language.setBackground(buttonColorDark);
            language.setForeground(buttonTextColorLight);
            language.setFocusable(false);
            languageScreenShotButtons.add(language);
            printScreen = new JButton();
            printScreen.setSize(40, 25);
            printScreen.setBackground(buttonColorDark);
            printScreen.setForeground(buttonTextColorLight);
            printScreen.setFocusable(false);
            printScreen.addActionListener(this);
            printScreen.setBorder(new EmptyBorder(0, 0, 0, 0));
            ImageIcon PSIcon = new ImageIcon("pictures/screenshotIcon1.png");
//            Image temp = PSIcon.getImage().getScaledInstance(printScreen.getWidth(), printScreen.getHeight(),
//                    PSIcon.getImage().SCALE_SMOOTH);
//            PSIcon = new ImageIcon(temp);
            printScreen.setIcon(PSIcon);
            languageScreenShotButtons.add(printScreen);
            ImageIcon icon = new ImageIcon("pictures/ovAppLogoBlack.png");
            JLabel titleLabel = new JLabel("TravelRoutes");
            titleLabel.setForeground(new Color(0, 0, 0));
            titleLabel.setFont(new Font("", Font.BOLD, 50));
            titleLabel.setIcon(icon);
            title.add(languageScreenShotButtons, BorderLayout.EAST);
            logo.add(titleLabel);
            title.add(logo, BorderLayout.CENTER);
            darkModeButton = new JButton("Dark Mode");
            darkModeButton.setSize(100, 20);
            darkModeButton.setBackground(buttonColorLight);
            darkModeButton.setForeground(buttonTextColorLight);
            darkModeButton.setFocusable(false);
            darkModeButton.addActionListener(colourSwitch);
            JPanel titleLeft = new JPanel();
            titleLeft.setBackground(new Color(255, 98, 0));
            titleLeft.setPreferredSize(languageScreenShotButtons.getPreferredSize());
            title.add(titleLeft, BorderLayout.WEST);
            frame.add(darkModeButton, BorderLayout.SOUTH);
            frame.add(title, BorderLayout.NORTH);
        }

        // NORTH panel of HOME
        {
            home = new JPanel();
            home.setLayout(new BorderLayout());
            home.setBackground(backgroundColorLight);
            homeLeft = new JPanel();
            homeLeft.setBorder(new LineBorder(textColorLight, 1));
            homeLeft.setLayout(new BorderLayout(0, 100));
            homeLeft.setBackground(backgroundColorLight);
            homeLeft.setPreferredSize(new Dimension(400, 0));
            plan = new JPanel();
            plan.setLayout(new BorderLayout());
            plan.setBackground(backgroundColorLight);
            planInp = new JPanel();
            planInp.setLayout(new GridLayout(2, 1));
            planInp.setBackground(backgroundColorLight);
            planInp.setBorder(new EmptyBorder(25, 0, 0, 0));
            planStartSearch = new JPanel();
            planStartSearch.setBackground(backgroundColorLight);
            planStartSearch.setLayout(new FlowLayout(FlowLayout.CENTER));
            comboBoxStart = new JComboBox<>();
            for (String a : data.getAllLocations()) {
                comboBoxStart.addItem(a);
            }
            comboBoxStart.setRenderer(new ComboBoxRenderer(rb.getString("vertrek")));
            comboBoxStart.setSelectedIndex(-1);
            comboBoxStart.addItemListener(this);
            comboBoxStart.setFocusable(false);
            comboBoxStart.setBackground(buttonColorLight);
            comboBoxStart.setForeground(buttonTextColorLight);
            comboBoxStart.setPreferredSize(new Dimension(200, 20));
            planStartSearch.add(comboBoxStart);
            planInp.add(planStartSearch);
            planEndSearch = new JPanel();
            planEndSearch.setBackground(backgroundColorLight);
            planEndSearch.setLayout(new FlowLayout(FlowLayout.CENTER));
            comboBoxEnd = new JComboBox<>();
            for (String a : data.getNotSelectedLocations(comboBoxStart.getItemAt(0))) {
                comboBoxEnd.addItem(a);
            }
            comboBoxEnd.setRenderer(new ComboBoxRenderer(rb.getString("aankomst")));
            comboBoxEnd.setSelectedIndex(-1);
            comboBoxEnd.addItemListener(this);
            comboBoxEnd.setFocusable(false);
            comboBoxEnd.setBackground(buttonColorLight);
            comboBoxEnd.setForeground(buttonTextColorLight);
            comboBoxEnd.setPreferredSize(new Dimension(200, 20));
            planEndSearch.add(comboBoxEnd);
            planInp.add(planEndSearch);
            plan.add(planInp, BorderLayout.NORTH);
            timeInp = new JPanel();
            timeInp.setBackground(backgroundColorLight);
            timeInp.setLayout(new FlowLayout(FlowLayout.CENTER, 0, 0));
            timeHour = new JPanel();
            timeHour.setBackground(backgroundColorLight);
            timeHour.setLayout(new FlowLayout(FlowLayout.RIGHT));
            timeHourSelect = new JComboBox<>();
            for (int i = 0; i < 24; i++) {
                timeHourSelect.addItem(i);
            }
            timeHourSelect.setSelectedIndex(lt.getHour());
            timeHourSelect.setFocusable(false);
            timeHourSelect.setBackground(buttonColorLight);
            timeHourSelect.setForeground(buttonTextColorLight);
            timeHour.add(timeHourSelect);
            timeInp.add(timeHour);
            hourMinuteSeparator = new JPanel();
            hourMinuteSeparator.setBackground(backgroundColorLight);
            separator = new JLabel(":");
            separator.setForeground(textColorLight);
            hourMinuteSeparator.add(separator);
            timeInp.add(hourMinuteSeparator);
            timeMinutes = new JPanel();
            timeMinutes.setBackground(backgroundColorLight);
            timeMinutes.setLayout(new FlowLayout(FlowLayout.RIGHT));
            timeMinutesSelect = new JComboBox<>();
            for (int i = 0; i < 60; i++) {
                timeMinutesSelect.addItem(i);
            }
            timeMinutesSelect.setSelectedIndex(lt.getMinute());
            timeMinutesSelect.setFocusable(false);
            timeMinutesSelect.setBackground(buttonColorLight);
            timeMinutesSelect.setForeground(buttonTextColorLight);
            timeMinutes.add(timeMinutesSelect);
            timeInp.add(timeMinutes);
            timeNowSeparator = new JPanel();
            timeNowSeparator.setBackground(backgroundColorLight);
            separator1 = new JLabel(rb.getString("of"));
            separator1.setForeground(textColorLight);
            timeNowSeparator.add(separator1);
            timeInp.add(timeNowSeparator);
            nowButton = new JPanel();
            nowButton.setBackground(backgroundColorLight);
            timeSelectNow = new JButton(rb.getString("nu"));
            timeSelectNow.addActionListener(this);
            timeSelectNow.setFocusable(false);
            timeSelectNow.setBackground(buttonColorLight);
            timeSelectNow.setForeground(buttonTextColorLight);
            nowButton.add(timeSelectNow);
            timeInp.add(nowButton);
            plan.add(timeInp, BorderLayout.CENTER);
            transType = new JPanel();
            transType.setBackground(backgroundColorLight);
            transType.setLayout(new FlowLayout(FlowLayout.CENTER));
            trainSelect = new JRadioButton(rb.getString("trein"), true);
            trainSelect.setFocusable(false);
            trainSelect.setBackground(backgroundColorLight);
            trainSelect.setForeground(textColorLight);
            trainSelect.addActionListener(e -> {
                data = new TrainData();
                transServiceSelect();
            });
            metroSelect = new JRadioButton(rb.getString("metro"), false);
            metroSelect.setFocusable(false);
            metroSelect.setBackground(backgroundColorLight);
            metroSelect.setForeground(textColorLight);
            metroSelect.addActionListener(e -> {
                data = new MetroData();
                transServiceSelect();
            });
            tramSelect = new JRadioButton(rb.getString("tram"), false);
            tramSelect.setFocusable(false);
            tramSelect.setBackground(backgroundColorLight);
            tramSelect.setForeground(textColorLight);
            tramSelect.addActionListener(e -> {
                data = new TramData();
                transServiceSelect();
            });
            busSelect = new JRadioButton(rb.getString("bus"), false);
            busSelect.setFocusable(false);
            busSelect.setBackground(backgroundColorLight);
            busSelect.setForeground(textColorLight);
            busSelect.addActionListener(e -> {
                data = new BusData();
                transServiceSelect();
            });
            planeSelect = new JRadioButton(rb.getString("vliegtuig"), false);
            planeSelect.setFocusable(false);
            planeSelect.setBackground(backgroundColorLight);
            planeSelect.setForeground(textColorLight);
            planeSelect.addActionListener(e -> {
                data = new PlaneData();
                transServiceSelect();
            });
            transType.add(trainSelect);
            transType.add(metroSelect);
            transType.add(tramSelect);
            transType.add(busSelect);
            transType.add(planeSelect);
            ButtonGroup bg = new ButtonGroup();
            bg.add(trainSelect);
            bg.add(planeSelect);
            bg.add(tramSelect);
            bg.add(busSelect);
            bg.add(metroSelect);
            plan.add(transType, BorderLayout.SOUTH);
            homeLeft.add(plan, BorderLayout.NORTH);
        }

        // CENTER panel, HOME
        {
            buttons = new JPanel();
            buttons.setBackground(backgroundColorLight);
            buttons.setLayout(new FlowLayout());
            goToRegister = new JButton();
            goToRegister.setSize(new Dimension(100, 20));
//            goToRegister.setBorder(new RoundedBorder(20));
//            goToRegister.setUI(new BasicButtonUI() {
//                @Override
//                public void update(Graphics g, JComponent c) {
//                    if (c.isOpaque()) {
//                        Color fillcolor = c.getBackground();
//
//                        AbstractButton button = (AbstractButton) c;
//                        ButtonModel model = button.getModel();
//
//                        if (model.isPressed()) {
//                            fillcolor = fillcolor.darker();
//                        } else if (model.isRollover()) {
//                            fillcolor = fillcolor.brighter();
//                        }
//
//                        g.setColor(fillcolor);
//                        g.fillRoundRect(0, 0, c.getWidth(), c.getHeight(), 20, 20);
//                     }
//                }
//            });
            goToRegister.setText(rb.getString("registreren"));
            goToRegister.addActionListener(this);
            goToRegister.setBackground(buttonColorLight);
            goToRegister.setForeground(buttonTextColorLight);
            goToRegister.setFocusable(false);
            goToLogin = new JButton();
            goToLogin.setSize(new Dimension(100, 20));
            goToLogin.setText(rb.getString("inloggen"));
            goToLogin.addActionListener(this);
            goToLogin.setBackground(buttonColorLight);
            goToLogin.setForeground(buttonTextColorLight);
            goToLogin.setFocusable(false);
            buttons.add(goToRegister);
            buttons.add(goToLogin);
            homeCenter = new JPanel();
            homeCenter.setBackground(backgroundColorLight);
            homeCenter.setLayout(new CardLayout());

            homeCenterText = new JPanel();
            homeCenterText.setBackground(backgroundColorLight);
            homeCenterText.setLayout(new FlowLayout(FlowLayout.CENTER, 1000, 0));

            homeCenterLabel = new JLabel(rb.getString("home.center.label"));
            homeCenterLabel.setForeground(textColorLight);
            homeCenterText.add(homeCenterLabel);
            homeCenterLabel1 = new JLabel(rb.getString("home.center.label1"));
            homeCenterLabel1.setForeground(textColorLight);
            homeCenterText.add(homeCenterLabel1);
            homeCenterLabel2 = new JLabel(rb.getString("home.center.label2"));
            homeCenterLabel2.setForeground(textColorLight);
            homeCenterText.add(homeCenterLabel2);
            homeCenterLabel3 = new JLabel(rb.getString("home.center.label3"));
            homeCenterLabel3.setForeground(textColorLight);
            homeCenterText.add(homeCenterLabel3);
            homeCenterLabel4 = new JLabel(rb.getString("home.center.label4"));
            homeCenterLabel4.setForeground(textColorLight);
            homeCenterText.add(homeCenterLabel4);
            homeCenterText.add(buttons);
            homeCenter.add(homeCenterText, LOGINREGISTER);
            homeLeft.add(homeCenter, BorderLayout.CENTER);

            // CENTER panel, HOME after login
            recentRoutes = new JPanel();
            recentRoutes.setBackground(backgroundColorLight);
            recentRoutes.setLayout(new GridLayout(2, 1));
            temp = new JLabel(rb.getString("temp"));
            temp.setForeground(textColorLight);
            recentRoutes.add(temp);
            logoutButtonPanel = new JPanel();
            logoutButtonPanel.setLayout(new FlowLayout(FlowLayout.CENTER));

            logoutButton = new JButton(rb.getString("logout.button"));
            logoutButton.addActionListener(this);
            logoutButtonPanel.add(logoutButton);
            recentRoutes.add(logoutButtonPanel);
            homeCenter.add(recentRoutes, RECENT);

            // SOUTH panel of HOME
            planButtonPanel = new JPanel();
            planButtonPanel.setBackground(backgroundColorLight);
            planButtonPanel.setLayout(new FlowLayout());
            planButton = new JButton(rb.getString("plan.button"));
            planButton.setSize(new Dimension(100, 20));
            planButton.setBackground(buttonColorLight);
            planButton.setForeground(buttonTextColorLight);
            planButton.setFocusable(false);
            planButton.addActionListener(this);
            planButtonPanel.add(planButton);

//            planButtonPanel.add(language);
//            planButtonPanel.add(printScreen);

            //profielpagina knop als je ingelogd bent
            profileButton = new JButton(rb.getString("profielpagina"));
            profileButton.setSize(new Dimension(100, 20));
            profileButton.setBackground(buttonColorLight);
            profileButton.setForeground(buttonTextColorLight);
            profileButton.setFocusable(false);
            profileButton.addActionListener(this);
            profileButton.setVisible(false);
            profileButton.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    updateStatus.setVisible(false);
                }
            });
            planButtonPanel.add(profileButton);
//            profileButton.setVisible(false);

            //log uit knop
            logOutButton = new JButton("loguit");
            logOutButton.setSize(new Dimension(100, 20));
            logOutButton.setBackground(buttonColorLight);
            logOutButton.setForeground(buttonTextColorLight);

            logOutButton.setFocusable(false);
            logOutButton.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {

                    login.setInlogStatus(false);
                    login.setSessionID(null);
                    System.out.println(login.isInlogStatus() + login.getSessionID());
                    profileButton.setVisible(false);
                    logOutButton.setVisible(false);
                    inlogStatus.setVisible(false);
                    inlogStatus2.setVisible(false);
                    goToLogin.setVisible(true);
                    homeCenterLabel.setVisible(true);
                    homeCenterLabel1.setVisible(true);
                    homeCenterLabel2.setVisible(true);
                    homeCenterLabel3.setVisible(true);
                    homeCenterLabel4.setVisible(true);

                    goToLogin.setVisible(true);
                    goToRegister.setVisible(true);

                }
            });
            planButtonPanel.add(logOutButton);
            logOutButton.setVisible(false);
            homeLeft.add(planButtonPanel, BorderLayout.SOUTH);
            home.add(homeLeft, BorderLayout.WEST);
            mainPanel.add(home, PLAN);
        }

        ///////////////////////////////////////////////////////////////////////////////////////////
        //profielpagina////////////////////////////////////////////////////////////////////////////
        {
            profileMain = new JPanel();


            profileMain.setLayout(new BorderLayout());
            profilePage = new JPanel();
            profilePage.setLayout(new GridLayout(6, 2));
            profilePage.setBorder(new RoundedBorder(20));
            profilePage1 = new JPanel();
            avatarPanel = new JPanel();
            //image
            try {

                BufferedImage avatar1 = ImageIO.read(new File("pictures/vrouwAvatarPNG.png"));
                Image newImage = avatar1.getScaledInstance(200, 200, Image.SCALE_DEFAULT);
                JLabel avatarLabel = new JLabel(new ImageIcon(newImage));
                avatarPanel.add(avatarLabel);
            } catch (IOException e) {

            }


            //firstname
            updateFirstNameText = new JPanel();
            updateFirstNameText.setLayout(new FlowLayout(FlowLayout.RIGHT));
            updateFirstName = new JLabel(rb.getString("first.name"));
            updateFirstNameText.add(updateFirstName);
            profilePage.add(updateFirstNameText);
            //firstname input
            firstNameInput = new JPanel();
            firstNameInput.setLayout(new FlowLayout(FlowLayout.LEFT));
            inpFirstName = new JTextField();
            inpFirstName.setPreferredSize(new Dimension(175, 20));
            firstNameInput.add(inpFirstName);
            profilePage.add(firstNameInput);
            //lastname
            lastNameUpdateText = new JPanel();
            lastNameUpdateText.setLayout(new FlowLayout(FlowLayout.RIGHT));
            lastName2 = new JLabel(rb.getString("last.name"));
            lastNameUpdateText.add(lastName2);
            profilePage.add(lastNameUpdateText);
            //lastname input
            lastNameInp2 = new JPanel();
            lastNameInp2.setLayout(new FlowLayout(FlowLayout.LEFT));
            inpLastName = new JTextField();
            inpLastName.setPreferredSize(new Dimension(175, 20));
            lastNameInp2.add(inpLastName);
            profilePage.add(lastNameInp2);
            //username
            usernameText2 = new JPanel();
            usernameText2.setLayout(new FlowLayout(FlowLayout.RIGHT));
            username2 = new JLabel(rb.getString("username"));
            usernameText2.add(username2);
            profilePage.add(usernameText2);
            //username input
            usernameInp2 = new JPanel();
            usernameInp2.setLayout(new FlowLayout(FlowLayout.LEFT));
            inpUsername = new JTextField();
            inpUsername.setPreferredSize(new Dimension(175, 20));
            usernameInp2.add(inpUsername);
            profilePage.add(usernameInp2);
            //email
            emailText2 = new JPanel();
            emailText2.setLayout(new FlowLayout(FlowLayout.RIGHT));
            email2 = new JLabel(rb.getString("email"));
            emailText2.add(email2);
            profilePage.add(emailText2);
            //email input
            emailInp2 = new JPanel();
            emailInp2.setLayout(new FlowLayout(FlowLayout.LEFT));
            inpEmail = new JTextField();
            inpEmail.setPreferredSize(new Dimension(175, 20));
            emailInp2.add(inpEmail);
            profilePage.add(emailInp2);
            //password
            pwText3 = new JPanel();
            pwText3.setLayout(new FlowLayout(FlowLayout.RIGHT));
            password3 = new JLabel(rb.getString("password"));
            pwText3.add(password3);
            profilePage.add(pwText3);
            //password input
            pwInp3 = new JPanel();
            pwInp3.setLayout(new FlowLayout(FlowLayout.LEFT));
            inpPassword = new JPasswordField();
            inpPassword.setPreferredSize(new Dimension(175, 20));
            pwInp3.add(inpPassword);
            profilePage.add(pwInp3);

            panel4 = new JPanel();
            panel4.setLayout(new FlowLayout(FlowLayout.CENTER));
            updateButton = new JButton("Update");
            updateButton.setSize(new Dimension(100, 20));
            updateButton.setFocusable(false);

            updateStatus = new JLabel("Gegevens geupdate.");
            updateStatus.setVisible(false);

            panel4.add(updateStatus);
            panel4.add(updateButton);

            updateButton.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {

                    UpdateUser updateUser = new UpdateUser();
                    String sessionID = login.getSessionID();
                    String inputUsername = inpUsername.getText();
                    String inputFirstName = inpFirstName.getText();
                    String inputLastName = inpLastName.getText();
                    String inputEmail = inpEmail.getText();
                    String inputPassword = inpPassword.getText();

                    System.out.println("update sessionid: " + sessionID + " input: " + inputUsername + " " + inputFirstName + " " + inputLastName + " " + inputEmail + " " + inputPassword);

                    //update username
                    if (!(inputUsername.equals(""))) {
                        inpUsername.setText("");
                        updateStatus.setVisible(true);
                        updateUser.updateUser(sessionID, "username", inputUsername);
                    }
                    //update firstname
                    if (!(inputFirstName.equals(""))) {
                        inpFirstName.setText("");
                        updateStatus.setVisible(true);
                        updateUser.updateUser(sessionID, "firstName", inputFirstName);
                    }
                    //update lastname
                    if (!(inputLastName.equals(""))) {
                        inpLastName.setText("");
                        updateStatus.setVisible(true);
                        updateUser.updateUser(sessionID, "lastName", inputLastName);
                    }
                    //update email
                    if (!(inputEmail.equals(""))) {
                        inpEmail.setText("");
                        updateStatus.setVisible(true);
                        updateUser.updateUser(sessionID, "email", inputEmail);
                    }
                    //update password
                    if (!(inputPassword.equals(""))) {
                        inpPassword.setText("");
                        updateStatus.setVisible(true);
                        updateUser.updateUser(sessionID, "password", inputPassword);
                    }

                }
            });

            profilePage.add(panel4);
//            profileMain.add(panel4, BorderLayout.CENTER);
            panel5 = new JPanel();
            panel5.setLayout(new FlowLayout(FlowLayout.CENTER));
            goToPlan3 = new JButton("Terug");
            goToPlan3.setSize(new Dimension(100, 20));
            goToPlan3.addActionListener(this);

            goToPlan3.setFocusable(false);
            panel5.add(goToPlan3);

            profileMain.add(panel5, BorderLayout.SOUTH);

            avatarPanel.setLayout(new FlowLayout(FlowLayout.LEFT));
            profilePage1.setLayout(new FlowLayout(FlowLayout.RIGHT));
            profilePage1.add(profilePage);

            profileMain.add(avatarPanel, BorderLayout.WEST);
            profileMain.add(profilePage1, BorderLayout.EAST);
            mainPanel.add(profileMain, PROFILE);

            updateStatus.setForeground(textColorLight);
            profileMain.setBackground(backgroundColorLight);
//            profilePage.setBackground(backgroundColorLight);
//            profilePage1.setBackground(backgroundColorLight);
//            avatarPanel.setBackground(Color.green);
//            profilePage.setBackground(Color.blue);
//            profilePage1.setBackground(Color.yellow);
//            panel4.setBackground(Color.red);
            avatarPanel.setBackground(backgroundColorLight);
            profilePage.setBackground(backgroundColorLight);
            profilePage1.setBackground(backgroundColorLight);
            panel4.setBackground(backgroundColorLight);


            updateFirstNameText.setBackground(backgroundColorLight);
            updateFirstName.setForeground(textColorLight);
            firstNameInput.setBackground(backgroundColorLight);
            inpFirstName.setForeground(buttonTextColorLight);
            inpFirstName.setBackground(buttonColorLight);
            lastNameUpdateText.setBackground(backgroundColorLight);
            lastName2.setForeground(textColorLight);
            lastNameInp2.setBackground(backgroundColorLight);
            inpLastName.setForeground(buttonTextColorLight);
            inpLastName.setBackground(buttonColorLight);
            usernameText2.setBackground(backgroundColorLight);
            username2.setForeground(textColorLight);
            usernameInp2.setBackground(backgroundColorLight);
            inpUsername.setForeground(buttonTextColorLight);
            inpUsername.setBackground(buttonColorLight);
            emailText2.setBackground(backgroundColorLight);
            email2.setForeground(textColorLight);
            emailInp2.setBackground(backgroundColorLight);
            inpEmail.setForeground(buttonTextColorLight);
            inpEmail.setBackground(buttonColorLight);
            inpEmail.setBackground(buttonColorLight);
            inpEmail.setForeground(buttonTextColorLight);
            inpPassword.setBackground(buttonColorLight);
            inpPassword.setForeground(buttonTextColorLight);
            pwInp3.setBackground(backgroundColorLight);
            password3.setForeground(textColorLight);
            pwText3.setBackground(backgroundColorLight);
            panel5.setBackground(backgroundColorLight);
            updateButton.setBackground(buttonColorLight);
            updateButton.setForeground(buttonTextColorLight);
            panel4.setBackground(backgroundColorLight);
            goToPlan3.setBackground(buttonColorLight);
            goToPlan3.setForeground(buttonTextColorLight);
        }
        ///////////////////////////////////////////////////////////////////////////////////////////


        // CENTER panel, REGISTREREN
        {
            registerMain = new JPanel();
            registerMain.setLayout(new BorderLayout());
            register = new JPanel();
            register.setLayout(new GridLayout(6, 2));
            register.setBorder(new RoundedBorder(20));
            register1 = new JPanel();
            register1.setLayout(new FlowLayout(FlowLayout.CENTER));
            firstNameText = new JPanel();
            firstNameText.setLayout(new FlowLayout(FlowLayout.RIGHT));
            firstName = new JLabel("Voornaam:");
            firstNameText.add(firstName);
            register.add(firstNameText);
            firstNameInp = new JPanel();
            firstNameInp.setLayout(new FlowLayout(FlowLayout.LEFT));
            textField2 = new JTextField();
            textField2.setPreferredSize(new Dimension(175, 20));
            firstNameInp.add(textField2);
            register.add(firstNameInp);
            lastNameText = new JPanel();
            lastNameText.setLayout(new FlowLayout(FlowLayout.RIGHT));
            lastName = new JLabel("Achternaam:");
            lastNameText.add(lastName);
            register.add(lastNameText);
            lastNameInp = new JPanel();
            lastNameInp.setLayout(new FlowLayout(FlowLayout.LEFT));
            textField3 = new JTextField();
            textField3.setPreferredSize(new Dimension(175, 20));
            lastNameInp.add(textField3);
            register.add(lastNameInp);
            usernameText = new JPanel();
            usernameText.setLayout(new FlowLayout(FlowLayout.RIGHT));
            username = new JLabel("Gebruikersnaam:");
            username.setForeground(textColorLight);
            usernameText.add(username);
            register.add(usernameText);
            usernameInp = new JPanel();
            usernameInp.setLayout(new FlowLayout(FlowLayout.LEFT));
            textField4 = new JTextField();
            textField4.setPreferredSize(new Dimension(175, 20));
            usernameInp.add(textField4);
            register.add(usernameInp);
            emailText = new JPanel();
            emailText.setLayout(new FlowLayout(FlowLayout.RIGHT));
            email = new JLabel("Email:");
            emailText.add(email);
            register.add(emailText);
            emailInp = new JPanel();
            emailInp.setLayout(new FlowLayout(FlowLayout.LEFT));
            textField5 = new JTextField();
            textField5.setPreferredSize(new Dimension(175, 20));
            emailInp.add(textField5);
            register.add(emailInp);
            pwText = new JPanel();
            pwText.setLayout(new FlowLayout(FlowLayout.RIGHT));
            password = new JLabel("Wachtwoord:");

            pwText.add(password);
            register.add(pwText);
            pwInp = new JPanel();

            pwInp.setLayout(new FlowLayout(FlowLayout.LEFT));
            passwordField = new JPasswordField();
            passwordField.setPreferredSize(new Dimension(175, 20));
            pwInp.add(passwordField);
            register.add(pwInp);
            pwText1 = new JPanel();

            pwText1.setLayout(new FlowLayout(FlowLayout.RIGHT));
            password1 = new JLabel(rb.getString("password1"));
            pwText1.add(password1);
            register.add(pwText1);
            pwInp1 = new JPanel();
            pwInp1.setLayout(new FlowLayout(FlowLayout.LEFT));
            passwordField1 = new JPasswordField();
            passwordField1.setPreferredSize(new Dimension(175, 20));
            pwInp1.add(passwordField1);
            register.add(pwInp1);
            register1.add(register);
            registerMain.add(register1, BorderLayout.NORTH);
            panel1 = new JPanel();
            panel1.setLayout(new FlowLayout(FlowLayout.CENTER));
            registerButton = new JButton(rb.getString("register.button"));
            registerButton.setSize(new Dimension(100, 20));

            registerStatus = new JLabel(rb.getString("register.status"));
            inputUsernameStatus = new JLabel(rb.getString("input.username.status"));
            inputEmailSatus = new JLabel(rb.getString("input.email.status"));
            inputPasswordStatus = new JLabel(rb.getString("input.password.status"));
            inputRepeatPasswordStatus = new JLabel(rb.getString("input.repeat.password.status"));
            foutUserStatus = new JLabel(rb.getString("fout.users.status"));

            registerStatus.setVisible(false);
            inputUsernameStatus.setVisible(false);
            inputEmailSatus.setVisible(false);
            inputPasswordStatus.setVisible(false);
            inputRepeatPasswordStatus.setVisible(false);
            foutUserStatus.setVisible(false);

            panel1.add(registerStatus);
            panel1.add(inputUsernameStatus);
            panel1.add(inputEmailSatus);
            panel1.add(inputPasswordStatus);
            panel1.add(inputRepeatPasswordStatus);
            panel1.add(foutUserStatus);

            JSONFileWriter jsonFileWriter = new JSONFileWriter();

            //nieuwe register actionlistener
            registerButton.addActionListener(e -> {

                String inputUsername = textField4.getText();
                String inputFirstName = textField2.getText();
                String inputLastName = textField3.getText();
                String inputEmail = textField5.getText();
                String inputPassword = passwordField.getText();
                String inputRepeatPassword = passwordField1.getText();
                String avatar = "";

                if (inputUsername.equals("")) {
                    inputUsernameStatus.setVisible(true);
                    System.out.println(rb.getString("username.leeg"));
                } else {
                    inputUsernameStatus.setVisible(false);
                }
                if (inputEmail.equals("")) {
                    inputEmailSatus.setVisible(true);
                    System.out.println(rb.getString("email.leeg"));
                } else {
                    inputEmailSatus.setVisible(false);
                }
                if (inputPassword.equals("")) {
                    inputPasswordStatus.setVisible(true);
                    System.out.println(rb.getString("ww.leeg"));
                } else {
                    inputPasswordStatus.setVisible(false);
                }
                inputRepeatPasswordStatus.setVisible(!inputPassword.equals(inputRepeatPassword));


                if ((!inputUsername.equals("") && !inputEmail.equals("") && (inputPassword.equals(inputRepeatPassword) && !inputPassword.equals("")))) {
                    jsonFileWriter.jsonfilewriter(inputUsername, inputFirstName, inputLastName, inputPassword, inputEmail, avatar);
                    if (jsonFileWriter.isNewUserStatus()) {

                        textField4.setText("");
                        textField2.setText("");
                        textField3.setText("");
                        textField5.setText("");
                        passwordField.setText("");
                        passwordField1.setText("");
                        registerStatus.setVisible(true);
                        foutUserStatus.setVisible(false);
                    }

                    if (!jsonFileWriter.isNewUserStatus()) {
                        registerStatus.setVisible(false);
                        foutUserStatus.setVisible(true);
                    }
                }
            });


            registerButton.setFocusable(false);
            panel1.add(registerButton);
            registerMain.add(panel1, BorderLayout.CENTER);
            panel2 = new JPanel();

            panel2.setLayout(new FlowLayout(FlowLayout.CENTER));
            goToPlan = new JButton(rb.getString("back"));
            goToPlan.setSize(new Dimension(100, 20));
            goToPlan.addActionListener(this);

            goToPlan.setFocusable(false);
            goToLogin1 = new JButton(rb.getString("login"));
            goToLogin1.setSize(new Dimension(100, 20));
            goToLogin1.addActionListener(this);
            goToLogin1.setFocusable(false);

            panel2.add(goToPlan);
            panel2.add(goToLogin1);
            registerMain.add(panel2, BorderLayout.SOUTH);
            mainPanel.add(registerMain, REGISTER);

            register.setBackground(backgroundColorLight);
            registerMain.setBackground(backgroundColorLight);
            register1.setBackground(backgroundColorLight);
            firstNameText.setBackground(backgroundColorLight);
            firstName.setForeground(textColorLight);
            firstNameInp.setBackground(backgroundColorLight);
            textField2.setForeground(buttonTextColorLight);
            textField2.setBackground(buttonColorLight);
            lastNameText.setBackground(backgroundColorLight);
            lastNameInp.setBackground(backgroundColorLight);
            lastName.setForeground(textColorLight);
            textField3.setForeground(buttonTextColorLight);
            usernameText.setBackground(backgroundColorLight);
            textField3.setBackground(buttonColorLight);
            textField4.setForeground(buttonTextColorLight);
            textField4.setBackground(buttonColorLight);
            usernameInp.setBackground(backgroundColorLight);
            emailText.setBackground(backgroundColorLight);
            email.setForeground(textColorLight);
            emailInp.setBackground(backgroundColorLight);
            textField5.setForeground(buttonTextColorLight);
            textField5.setBackground(buttonColorLight);
            passwordField.setBackground(buttonColorLight);
            passwordField.setForeground(buttonTextColorLight);
            pwText.setBackground(backgroundColorLight);
            pwText1.setBackground(backgroundColorLight);
            password1.setForeground(textColorLight);
            pwInp1.setBackground(backgroundColorLight);
            passwordField1.setForeground(buttonTextColorLight);
            passwordField1.setBackground(buttonColorLight);
            panel1.setBackground(backgroundColorLight);
            registerButton.setBackground(buttonColorLight);
            registerButton.setForeground(buttonTextColorLight);
            panel2.setBackground(backgroundColorLight);
            goToPlan.setBackground(buttonColorLight);
            goToPlan.setForeground(buttonTextColorLight);
            goToLogin1.setBackground(buttonColorLight);
            goToLogin1.setForeground(buttonTextColorLight);
        }

        // CENTER panel, INLOGGEN
        {
            loginMain = new JPanel();
            loginMain.setLayout(new BorderLayout());
            loginPanel = new JPanel();
            loginPanel.setLayout(new GridLayout(2, 2));
            emailText1 = new JPanel();
            emailText1.setLayout(new FlowLayout(FlowLayout.RIGHT));
            email1 = new JLabel("Email/Gebruikersnaam:");
            emailText1.add(email1);
            pwText2 = new JPanel();
            pwText2.setLayout(new FlowLayout(FlowLayout.RIGHT));
            password2 = new JLabel("Wachtwoord:");
            pwText2.add(password2);
            textField6 = new JTextField();
            textField6.setPreferredSize(new Dimension(175, 20));
            emailInp1 = new JPanel();
            emailInp1.add(textField6);
            emailInp1.setLayout(new FlowLayout(FlowLayout.LEFT));
            passwordField2 = new JPasswordField();
            passwordField2.setPreferredSize(new Dimension(175, 20));
            pwInp2 = new JPanel();
            pwInp2.setLayout(new FlowLayout(FlowLayout.LEFT));
            pwInp2.add(passwordField2);
            loginPanel.add(emailText1);
            loginPanel.add(emailInp1);
            loginPanel.add(pwText2);
            loginPanel.add(pwInp2);
            loginMain.add(loginPanel, BorderLayout.NORTH);
            panel = new JPanel();
            panel.setLayout(new FlowLayout(FlowLayout.CENTER));
            loginButton = new JButton("Inloggen");
            loginButton.setSize(new Dimension(100, 20));

            inlogStatus = new JLabel("Ingelogd.");
            inlogStatus2 = new JLabel("Verkeerde gegevens.");
            inlogStatus.setForeground(textColorLight);
            inlogStatus2.setForeground(textColorLight);


            inlogStatus.setVisible(false);
            inlogStatus2.setVisible(false);
            panel.add(inlogStatus);
            panel.add(inlogStatus2);
            //nieuwe actionlistener voor login
            loginButton.addActionListener(this);
            loginButton.addActionListener(e -> {
                String inputValue = textField6.getText();
                String inputPassword = passwordField2.getText();

                login.loginMethod(inputValue, inputPassword);
                if (login.isInlogStatus()) {
                    textField6.setText("");
                    passwordField2.setText("");
                    inlogStatus.setVisible(true);
                    inlogStatus2.setVisible(false);
                    profileButton.setVisible(true);
                    logOutButton.setVisible(true);
                    homeCenterLabel.setVisible(false);
                    homeCenterLabel1.setVisible(false);
                    homeCenterLabel2.setVisible(false);
                    homeCenterLabel3.setVisible(false);
                    homeCenterLabel4.setVisible(false);

                    goToRegister.setVisible(false);
                    goToLogin.setVisible(false);


                }
                if (!login.isInlogStatus()) {
                    inlogStatus2.setVisible(true);
                    inlogStatus.setVisible(false);
                    profileButton.setVisible(false);
                    logOutButton.setVisible(false);

                    homeCenterLabel.setVisible(true);
                    homeCenterLabel1.setVisible(true);
                    homeCenterLabel2.setVisible(true);
                    homeCenterLabel3.setVisible(true);
                    homeCenterLabel4.setVisible(true);

                    goToLogin.setVisible(true);
                    goToRegister.setVisible(true);
                }
            });

            loginButton.setFocusable(false);
            panel.add(loginButton);
            loginMain.add(panel, BorderLayout.CENTER);
            panel3 = new JPanel();
            panel3.setLayout(new FlowLayout(FlowLayout.CENTER));
            goToPlan1 = new JButton("Terug");
            goToPlan1.setSize(new Dimension(100, 20));
            goToPlan1.addActionListener(this);
            goToPlan1.setFocusable(false);
            goToRegister1 = new JButton(rb.getString("register"));
            goToRegister1.setSize(new Dimension(100, 20));
            goToRegister1.addActionListener(this);
            goToRegister1.setFocusable(false);

            panel3.add(goToPlan1);
            panel3.add(goToRegister1);
            loginMain.add(panel3, BorderLayout.SOUTH);
            mainPanel.add(loginMain, LOGIN);


            loginMain.setBackground(backgroundColorLight);
            loginPanel.setBackground(backgroundColorLight);
            emailText1.setBackground(backgroundColorLight);
            email1.setForeground(textColorLight);
            pwText2.setBackground(backgroundColorLight);
            textField6.setForeground(buttonTextColorLight);
            textField6.setBackground(buttonColorLight);
            password2.setForeground(textColorLight);
            emailInp1.setBackground(backgroundColorLight);
            passwordField2.setForeground(buttonTextColorLight);
            passwordField2.setBackground(buttonColorLight);
            pwInp2.setBackground(backgroundColorLight);
            panel.setBackground(backgroundColorLight);
            goToRegister1.setBackground(buttonColorLight);
            goToRegister1.setForeground(buttonTextColorLight);
            goToPlan1.setBackground(buttonColorLight);
            goToPlan1.setForeground(buttonTextColorLight);
            loginButton.setBackground(buttonColorLight);
            loginButton.setForeground(buttonTextColorLight);
            panel3.setBackground(backgroundColorLight);
        }


        //dark theme toevoegen.
        // CENTER panel, ROUTE
        {
            routeMain = new JPanel();
            routeMain.setLayout(new BorderLayout());
            routeMain.setBorder(new LineBorder(textColorLight, 1));
            routeTitle = new JPanel();
            routeTitle.setLayout(new FlowLayout(FlowLayout.CENTER));
            routeTitleText = new JLabel(rb.getString("route.title.text"));
            routeTitleText.setFont(new Font("", Font.BOLD, 15));
            routeTitle.add(routeTitleText);
            routeMain.add(routeTitle, BorderLayout.NORTH);
            routes = new JPanel();
            routes.setLayout(new GridLayout(4, 2));

            // ROUTE 1
            showRoute1 = new JPanel();
            showRoute1.setLayout(new BorderLayout());
            showRoute1Text = new JPanel();
            showRoute1Text.setLayout(new FlowLayout(FlowLayout.CENTER));
            route1Start = new JLabel();
            route1Start.setFont(new Font("", Font.PLAIN, 15));
            showRoute1Text.add(route1Start);
            route1Separator = new JLabel("➤");
            route1Separator.setFont(new Font("", Font.PLAIN, 15));
            route1Separator.setVisible(false);
            showRoute1Text.add(route1Separator);
            route1End = new JLabel();
            route1End.setFont(new Font("", Font.PLAIN, 15));
            showRoute1Text.add(route1End);
            showRoute1.add(showRoute1Text, BorderLayout.NORTH);
            route1TimeAndDistance = new JPanel();
            route1TimeAndDistance.setLayout(new FlowLayout(FlowLayout.CENTER));
            showRoute1Time = new JPanel();
            showRoute1Time.setLayout(new FlowLayout(FlowLayout.RIGHT));
            route1TimeStart = new JLabel();
            route1TimeStart.setFont(new Font("", Font.PLAIN, 15));
            showRoute1Time.add(route1TimeStart);
            route1TimeSeparator = new JLabel("➤");
            route1TimeSeparator.setFont(new Font("", Font.PLAIN, 15));
            route1TimeSeparator.setVisible(false);
            showRoute1Time.add(route1TimeSeparator);
            route1TimeEnd = new JLabel();
            route1TimeEnd.setFont(new Font("", Font.PLAIN, 15));
            showRoute1Time.add(route1TimeEnd);
            route1TimeAndDistance.add(showRoute1Time);
            routes.add(showRoute1);
            route1Distance = new JPanel();
            route1Distance.setLayout(new FlowLayout(FlowLayout.LEFT));
            route1DistanceText = new JLabel();
            route1DistanceText.setFont(new Font("", Font.PLAIN, 15));
            route1Distance.add(route1DistanceText);
            route1TimeAndDistance.add(route1Distance);
            showRoute1.add(route1TimeAndDistance, BorderLayout.CENTER);
            betweenRoute1 = new JPanel();
            betweenRoute1.setLayout(new FlowLayout(FlowLayout.CENTER));
            routes.add(betweenRoute1);

            // ROUTE 2
            showRoute2 = new JPanel();
            showRoute2.setLayout(new BorderLayout());
            showRoute2Text = new JPanel();
            showRoute2Text.setLayout(new FlowLayout(FlowLayout.CENTER));
            route2Start = new JLabel();
            route2Start.setFont(new Font("", Font.PLAIN, 15));
            showRoute2Text.add(route2Start);
            route2Separator = new JLabel("➤");
            route2Separator.setFont(new Font("", Font.PLAIN, 15));
            route2Separator.setVisible(false);
            showRoute2Text.add(route2Separator);
            route2End = new JLabel();
            route2End.setFont(new Font("", Font.PLAIN, 15));
            showRoute2Text.add(route2End);
            showRoute2.add(showRoute2Text, BorderLayout.NORTH);
            route2TimeAndDistance = new JPanel();
            route2TimeAndDistance.setLayout(new FlowLayout(FlowLayout.CENTER));
            showRoute2Time = new JPanel();
            showRoute2Time.setLayout(new FlowLayout(FlowLayout.RIGHT));
            route2TimeStart = new JLabel();
            route2TimeStart.setFont(new Font("", Font.PLAIN, 15));
            showRoute2Time.add(route2TimeStart);
            route2TimeSeparator = new JLabel("➤");
            route2TimeSeparator.setFont(new Font("", Font.PLAIN, 15));
            route2TimeSeparator.setVisible(false);
            showRoute2Time.add(route2TimeSeparator);
            route2TimeEnd = new JLabel();
            route2TimeEnd.setFont(new Font("", Font.PLAIN, 15));
            showRoute2Time.add(route2TimeEnd);
            route2TimeAndDistance.add(showRoute2Time);
            routes.add(showRoute2);
            route2Distance = new JPanel();
            route2Distance.setLayout(new FlowLayout(FlowLayout.LEFT));
            route2DistanceText = new JLabel();
            route2DistanceText.setFont(new Font("", Font.PLAIN, 15));
            route2Distance.add(route2DistanceText);
            route2TimeAndDistance.add(route2Distance);
            showRoute2.add(route2TimeAndDistance, BorderLayout.CENTER);
            betweenRoute2 = new JPanel();
            betweenRoute2.setLayout(new FlowLayout(FlowLayout.CENTER));
            routes.add(betweenRoute2);

            // ROUTE 3
            showRoute3 = new JPanel();
            showRoute3.setLayout(new BorderLayout());
            showRoute3Text = new JPanel();
            showRoute3Text.setLayout(new FlowLayout(FlowLayout.CENTER));
            route3Start = new JLabel();
            route3Start.setFont(new Font("", Font.PLAIN, 15));
            showRoute3Text.add(route3Start);
            route3Separator = new JLabel("➤");
            route3Separator.setFont(new Font("", Font.PLAIN, 15));
            route3Separator.setVisible(false);
            showRoute3Text.add(route3Separator);
            route3End = new JLabel();
            route3End.setFont(new Font("", Font.PLAIN, 15));
            showRoute3Text.add(route3End);
            showRoute3.add(showRoute3Text, BorderLayout.NORTH);
            route3TimeAndDistance = new JPanel();
            route3TimeAndDistance.setLayout(new FlowLayout(FlowLayout.CENTER));
            showRoute3Time = new JPanel();
            showRoute3Time.setLayout(new FlowLayout(FlowLayout.RIGHT));
            route3TimeStart = new JLabel();
            route3TimeStart.setFont(new Font("", Font.PLAIN, 15));
            showRoute3Time.add(route3TimeStart);
            route3TimeSeparator = new JLabel("➤");
            route3TimeSeparator.setFont(new Font("", Font.PLAIN, 15));
            route3TimeSeparator.setVisible(false);
            showRoute3Time.add(route3TimeSeparator);
            route3TimeEnd = new JLabel();
            route3TimeEnd.setFont(new Font("", Font.PLAIN, 15));
            showRoute3Time.add(route3TimeEnd);
            route3TimeAndDistance.add(showRoute3Time);
            routes.add(showRoute3);
            route3Distance = new JPanel();
            route3Distance.setLayout(new FlowLayout(FlowLayout.LEFT));
            route3DistanceText = new JLabel();
            route3DistanceText.setFont(new Font("", Font.PLAIN, 15));
            route3Distance.add(route3DistanceText);
            route3TimeAndDistance.add(route3Distance);
            showRoute3.add(route3TimeAndDistance, BorderLayout.CENTER);
            betweenRoute3 = new JPanel();
            betweenRoute3.setLayout(new FlowLayout(FlowLayout.CENTER));
            routes.add(betweenRoute3);

            // ROUTE 4
            showRoute4 = new JPanel();
            showRoute4.setLayout(new BorderLayout());
            showRoute4Text = new JPanel();
            showRoute4Text.setLayout(new FlowLayout(FlowLayout.CENTER));
            route4Start = new JLabel();
            route4Start.setFont(new Font("", Font.PLAIN, 15));
            showRoute4Text.add(route4Start);
            route4Separator = new JLabel("➤");
            route4Separator.setFont(new Font("", Font.PLAIN, 15));
            route4Separator.setVisible(false);
            showRoute4Text.add(route4Separator);
            route4End = new JLabel();
            route4End.setFont(new Font("", Font.PLAIN, 15));
            showRoute4Text.add(route4End);
            showRoute4.add(showRoute4Text, BorderLayout.NORTH);
            route4TimeAndDistance = new JPanel();
            route4TimeAndDistance.setLayout(new FlowLayout(FlowLayout.CENTER));
            showRoute4Time = new JPanel();
            showRoute4Time.setLayout(new FlowLayout(FlowLayout.RIGHT));
            route4TimeStart = new JLabel();
            route4TimeStart.setFont(new Font("", Font.PLAIN, 15));
            showRoute4Time.add(route4TimeStart);
            route4TimeSeparator = new JLabel("➤");
            route4TimeSeparator.setFont(new Font("", Font.PLAIN, 15));
            route4TimeSeparator.setVisible(false);
            showRoute4Time.add(route4TimeSeparator);
            route4TimeEnd = new JLabel();
            route4TimeEnd.setFont(new Font("", Font.PLAIN, 15));
            showRoute4Time.add(route4TimeEnd);
            route4TimeAndDistance.add(showRoute4Time);
            routes.add(showRoute4);
            route4Distance = new JPanel();
            route4Distance.setLayout(new FlowLayout(FlowLayout.LEFT));
            route4DistanceText = new JLabel();
            route4DistanceText.setFont(new Font("", Font.PLAIN, 15));
            route4Distance.add(route4DistanceText);
            route4TimeAndDistance.add(route4Distance);
            showRoute4.add(route4TimeAndDistance, BorderLayout.CENTER);
            betweenRoute4 = new JPanel();
            betweenRoute4.setLayout(new FlowLayout(FlowLayout.CENTER));
            routes.add(betweenRoute4);
            routeMain.add(routes, BorderLayout.CENTER);
            panel6 = new JPanel();
            goToPlan2 = new JButton(rb.getString("back"));
            goToPlan2.setSize(new Dimension(100, 20));
            goToPlan2.addActionListener(this);
            goToPlan2.setFocusable(false);

//            panel6.add(printScreen3);
//            panel6.add(goToPlan2);
            routeMain.add(panel6, BorderLayout.SOUTH);
            routes.setBackground(backgroundColorLight);
            routeTitleText.setForeground(textColorLight);
            routeTitle.setBackground(backgroundColorLight);
            routeMain.setBackground(backgroundColorLight);
            showRoute1.setBackground(backgroundColorLight);
            showRoute1Text.setBackground(backgroundColorLight);
            route1Start.setForeground(textColorLight);
            route1Separator.setForeground(textColorLight);
            route1End.setForeground(textColorLight);
            showRoute1Time.setBackground(backgroundColorLight);
            route1TimeStart.setForeground(textColorLight);
            route1TimeSeparator.setForeground(textColorLight);
            route1TimeEnd.setForeground(textColorLight);
            betweenRoute1.setBackground(backgroundColorLight);
            route1Distance.setBackground(backgroundColorLight);
            route1TimeAndDistance.setBackground(backgroundColorLight);
            route1DistanceText.setForeground(textColorLight);
            showRoute2.setBackground(backgroundColorLight);
            showRoute2Text.setBackground(backgroundColorLight);
            route2Start.setForeground(textColorLight);
            route2Separator.setForeground(textColorLight);
            route2End.setForeground(textColorLight);
            showRoute2Time.setBackground(backgroundColorLight);
            route2TimeStart.setForeground(textColorLight);
            route2TimeSeparator.setForeground(textColorLight);
            route2TimeEnd.setForeground(textColorLight);
            route2Distance.setBackground(backgroundColorLight);
            route2TimeAndDistance.setBackground(backgroundColorLight);
            route2DistanceText.setForeground(textColorLight);
            betweenRoute2.setBackground(backgroundColorLight);
            showRoute3.setBackground(backgroundColorLight);
            showRoute3Text.setBackground(backgroundColorLight);
            route3Start.setForeground(textColorLight);
            route3Separator.setForeground(textColorLight);
            route3End.setForeground(textColorLight);
            showRoute3Time.setBackground(backgroundColorLight);
            route3TimeStart.setForeground(textColorLight);
            route3TimeSeparator.setForeground(textColorLight);
            route3TimeEnd.setForeground(textColorLight);
            betweenRoute3.setBackground(backgroundColorLight);
            route3Distance.setBackground(backgroundColorLight);
            route3TimeAndDistance.setBackground(backgroundColorLight);
            route3DistanceText.setForeground(textColorLight);
            showRoute4.setBackground(backgroundColorLight);
            showRoute4Text.setBackground(backgroundColorLight);
            route4Start.setForeground(textColorLight);
            route4Separator.setForeground(textColorLight);
            route4End.setForeground(textColorLight);
            showRoute4Time.setBackground(backgroundColorLight);
            route4TimeStart.setForeground(textColorLight);
            route4TimeSeparator.setForeground(textColorLight);
            route4TimeEnd.setForeground(textColorLight);
            betweenRoute4.setBackground(backgroundColorLight);
            route4Distance.setBackground(backgroundColorLight);
            route4TimeAndDistance.setBackground(backgroundColorLight);
            route4DistanceText.setForeground(textColorLight);
            panel6.setBackground(backgroundColorLight);
            goToPlan2.setBackground(buttonColorLight);
            goToPlan2.setForeground(buttonTextColorLight);
            home.add(routeMain, BorderLayout.CENTER);
        }

        // add components to frame
        frame.add(mainPanel, BorderLayout.CENTER);

        frame.setVisible(true);
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        CardLayout mainCl = (CardLayout) (mainPanel.getLayout());
        CardLayout cl = (CardLayout) (homeCenter.getLayout());

        if (e.getSource() == goToRegister) {
            mainCl.show(mainPanel, REGISTER);
        } else if (e.getSource() == logoutButton) {
            cl.show(homeCenter, LOGINREGISTER);
        } else if (e.getSource() == goToPlan) {
            mainCl.show(mainPanel, PLAN);
        } else if (e.getSource() == profileButton) {
            mainCl.show(mainPanel, PROFILE);
        } else if (e.getSource() == goToPlan3) {
            mainCl.show(mainPanel, PLAN);
        } else if (e.getSource() == goToLogin) {
            mainCl.show(mainPanel, LOGIN);
        } else if (e.getSource() == goToPlan1) {
            mainCl.show(mainPanel, PLAN);
        } else if (e.getSource() == goToRegister1) {
            mainCl.show(mainPanel, REGISTER);
        } else if (e.getSource() == goToLogin1) {
            mainCl.show(mainPanel, LOGIN);
        } else if (e.getSource() == timeSelectNow) {
            lt = LocalTime.now();
            for (int i = 0; i < timeHourSelect.getItemCount(); i++) {
                if (timeHourSelect.getItemAt(i) == lt.getHour()) {
                    timeHourSelect.setSelectedIndex(i);
                }
            }
            for (int i = 0; i < timeMinutesSelect.getItemCount(); i++) {
                if (timeMinutesSelect.getItemAt(i) == lt.getMinute()) {
                    timeMinutesSelect.setSelectedIndex(i);
                }
            }
        } else if (e.getSource() == planButton) {
            planRoute(getStartTime(), getEndPoint(), getStartPoint());
        } else if (e.getSource() == goToPlan2) {
            mainCl.show(mainPanel, PLAN);
        } else if (e.getSource() == loginButton) {
            if (login.isInlogStatus()) {
                mainCl.show(mainPanel, PLAN);
            }
        } else if (e.getSource() == language) {
            int i = comboBoxStart.getSelectedIndex();
            int j = comboBoxEnd.getSelectedIndex();
            if (!languageSwitchBoolean) {
                languageSwitchBoolean = true; // switch to english
                rb = ResourceBundle.getBundle("info", localeUS);
                language.setIcon(icon1);
            } else {
                languageSwitchBoolean = false; // switch to dutch
                rb = ResourceBundle.getBundle("info", localeNL);
                language.setIcon(icon);
            }
            comboBoxStart.removeAllItems();
            for (String a : data.getAllLocations()) {
                comboBoxStart.addItem(a);
            }
            comboBoxStart.setRenderer(new ComboBoxRenderer(rb.getString("vertrek")));
            comboBoxStart.setSelectedIndex(i);
            comboBoxEnd.removeAllItems();
            if (comboBoxStart.getSelectedIndex() != -1) {
                for (String a : data.getNotSelectedLocations(comboBoxStart.getItemAt(i))) {
                    comboBoxEnd.addItem(a);
                }
            }
            comboBoxEnd.setRenderer(new ComboBoxRenderer(rb.getString("aankomst")));
            comboBoxEnd.setSelectedIndex(j);
            timeSelectNow.setText(rb.getString("nu"));
            trainSelect.setText(rb.getString("trein"));
            planeSelect.setText(rb.getString("vliegtuig"));
            goToRegister.setText(rb.getString("registreren"));
            goToLogin.setText(rb.getString("inloggen"));
            separator1.setText(rb.getString("of"));
            homeCenterLabel.setText(rb.getString("home.center.label"));
            homeCenterLabel1.setText(rb.getString("home.center.label1"));
            homeCenterLabel2.setText(rb.getString("home.center.label2"));
            homeCenterLabel3.setText(rb.getString("home.center.label3"));
            homeCenterLabel4.setText(rb.getString("home.center.label4"));
            temp.setText(rb.getString("temp"));
            logoutButton.setText(rb.getString("logout.button"));
            planButton.setText(rb.getString("plan.button"));
            firstName.setText(rb.getString("first.name"));
            lastName.setText(rb.getString("last.name"));
            username.setText(rb.getString("username"));
            email.setText(rb.getString("email"));
            password.setText(rb.getString("password"));
            password1.setText(rb.getString("password1"));
            registerButton.setText(rb.getString("register.button"));
            registerStatus.setText(rb.getString("register.status"));
            inputUsernameStatus.setText(rb.getString("input.username.status"));
            inputEmailSatus.setText(rb.getString("input.email.status"));
            inputPasswordStatus.setText(rb.getString("input.password.status"));
            inputRepeatPasswordStatus.setText(rb.getString("input.repeat.password.status"));
            profileButton.setText(rb.getString("profielpagina"));
            foutUserStatus.setText(rb.getString("fout.users.status"));
            goToPlan.setText(rb.getString("back"));
            goToLogin1.setText(rb.getString("login"));
            email1.setText(rb.getString("email1"));
            password2.setText(rb.getString("password2"));
            loginButton.setText(rb.getString("login"));
            inlogStatus.setText(rb.getString("login.status"));
            inlogStatus2.setText(rb.getString("login.status2"));
            goToPlan1.setText(rb.getString("back"));
            goToRegister1.setText(rb.getString("register"));
            routeTitleText.setText(rb.getString("route.title.text"));
            goToPlan2.setText(rb.getString("back"));
            updateFirstName.setText(rb.getString("first.name"));
            lastName2.setText(rb.getString("last.name"));
            username2.setText(rb.getString("username"));
            email2.setText(rb.getString("email"));
            password3.setText(rb.getString("password"));
            printScreen.setText(rb.getString("screen.shot"));
        } else if (e.getSource() == printScreen) {
            try {
                PrintScreen.printScreen(frame);
            } catch (AWTException ex) {
                ex.printStackTrace();
            } catch (IOException ex) {
                ex.printStackTrace();
            }
        }

    }

    //label aanmaken voor de endPoint text etc
    public void planRoute(LocalTime startTime, String endPoint, String startPoint) {
        betweenRoute1.removeAll();
        betweenRoute2.removeAll();
        betweenRoute3.removeAll();
        betweenRoute4.removeAll();
        trips = data.getTrips(startPoint, endPoint, startTime);
        route1Start.setText(startPoint);
        route2Start.setText(startPoint);
        route3Start.setText(startPoint);
        route4Start.setText(startPoint);
        route1End.setText(endPoint);
        route2End.setText(endPoint);
        route3End.setText(endPoint);
        route4End.setText(endPoint);

        DecimalFormat df= new DecimalFormat("##0.#");
        double route1DistanceDouble = (trips.getTripsArray().get(0).getLocationA().getCoordinates().getDistance(trips.getTripsArray().get(0).getLocationB().getCoordinates()));

        route1Separator.setVisible(true);
        route1TimeSeparator.setVisible(true);
        route2Separator.setVisible(true);
        route2TimeSeparator.setVisible(true);
        route3Separator.setVisible(true);
        route3TimeSeparator.setVisible(true);
        route4Separator.setVisible(true);
        route4TimeSeparator.setVisible(true);
        route1TimeStart.setText(trips.getTripsArray().get(0).getDeparture().toString());
        route2TimeStart.setText(trips.getTripsArray().get(1).getDeparture().toString());
        route3TimeStart.setText(trips.getTripsArray().get(2).getDeparture().toString());
        route4TimeStart.setText(trips.getTripsArray().get(3).getDeparture().toString());
        route1DistanceText.setText(df.format(route1DistanceDouble) + " km");
        route2DistanceText.setText(df.format(route1DistanceDouble) + " km");
        route3DistanceText.setText(df.format(route1DistanceDouble) + " km");
        route4DistanceText.setText(df.format(route1DistanceDouble) + " km");
        route1TimeEnd.setText(trips.getTripsArray().get(0).getArrival().toString());
        route2TimeEnd.setText(trips.getTripsArray().get(1).getArrival().toString());
        route3TimeEnd.setText(trips.getTripsArray().get(2).getArrival().toString());
        route4TimeEnd.setText(trips.getTripsArray().get(3).getArrival().toString());

        int indexStart = 0;
        int indexEnd = 0;
        for (Stopover stopover : trips.getTripsArray().get(0).getRoute().getStopOvers()) {
            if (Objects.equals(stopover.getLocationName(), startPoint)) {
                indexStart = trips.getTripsArray().get(0).getRoute().getStopOvers().indexOf(stopover);
            }
            if (Objects.equals(stopover.getLocationName(), endPoint)) {
                indexEnd = trips.getTripsArray().get(0).getRoute().getStopOvers().indexOf(stopover);
            }
        }
        List<Stopover> stopovers = trips.getTripsArray().get(0).getRoute().getStopOvers().subList(indexStart, indexEnd + 1);
        for (Stopover stopover : stopovers) {
            //labeltext maken

            routeLabel1 = new JLabel(stopover.getLocationName());
            betweenRoute1.add(routeLabel1);
            routeLabel2 = new JLabel(stopover.getLocationName());
            betweenRoute2.add(routeLabel2);
            routeLabel3 = new JLabel(stopover.getLocationName());
            betweenRoute3.add(routeLabel3);
            routeLabel4 = new JLabel(stopover.getLocationName());
            betweenRoute4.add(routeLabel4);

            if (darkmodeStatus) {
                routeLabel1.setForeground(textColorDark);
                routeLabel2.setForeground(textColorDark);
                routeLabel3.setForeground(textColorDark);
                routeLabel4.setForeground(textColorDark);
            } else {

                routeLabel1.setForeground(textColorLight);
                routeLabel2.setForeground(textColorLight);
                routeLabel3.setForeground(textColorLight);
                routeLabel4.setForeground(textColorLight);
            }


            if (stopovers.indexOf(stopover) < stopovers.size() - 1) {
                arrow1 = new JLabel("➤");
                arrow2 = new JLabel("➤");
                arrow3 = new JLabel("➤");
                arrow4 = new JLabel("➤");
                betweenRoute1.add(arrow1);
                betweenRoute2.add(arrow2);
                betweenRoute3.add(arrow3);
                betweenRoute4.add(arrow4);

                if (darkmodeStatus) {
                    arrow1.setForeground(textColorDark);
                    arrow2.setForeground(textColorDark);
                    arrow3.setForeground(textColorDark);
                    arrow4.setForeground(textColorDark);
                } else {

                    arrow1.setForeground(textColorLight);
                    arrow2.setForeground(textColorLight);
                    arrow3.setForeground(textColorLight);
                    arrow4.setForeground(textColorLight);
                }
            }
        }
    }

    private void transServiceSelect() {
        comboBoxStart.removeAllItems();
        comboBoxEnd.removeAllItems();
        for (String a : data.getAllLocations()) {
            comboBoxStart.addItem(a);
            comboBoxEnd.addItem(a);
        }
        comboBoxStart.setSelectedIndex(-1);
        comboBoxEnd.setSelectedIndex(-1);
    }

    @Override
    public void itemStateChanged(ItemEvent e) {
        if (e.getSource() == comboBoxStart) {
            if (comboBoxStart.getItemCount() != 0 && comboBoxStart.getSelectedIndex() != -1) {
                comboBoxEnd.removeAllItems();
                for (String a : data.getNotSelectedLocations(getStartPoint())) {
                    if (!Objects.equals(a, getStartPoint())) {
                        comboBoxEnd.addItem(a);
                    }
                }
            }
        }
    }

    public String getStartPoint() {
        return Objects.requireNonNull(comboBoxStart.getSelectedItem()).toString();
    }

    public String getEndPoint() {
        return Objects.requireNonNull(comboBoxEnd.getSelectedItem()).toString();
    }

    public LocalTime getStartTime() {
        return LocalTime.of(timeHourSelect.getSelectedIndex(), timeMinutesSelect.getSelectedIndex());
    }

    private static class RoundedBorder implements Border {
        private int radius;

        public RoundedBorder(int radius) {
            this.radius = radius;
        }

        public Insets getBorderInsets(Component c) {
            return getBorderInsets(c, new Insets(0,0,0,0));
        }

        public Insets getBorderInsets(Component c, Insets insets) {
            insets.top = insets.bottom = radius /2;
            insets.left = insets.right = 1;
            return insets;
        }

        public boolean isBorderOpaque() {
            return false;
        }

        @Override
        public void paintBorder(Component c, Graphics g, int x, int y, int width, int height) {
            g.drawRoundRect(x, y, width - 1, height - 1, radius, radius);
        }
    }

    private static class ComboBoxRenderer extends JLabel implements ListCellRenderer
    {
        private String _title;

        public ComboBoxRenderer(String title)
        {
            _title = title;
        }

        @Override
        public Component getListCellRendererComponent(JList list, Object value,
                                                      int index, boolean isSelected, boolean hasFocus)
        {
            if (index == -1 && value == null) setText(_title);
            else setText(value.toString());
            return this;
        }
    }
}