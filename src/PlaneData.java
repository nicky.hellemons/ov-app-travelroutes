import java.time.LocalTime;

public class PlaneData extends Data {

    public PlaneData()
    {
        //////////Locations
        Location location = new Location("Schiphol Airport", new Coordinates(52.30949795164425, 4.762265890910406));
        locationMap.put(location.getLocationName(), location);

        location = new Location("Rotterdam Airport", new Coordinates(51.956990999887694, 4.440235997966231));
        locationMap.put(location.getLocationName(), location);

        location = new Location("Eindhoven Airport", new Coordinates(51.45051175463289, 5.378399111479047));
        locationMap.put(location.getLocationName(), location);

        location = new Location("Groningen Eelde", new Coordinates(53.13091828976438, 6.58526293005224));
        locationMap.put(location.getLocationName(), location);

        location = new Location("Maastricht Aachen Airport", new Coordinates(50.921379731105475, 5.783904713609849));
        locationMap.put(location.getLocationName(), location);

        //////////ROUTE Schiphol-Rotterdam
        for (int hour = 6; hour <= 22; hour+=4) {
        Route route = new Route(locationMap.get("Schiphol Airport"), LocalTime.of(hour, 0));
        route.addEndPoint(locationMap.get("Rotterdam Airport"), LocalTime.of(hour, 15));
        addRoute(route);
        }

        //////////ROUTE Rotterdam-Schiphol
        for (int hour = 6; hour <= 22; hour+=4) {
            Route route = new Route(locationMap.get("Rotterdam Airport"), LocalTime.of(hour, 0));
            route.addEndPoint(locationMap.get("Schiphol Airport"), LocalTime.of(hour, 15));
            addRoute(route);
        }

        //////////ROUTE Schiphol-Eindhoven
        for (int hour = 6; hour <= 22; hour+=4) {
            Route route = new Route(locationMap.get("Schiphol Airport"), LocalTime.of(hour, 5));
            route.addEndPoint(locationMap.get("Eindhoven Airport"), LocalTime.of(hour, 20));
            addRoute(route);
        }


        //////////ROUTE Eindhoven-Schiphol
        for (int hour = 6; hour <= 22; hour+=4) {
            Route route = new Route(locationMap.get("Eindhoven Airport"), LocalTime.of(hour, 5));
            route.addEndPoint(locationMap.get("Schiphol Airport"), LocalTime.of(hour, 20));
            addRoute(route);
        }

        //////////ROUTE Schiphol-Groningen Eelde
        for (int hour = 6; hour <= 22; hour+=4) {
            Route route = new Route(locationMap.get("Schiphol Airport"), LocalTime.of(hour, 0));
            route.addEndPoint(locationMap.get("Groningen Eelde"), LocalTime.of(hour, 25));
            addRoute(route);
        }

        //////////ROUTE Groningen Eelde-Schiphol
        for (int hour = 6; hour <= 22; hour+=4) {
            Route route = new Route(locationMap.get("Groningen Eelde"), LocalTime.of(hour, 0));
            route.addEndPoint(locationMap.get("Schiphol Airport"), LocalTime.of(hour, 25));
            addRoute(route);
        }

        //////////ROUTE Schiphol-Maastricht
        for (int hour = 6; hour <= 22; hour+=4) {
            Route route = new Route(locationMap.get("Schiphol Airport"), LocalTime.of(hour, 10));
            route.addEndPoint(locationMap.get("Maastricht Aachen Airport"), LocalTime.of(hour, 40));
            addRoute(route);
        }

        //////////ROUTE Maastricht-Schiphol
        for (int hour = 6; hour <= 22; hour+=4) {
            Route route = new Route(locationMap.get("Maastricht Aachen Airport"), LocalTime.of(hour,10));
            route.addEndPoint(locationMap.get("Schiphol Airport"), LocalTime.of(hour, 40));
            addRoute(route);
        }

        //////////Rotterdam Airport - Eindhoven Airport
        for (int hour = 7; hour <= 19; hour+=4) {
            Route route = new Route(locationMap.get("Rotterdam Airport"), LocalTime.of(hour, 0));
            route.addEndPoint(locationMap.get("Eindhoven Airport"), LocalTime.of(hour, 15));
            addRoute(route);
        }

        //////////Eindhoven Airport - Rotterdam Airport
        for (int hour = 7; hour <= 19; hour+=4) {
            Route route = new Route(locationMap.get("Eindhoven Airport"), LocalTime.of(hour, 0));
            route.addEndPoint(locationMap.get("Rotterdam Airport"), LocalTime.of(hour, 15));
            addRoute(route);
        }

        //////////Rotterdam Airport - Groningen Eelde
        for (int hour = 8; hour <= 20; hour+=4) {
            Route route = new Route(locationMap.get("Rotterdam Airport"), LocalTime.of(hour, 0));
            route.addEndPoint(locationMap.get("Groningen Eelde"), LocalTime.of(hour, 30));
            addRoute(route);
        }

        //////////Groningen Eelde - Rotterdam Airport
        for (int hour = 8; hour <= 20; hour+=4) {
            Route route = new Route(locationMap.get("Groningen Eelde"), LocalTime.of(hour, 0));
            route.addEndPoint(locationMap.get("Rotterdam Airport"), LocalTime.of(hour, 30));
            addRoute(route);
        }

        //////////Rotterdam Airport - Maastricht Aachen Airport
        for (int hour = 6; hour <= 22; hour+=4) {
            Route route = new Route(locationMap.get("Rotterdam Airport"), LocalTime.of(hour, 0));
            route.addEndPoint(locationMap.get("Maastricht Aachen Airport"), LocalTime.of(hour, 25));
            addRoute(route);
        }

        //////////Maastricht Aachen Airport - Rotterdam Airport
        for (int hour = 6; hour <= 22; hour+=4) {
            Route route = new Route(locationMap.get("Maastricht Aachen Airport"), LocalTime.of(hour, 0));
            route.addEndPoint(locationMap.get("Rotterdam Airport"), LocalTime.of(hour, 25));
            addRoute(route);
        }

        //////////Eindhoven Airport - Groningen Eelde
        for (int hour = 6; hour <= 22; hour+=4) {
            Route route = new Route(locationMap.get("Eindhoven Airport"), LocalTime.of(hour, 0));
            route.addEndPoint(locationMap.get("Groningen Eelde"), LocalTime.of(hour, 25));
            addRoute(route);
        }

        //////////Groningen Eelde - Eindhoven Airport
        for (int hour = 6; hour <= 22; hour+=4) {
            Route route = new Route(locationMap.get("Groningen Eelde"), LocalTime.of(hour, 0));
            route.addEndPoint(locationMap.get("Eindhoven Airport"), LocalTime.of(hour, 25));
            addRoute(route);
        }

        //////////Eindhoven Aiport - Maastricht Aachen Airport
        for (int hour = 8; hour <= 20; hour+=4) {
            Route route = new Route(locationMap.get("Eindhoven Airport"), LocalTime.of(hour, 0));
            route.addEndPoint(locationMap.get("Maastricht Aachen Airport"), LocalTime.of(hour, 15));
            addRoute(route);
        }

        //////////Maastricht Aachen Airport - Eindhoven Airport
        for (int hour = 8; hour <= 20; hour+=4) {
            Route route = new Route(locationMap.get("Maastricht Aachen Airport"), LocalTime.of(hour, 0));
            route.addEndPoint(locationMap.get("Eindhoven Airport"), LocalTime.of(hour, 15));
            addRoute(route);
        }

        //////////Groningen Eelde - Maastricht Aachen Airport
        for (int hour = 8; hour <= 20; hour+=4) {
            Route route = new Route(locationMap.get("Groningen Eelde"), LocalTime.of(hour, 0));
            route.addEndPoint(locationMap.get("Maastricht Aachen Airport"), LocalTime.of(hour, 30));
            addRoute(route);
        }

        //////////Maastricht Aachen Airport - Groningen Eelde
        for (int hour = 8; hour <= 20; hour+=4) {
            Route route = new Route(locationMap.get("Maastricht Aachen Airport"), LocalTime.of(hour, 0));
            route.addEndPoint(locationMap.get("Groningen Eelde"), LocalTime.of(hour, 30));
            addRoute(route);
        }


    }
}
