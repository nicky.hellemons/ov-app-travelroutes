import org.json.simple.JSONObject;

import java.util.HashMap;

public class Login {


    private boolean inlogStatus;
    private String sessionID;
    public Login() {

    }

    public void loginMethod(String inputValue, String inputPassword) {


//        JSONFileHandler jsonFileHandler = new JSONFileHandler();
        ReadDataFromFile data = new ReadDataFromFile();

        for (int i = 0; i < data.getUserArray().size(); i++) {
            JSONObject users = (JSONObject) data.getUserArray().get(i);

            if ((inputValue.equalsIgnoreCase((String) users.get("email")) || inputValue.equalsIgnoreCase((String) users.get("username")))&& inputPassword.equalsIgnoreCase((String) users.get("password"))) {
                System.out.println("inlog gelukt");
                inlogStatus = true;
                sessionID = (String)(users.get("userid"));
                System.out.println(sessionID);
                //update user test

            }
        }
        if (!inlogStatus) {
            System.out.println("inlog mislukt");
            inlogStatus = false;
            sessionID = null;
        }
    }

    public boolean isInlogStatus() {
        return inlogStatus;
    }

    public String getSessionID() {
        return sessionID;
    }

    public void setInlogStatus(boolean inlogStatus) {
        this.inlogStatus = inlogStatus;
    }

    public void setSessionID(String sessionID) {
        this.sessionID = sessionID;
    }
}
