import java.time.LocalTime;
import java.util.*;

//Nicky & Koen
public class Data {

    protected final HashMap<String, Location> locationMap = new HashMap<>();
    protected SortedMap<String, HashSet<Route>> routeMap = new TreeMap<>();


    protected void addRoute(Route route) {
        var routeKey = route.getKey();

        var set = routeMap.get(routeKey);
        if (set == null) {
            set = new HashSet<Route>();
            routeMap.put(routeKey, set);
        }

        set.add(route);
    }

    public Trips getTrips(String keyA, String keyB, LocalTime departure) {
        Trips trips = new Trips();
        Trips trips1 = new Trips();
        for (var e : routeMap.entrySet()) {
            var routeKey = e.getKey(); //vb: Amsterdam-Utrecht-Arnhem-Nijmegen
            var posA = routeKey.indexOf(keyA); //indexOf("Utrecht") = 10
            if (posA >= 0) {
                var posB = routeKey.indexOf(keyB);
                if (posB > posA) //Als posB groter is dan posA dan a-->B
                {
                    var set = e.getValue();
                    for (var r : set) {
                        var candidate = new Trip(r, getLocation(keyA), getLocation(keyB), departure, null);
                        if (candidate.getDeparture().isAfter(departure)) {
                            trips.addTrip(candidate);
                        } else {
                            trips1.addTrip(candidate);
                        }
                    }
                    trips.getTripsArray().sort((o1, o2) -> {
                        LocalTime lt1 = o1.getDeparture();
                        LocalTime lt2 = o2.getDeparture();
                        return lt1.compareTo(lt2);
                    });
                    trips1.getTripsArray().sort((o1, o2) -> {
                        LocalTime lt1 = o1.getDeparture();
                        LocalTime lt2 = o2.getDeparture();
                        return lt1.compareTo(lt2);
                    });
                    trips.getTripsArray().addAll(trips1.getTripsArray());
                }
            }
        }
        return trips;
    }

    public ArrayList<String> getNotSelectedLocations(String selected) {
        ArrayList<String> notSelected = new ArrayList<>();
        for (var e : routeMap.entrySet()) {
            var routeKey = e.getKey();
            var pos = routeKey.indexOf(selected);
            if (pos >= 0) {
                var set = e.getValue();
                for (var r : set) {
                    for (Stopover stopover : r.getStopOvers()) {
                        if (!notSelected.contains(stopover.getLocationName())) {
                            notSelected.add(stopover.getLocationName());
                        }
                    }
                }
            }
        }
        return notSelected;
    }

    public Location getLocation(String key) {
        return locationMap.get(key);
    }

    public ArrayList<String> getAllLocations()
    {
        ArrayList<String> locations = new ArrayList<>();
        locations.addAll(locationMap.keySet());
        Collections.sort(locations);
        return locations;
    }
}


