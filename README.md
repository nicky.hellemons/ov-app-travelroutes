#NAME
Project periode 1B - OV App TravelRoutes T1

##Description
Een routeplanner app waarbij de gebruikers snel en makkelijk een reis kunnen plannen, met integratie van een gebruikersaccount waar gebruikers persoonlijke reis-gerelateerde data kunnen vinden.

##CONTRIBUTORS
- Koen van Meggelen (Product Owner)
- Ivo Coopmans (Scrum Master)
- Nicky Hellemons
- Kevin Scherpenzeel
- Mike van Groningen
- Baraa Mansour
- Ying Tang

##LANGUAGE USED
- Java

##Visuals
- Java swing UI

##Project structure
- Maven

##Java Code conventions
The Java code conventions are defined by Oracle in the coding conventions document. In short, these conventions ask the user to use camel case when defining classes, methods, or variables. Classes start with a capital letter and should be nouns, like CalendarDialogView. For methods, the names should be verbs in imperative form, like getBrakeSystemType, and should start with a lowercase letter. The standard Java code conventions apply for this project. 

##Clean code richtlijnen
1.  Volg de standaard (naming/coding) conventions (afspraken), zowel taal gerelateerd 
als team gerelateerd. 
2.  Keep it simple stupid. Eenvoud is altijd beter. Reduceer complexiteit zoveel mogelijk. 
3.  Zoek altijd naar de kernoorzaak van een probleem. Los het daar ook op.  
4.  Vermijd meerdere malen van dezelfde code (duplicaten)  
5.  Controleer altijd de waarden van onzekere bronnen, zoals bij I/O  
6.  Schrijf altijd brackets om een een-regel block (zoals bij if, while)  
7.  Wees consistent. Doe je iets op een bepaalde manier, doe het overal.  
8.  Gebruik verklarende namen voor variabelen, zoals ‘naam=..’ en niet ‘n=..’ 
9.  Kies betekenisvolle en duidelijke namen voor bestanden, classes, methods, etc. 
10. Schrijf kleine methods/functies 
11. Doe één ding in een method/functies 
12. Maak jezelf begrijpelijk in code, zoals een verhaal. Gebruik comments wijselijk en 
beperkt.  
13. Verberg interne structuren in een class (encapsulation)  
14. Doe één ding in een class (abstraction) 
15. Gebruik een enkele assert per unit testcase  
16. Schrijf alle code en comments in het Engels  
17. Review je code voor compile en run, weet wat je code doet 
18. Gebruik een versiebeheer systeem (zoals Git) 
19. Geef de voorkeur aan niet-statische methoden boven statische methoden 





